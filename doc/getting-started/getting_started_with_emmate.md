# Getting Started with the EmMate Framework

## Introduction

Welcome to EmMate!! Your one stop solution for all your embedded requirements.

You are about to build a **hardware platform agnostic embedded application**

The EmMate framework is built with the intent to **support and work with multiple hardware platforms**

Here is your chance to be a part of **build once and flash anywhere** application ecosystem

# Getting Started

This guide will help you to set up, build and run an embedded application based on the EmMate framework.

**Considerations**

Since this framework is intended to work with multiple hardware platforms, you may choose any hardware to get started.

**We will use ESP32 for this guide.**

## Prerequisites

There are few prior requirements.

### First thing first

You will need a Computer running Linux.

*The EmMate framework is tested with Ubuntu 16.04 LTS. It is highly recommended to use Ubuntu 16.04 LTS as your environment as well.*

### Hardware

You will need any development board based on the following hardware platforms:

- ESP32
- PIC32
- nRF51 or nRF52
- STM32

**We are using ESP32 for this guide.**

It is recommended to use [MIG SoM](https://mig.iquesters.com/?s=somthing#thesom) and [MIG Thing](https://mig.iquesters.com/?s=somthing#thething) boards.

### Software

First you need download the EmMate Framework package. You will be directed to download it in the next section of the document.

You will need an editor to write code in C. We prefer using [Eclipse CDT](https://www.eclipse.org/)

Please download and install **Eclipse CDT** before proceeding as it will be required later on. You may also need to install **a java runtime environment (jre)** for Eclipse

## Downloading the Required Files

Now that you have,

- A Computer running Linux (Ubuntu).
- Chosen your preferred hardware platform. **We are using ESP32 for this guide.**
- Installed **Eclipse CDT**

You may download a release package of EmMate framework.

### Download Package
**NOTE:**

- *It is recommended to download the latest version under the 'Releases' Section from the link below as they have been tested and are stable. Remember to download the package that corresponds to your chosen hardware platform. We are using ESP32 for this guide.*

- *Alternatively you may want to clone the source files from the git. You will get the link to EmMate's source in the link below.*

- *Please note that this document only covers the steps for application development using the release packages of EmMate. If you have cloned the git repository then you need to take additional steps (coming soon) before following this document.*

To download please visit [Downloads](https://mig.iquesters.com/?s=embedded&p=downloads) 

Assuming that you have successfully downloaded a release package, we would proceed further. You may keep the release package anywhere in your machine. We will keep it in `/home/iquesters/emmate-framework/` for this guide.

*NOTE: If you are using the same path as shown above then change the user (i.e. iquesters in this case) on the command above to your system username*

### Distribution package Contents

Extract files from the downloaded package.

The extracted package must contain the following directories:

**platform** - The platform directory contains the source code for the specific hardware platform you have chosen.

**platform-resources** - The platform-resources directory contains the scripts to setup the development environment specific to the hardware platform you have chosen

**setup** - The setup directory contains setup files which will help setting up projects that will be developed using the EmMate framework. Details of how to use the setup files is mentioned below in the Setting up an EmMate Application section.

**src** - The src directory contains the source code for the EmMate framework.

**examples** - The examples directory contains numerous example codes for learning and using the EmMate framework.

**getting-started** - The Getting Started guide doc directory

It must look like:

```
$ pwd
/home/iquesters/emmate-framework/esp32

$ cd esp32
$ ls -l
total 24
drwxrwxr-x 13 iquesters iquesters 4096 Jan 30 18:56 examples
drwxrwxr-x  2 iquesters iquesters 4096 Jan 30 18:56 getting-started
drwxrwxr-x 15 iquesters iquesters 4096 Jan 30 18:56 platform
drwxrwxr-x  3 iquesters iquesters 4096 Jan 30 18:56 platform-resources
drwxrwxr-x  3 iquesters iquesters 4096 Jan 30 18:56 setup
drwxrwxr-x 20 iquesters iquesters 4096 Jan 24 16:24 src
```

### Make Distribution package scripts executable

Please run the below command to make all shell scripts executable.

```
$ cd /home/iquesters/emmate-framework
$ find esp32 -type f -iname "*sh" -exec chmod +x {} \;
```

## Setting up the Development Environment for EmMate

Now that you have,

- Downloaded and extracted the release package of the EmMate framework.

You are ready to setup the development environment for writing, building and deploying embedded applications based on the EmMate Framework.

### First set the EmMate Release Path

Before doing anything you must set the EmMate Release Package's path that you have just downloaded. To do so, you will need to enter a variable called `EMMATE_RELEASE_PATH` in the `~/.profile` file. Please, add the following line to your `~/.profile` file. Make sure to add the path as per your system.

```
export EMMATE_RELEASE_PATH="/home/iquesters/emmate-framework/esp32"
```

**Make Changes Effective and Verify**

Log off and log in back to make this change effective.

Run the following command to check if `EMMATE_RELEASE_PATH` is set:

```
$ printenv EMMATE_RELEASE_PATH
```

*NOTE: Currently only ESP32 is supported. Other platforms are coming soon.*

### ESP32 - Prerequisites

To compile with ESP-IDF you need to get the following packages. Please run the below command in a terminal.

```
$ sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-pip python-setuptools python-serial python-cryptography python-future python-pyparsing python-pyelftools
```

### ESP32 - Toolchain Setup

Please follow the below steps to download and setup your toolchain:

1. **Download and Setup Toolchain**

To setup ESP32 Toolchain for **Ubuntu 16.04 LTS**, run the following commands in a **Terminal**:

```
$ mkdir /home/iquester/esp
$ cd /home/iquester/esp
```

then copy toolchain setup script from `/home/iquester/emmate-framework/esp32/platform-resources/setup/setup-esp-toolchain.sh` to `/home/iquester/esp`

```
$ cp -v -u -r /home/iquester/emmate-framework/esp32/platform-resources/setup/setup-esp-toolchain.sh /home/iquester/esp
$ ./setup-esp-toolchain.sh
```

*Note:* When running the script if you get the error `Permission Denied` then you need to change the file's permission using the below command and then run the script again

``` 
$ chmod 764 setup-esp-toolchain.sh
$ ./setup-esp-toolchain.sh
```

It will take some time to download and setup the toolchain.

2. **Set PATH Environment Variable**

To use the toolchain, you will need to update your PATH environment variable in `~/.profile` file. To make `xtensa-esp32-elf` available for all terminal sessions, add the following line to your `~/.profile` file:

```
export PATH="/home/iquester/esp/xtensa-esp32-elf/bin:$PATH"
```

3. **Make Changes Effective and Verify**

Log off and log in back to make the `.profile` changes effective. Run the following command to verify if `PATH` is correctly set:

```
$ printenv PATH
```

You are looking for similar result containing toolchain's path at the beginning of displayed string:

```
$ printenv PATH
/home/user-name/esp/xtensa-esp32-elf/bin:/home/user-name/bin:/home/user-name/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```

Instead of `/home/user-name` there should be a home path specific to your installation.

### ESP32 - ESP-IDF (SDK) Setup

Besides the toolchain, you also need **ESP-IDF (SDK)**. Please follow the below steps to download and setup the SDK.

1. **Download and Setup ESP-IDF (SDK)**

First you need to copy the SDK setup script from `/home/iquester/emmate-framework/esp32/platform-resources/setup/setup-esp-sdk.sh` to `/home/iquester/esp`. To do so run the following commands:

```
$ cd /home/iquester/esp
$ cp -v -u -r /home/iquester/emmate-framework/esp32/platform-resources/setup/setup-esp-sdk.sh /home/iquester/esp
$ ./setup-esp-sdk.sh
```

*Note:* When running the script if you get the error `Permission Denied` then you need to change the file's permission using the below command and then run the script again

``` 
$ chmod 764 setup-esp-sdk.sh
$ ./setup-esp-sdk.sh
```

ESP-IDF SDK will be downloaded into `/home/iquester/esp/esp-idf`. This will take several minutes to complete, depending upon your internet connection.

2. **Set PATH Environment Variable**

Set up `IDF_PATH` by adding the following line to `~/.profile` file:

```
export IDF_PATH=/home/iquester/esp/esp-idf
```

3. **Make Changes Effective and Verify**

Log off and log in back to make this change effective.

Run the following command to check if `IDF_PATH` is set:

```
$ printenv IDF_PATH
```

The path previously entered in `~/.profile` file should be printed out.

### ESP32 - Required Python2 Packages

The python2 packages required by ESP-SDK are located in `IDF_PATH/requirements.txt`. You can install them by running:

```
$ python2 -m pip install --user -r $IDF_PATH/requirements.txt
```

### ESP32 - Required Python3 Packages

The required python3 packages are: **Kconfiglib** and **Python3 standard GUI Library (tkinter)**

```
$ pip3 install --user kconfiglib
$ sudo apt install python3-tk
```

**NOTE:** If pip3 is not installed, then first install it using the command: `sudo apt install python3-pip`

## ESP32 - Device Setup

After completing the above installations, you are now ready setup your ESP32 based device/board. So, connect your ESP32 based device/board to the computer and check under what serial port it is visible

Serial ports have the following patterns in their names:

- **Linux:** starting with `/dev/tty`

To check the serial port name in Linux run the following command in a terminal

```
$ ls /dev/tty*
```

**Adding user to dialout on Linux**

The currently logged user should have read and write access the serial port over USB. So we need to run the following command on our Ubuntu Linux terminal:

```
$ sudo usermod -a -G dialout $USER
```

Log off and log in back to make this change effective.



# Setting up EmMate Examples and Applications

Now that you have,

- Successfully setup your Development Environment
- Successfully setup your Device

You may now start developing actual applications. The best way to learn EmMate is to try the examples and applications first. Follow this guide to setup and run the EmMate examples and applications.

*Note: The below steps require usage of the Eclipse CDT IDE. So, it is assumed that you have successfully installed the latest version of Eclipse CDT on your machine.* If you have not installed Eclipse, please [click here](https://www.eclipse.org/) to do so. 

## Steps to Setup an Example

For this guide we will use the `hello-world` example located in `/home/iquester/emmate-framework/esp32/examples/getting-started/`. All the other examples can be setup, built and run in the same process. Please refer each example's README.md file to know more about it.

Also note that we have downloaded and extracted the EmMate release package at:

```
/home/iquester/emmate-framework/esp32
```

1. To setup an example project simply copy a example from the EmMate release package and paste it in your Eclipse's workspace.

```
Note: it is mandatory that you copy the examples in your Eclipse workspace, else the build scripts will fail
```

```
$ cd /home/iquester/eclipse-workspace
$ cp -r /home/iquester/emmate-framework/esp32/examples/getting-started/hello-world .
```

2. Change to `setup` directory inside `hello-world`. Now run the `setup.sh` (shell script). This will create the required folder structure and copy other dependency files in your project.

**Note: Before performing this step make sure you have successfully done the steps provided in "First set the EmMate Release Path"**

```
$ cd hello-world/setup
$ ./setup.sh
```

*Note:* When running the script if you get the error `Permission Denied` then you need to change the file's permission using the below command and then run the script again

``` 
$ chmod 764 setup.sh
$ ./setup.sh
```

3. Import EmMate Project into Eclipse.

Open Eclipse and click on `File -> Import`

<img src="img/import-emmate-project.png" width="1080">

Select `General -> Existing Projects into Workspace` and click Next

<img src="img/import-existing-proj.png" width="1080">

Then, click Browse and select the `hello-world` project from the file explorer and click Ok

<img src="img/select-hello-world.png" width="1080">

The `hello-world` project will be automatically shown in the Import Projects window. Click on Finish

<img src="img/select-eclipse-project-and-finish.png" width="1080">

Now the EmMate `hello-world` project will be shown in the Eclipse Project Explorer

<img src="img/hello-world-project-explorer.png" width="1080">


## Steps to Build an Example

1. Click on the Project Name i.e. `hello-world`, then select the External Tool `build` from the dropdown as shown below and finally click on Launch button 

<img src="img/build-project.png" width="1080">

2. You will start seeing log message on the Eclipse Console and the **EmMate Development Framework Configuration** Tool will open. Without changing any configurations, click on Save and close it.

<img src="img/emmate-configuration-tool.png" width="1080">

Now the `hello-world` project will continue the build process and finally you will be able to see the log:

```
## Execute EmMate Make Successful ################################
```

<img src="img/build-complete.png" width="1080">  

## Steps to Flash and View Log Messages

1. First open the README.md file inside the example directory and follow the section **Prepare the Hardware**. In this case `hello-world`

2. Once your hardware is ready and connected, select the Eclipse External Tool `flash` from the dropdown as shown below and click on Launch button.

<img src="img/flash-project.png" width="1080">

If all goes well you will see the following log message in the console.

```
## Execute EmMate Flash Successful ################################
```

<img src="img/flash-complete.png" width="1080">

3. To view log messages you need to open a **Serial Terminal**. The Eclipse terminal provides a Serial Terminal and we are going to use that one. So click on the Terminal button as shown below. The keyboard shortcut is Shift+Ctrl+Alt+T

<img src="img/terminal-open.png" width="1080">

From the `Choose Terminal` dropdown select `Serial Terminal`, and keep the settings as per the below image and click Ok

<img src="img/terminal-settings.png" width="1080">

Now you will be able to see the following.

```
  ______           __  __       _         ______                                           _    
 |  ____|         |  \/  |     | |       |  ____|                                         | |   
 | |__   _ __ ___ | \  / | __ _| |_ ___  | |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
 |  __| | '_ ` _ \| |\/| |/ _` | __/ _ \ |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 | |____| | | | | | |  | | (_| | ||  __/ | |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   < 
 |______|_| |_| |_|_|  |_|\__,_|\__\___| |_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\
     _____                      _               _____       _       _   _                          
    |_   _|                    | |             / ____|     | |     | | (_)                         
      | |  __ _ _   _  ___  ___| |_ ___ _ __  | (___   ___ | |_   _| |_ _  ___  _ __  ___          
      | | / _` | | | |/ _ \/ __| __/ _ \ '__|  \___ \ / _ \| | | | | __| |/ _ \| '_ \/ __|         
     _| || (_| | |_| |  __/\__ \ ||  __/ |     ____) | (_) | | |_| | |_| | (_) | | | \__ \         
    |_____\__, |\__,_|\___||___/\__\___|_|    |_____/ \___/|_|\__,_|\__|_|\___/|_| |_|___/         
             | |                                                                                   
             |_| 


W (852) sysinfo: =============================================================================
W (862) sysinfo: System Information
W (862) sysinfo: =============================================================================
W (872) sysinfo: -----------------------------
W (872) sysinfo: Hardware Information
W (882) sysinfo: -----------------------------
W (882) sysinfo: Chip Name: ESP32
W (892) sysinfo: Chip Manufacturer: ESSPRESIF
W (892) sysinfo: Features:
W (902) sysinfo:        WIFI : YES
W (902) sysinfo:        BT : YES
W (902) sysinfo:        BLE : YES
W (912) sysinfo:        GSM 2G : NO
W (912) sysinfo:        GSM 3G : NO
W (912) sysinfo:        GSM 4G : NO
W (922) sysinfo:        LORA : NO
W (922) sysinfo:        NBIOT : NO
W (932) sysinfo: FLASH SIZE : 4194304 bytes
W (932) sysinfo: RAM SIZE : 532480 bytes
W (932) sysinfo: -----------------------------
W (942) sysinfo: SDK Information
W (942) sysinfo: -----------------------------
W (952) sysinfo: Running SDK : ESP-IDF
W (952) sysinfo: SDk Version : v4.1-dev-256-g9f145ff
W (962) sysinfo: -----------------------------
W (962) sysinfo: Framework Information
W (972) sysinfo: -----------------------------
W (972) sysinfo: Core Version: 1.0.0.0.1127
W (982) sysinfo: Application Version: 0.0.0.0.1
W (982) sysinfo: =============================================================================

I (992) sysinit: ============================== START_APPLICATION ==============================

I (1002) core_app_main: ==================================================================
I (1012) core_app_main: 
I (1012) core_app_main: Starting application built on the EmMate Framework
I (1022) core_app_main: 
I (1032) core_app_main: ==================================================================
Hello World
Hello World
Hello World
Hello World
Hello World
Hello World
```

<img src="img/log-messages.png" width="1080">

And that's all, you have successfully completed the required steps.

4. If you want to erase the flash memory of the hardware, select the **Eclipse External Tool** `erase flash` from the dropdown as shown below and click on **Launch button**.

<img src="img/erase-flash-project.png" width="1080">

You will see the following log message in the console.

```
## Execute EmMate Erase-Flash Successful ################################
```

## Known Issues

1. The **first build might fail**. It is because some git submodules may not get downloaded while the setup scripts are run. So if you get a build error on the first build, please build again and try. 

2. At any point in time if you get the following error in Eclipse while running any of the `External Tools` for building, flash etc. click on the project name in the `Project Explorer` and try again.

<img src="img/eclipse-problem.png" width="1080">


# API Documentation

The API Documentation for the **EmMate** framework is a documentation generated using **Doxygen**

[EmMate Version 1.1.0.0 API Documentation](https://mig.iquesters.com/section/embedded/api-docs/v1.0.0.0)

