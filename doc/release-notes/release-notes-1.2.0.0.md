# EmMate Release v1.2

Documentation for this release can be found at: https://mig.iquesters.com/?s=embedded&p=documentation
API Documentation for this release can be found at: https://mig.iquesters.com/section/embedded/api-docs/

- Supported Hardware - ESP32
- Supported Platform - ESP IDF

## Changes
1. Bugs fixed
  - Factory Reset failed with newer version of ESP IDF
  - Application crased after a FOTA update

2. Device Config module now does not uses `ESP IDF cJSON`. It uses `parson` of EmMate `input-processor`

3. LED Helper can be used simultaneously by EmMate system and application.
  - Now every function of LED Helper takes 1 extra parameter, i.e. MODE
  - **Existing application using the LED Helper must be changed**
  - See the API documentation for details

4. `Netinfo` sub-module introduced in `conn` module. It collects all network interface's information

5. Now `migcloud` module can send the `netinfo` to the cloud in heartbeat. However the code is disabled in this release as migcloud does not support it yet

6. Internet availibility checking sub-module introduced inside `conn` module.
  - It can check if internet is available, if the configured IoT Cloud is available.
  - The `conn` module's statuses has been changed completely. The enum is removed and macro containing bits are added.
  - **Applications using the conn status must be changed to function properly**
  - See the API documentation for details 

## Features
1. Connectivity:
* BLE SPP
* Wi-Fi STA
* Collect network info
* Internet availability checker

2. Network Protocols:
* HTTP(S)
* SNTP

3. IoT Cloud Support (migCloud):
* migCloud - https://mig.iquesters.com/?s=cloud
* Heartbeat
* Send network info in hearbeat
* Firmware Over the Air (FOTA)
* Get application configurations from migCloud server
* Post application data to migCloud server
* Release SoMThing
* Network Reset from server
 

4. System Services:
* Device Configuration via BLE
* System Time synced via SNTP

5. Downlink:
* OneWire

6. Peripherals:
* ADC
* GPIO
* I2C
* UART
* PWM (only for LED)

7. Persistent Memory Support:
* Non-Volatile Memory
* SD/MMC Card

8. File System
* FAT File System

9. HMI Support:
* LED notifications - Now supports system and application simultaneously
* Push button configuration
* Character LCD helper library

10. Logging
* Log levels
* Console Log
* Network Log via UDP (in experimental stage)

11. RTOS features:
* FreeRTOS Tasks
* FreeRTOS Queues
* FreeRTOS Event Groups
* FreeRTOS Mutex
* FreeRTOS Semaphores

12. Helper Libraries:
* Generic Doubly Linked List - https://github.com/philbot9/generic-linked-list
* Parson - https://github.com/kgabis/parson

13. Framework Configurations:
* The EmMate framework can be configured via Kconfig menu based configuration tool

