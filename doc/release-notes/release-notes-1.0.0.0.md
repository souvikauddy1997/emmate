#EmMate Release v1.0

Documentation for this release can be found at: https://mig.iquesters.com/?s=embedded&p=documentation
API Documentation for this release can be found at: https://mig.iquesters.com/section/embedded/api-docs/

- Supported Hardware - ESP32
- Supported Platform - ESP IDF
- Supported RTOS - FreeRTOS

##Features
1. Connectivity:
* BLE SPP
* Wi-Fi STA

2. Network Protocols:
* HTTP(S)
* SNTP

3. IoT Cloud Support:
* migCloud - https://mig.iquesters.com/?s=cloud

4. System Services:
* Device Configuration via BLE
* System Time synced via SNTP
* System Heartbeat
* Firmware Over the Air
* Get application configurations from migCloud server
* Post application data to migCloud server

5. Downlink:
* OneWire

6. Peripherals:
* ADC
* GPIO
* I2C
* UART

7. Persistent Memory Support:
* Non-Volatile Memory
* SD/MMC Card

8. File System
* FAT File System

9. HMI Support:
* LED notifications
* Push button configuration
* Character LCD helper library

10. RTOS features:
* FreeRTOS Tasks
* FreeRTOS Queues
* FreeRTOS Event Groups
* FreeRTOS Mutex
* FreeRTOS Semaphores

11. Helper Libraries:
* Generic Doubly Linked List - https://github.com/philbot9/generic-linked-list
* Parson - https://github.com/kgabis/parson

12. Framework Configurations:
* The EmMate framework can be configured via Kconfig menu based configuration tool

