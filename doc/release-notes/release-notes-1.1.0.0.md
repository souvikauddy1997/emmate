# EmMate Release v1.1

Documentation for this release can be found at: https://mig.iquesters.com/?s=embedded&p=documentation
API Documentation for this release can be found at: https://mig.iquesters.com/section/embedded/api-docs/

- Supported Hardware - ESP32
- Supported Platform - ESP IDF
- Supported RTOS - FreeRTOS

## Changes
- Bug fixes
- Modified build system. Added EMMATE_RELEASE_PATH environment variable
- Separated migCloud from system module and made a separate module
- Added migCloud features
- Added Eclipse IDE support for EmMate Applications
- Examples added

## Features
1. Connectivity:
* BLE SPP
* Wi-Fi STA

2. Network Protocols:
* HTTP(S)
* SNTP

3. IoT Cloud Support (migCloud):
* migCloud - https://mig.iquesters.com/?s=cloud
* Heartbeat
* Firmware Over the Air (FOTA)
* Get application configurations from migCloud server
* Post application data to migCloud server
* Release SoMThing
* Network Reset from server
 

4. System Services:
* Device Configuration via BLE
* System Time synced via SNTP
* System Heartbeat (moved to migCloud)
* Firmware Over the Air (moved to migCloud)
* Get application configurations from migCloud server (moved to migCloud)
* Post application data to migCloud server (moved to migCloud)

5. Downlink:
* OneWire

6. Peripherals:
* ADC
* GPIO
* I2C
* UART
* PWM (only for LED)

7. Persistent Memory Support:
* Non-Volatile Memory
* SD/MMC Card

8. File System
* FAT File System

9. HMI Support:
* LED notifications
* Push button configuration
* Character LCD helper library

10. Logging
* Log levels
* Console Log
* Network Log via UDP (in experimental stage)

11. RTOS features:
* FreeRTOS Tasks
* FreeRTOS Queues
* FreeRTOS Event Groups
* FreeRTOS Mutex
* FreeRTOS Semaphores

12. Helper Libraries:
* Generic Doubly Linked List - https://github.com/philbot9/generic-linked-list
* Parson - https://github.com/kgabis/parson

13. Framework Configurations:
* The EmMate framework can be configured via Kconfig menu based configuration tool

