/*
 * persistent_mem_ble.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_BLE_H_
#define PERSISTENT_MEM_BLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

#if CONFIG_USE_BLE

/**
 * @brief This function reads the Wi-Fi MAC from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	mac		Pointer to ble MAC to be populated and returned as an out parameter
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_ble_mac_from_persistent_mem(uint8_t* mac);

/**
 * @brief This function writes the Wi-Fi MAC to the selected persistent memory
 *
 * @param[in]	mac		Pointer to ble MAC to be saved in the memory
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_ble_mac_to_persistent_mem(uint8_t* mac);

#endif

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_BLE_H_ */
