/*
 * File Name: core_system_info.c
 * File Path: /emmate/src/system/sysinfo/core_system_info.c
 * Description:
 *
 *  Created on: 11-Jun-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include "core_system_info.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include "version.h"
#include "app_version.h"

#define TAG	LTAG_SYSTEM_SYSINFO

static SystemInfo system_info;
static bool is_systeminfo_acquired = false;

/* Get hardware specific information by reading from the chip */
//static void read_save_platform_info() {
//	read_hardware_info(&system_info.platform_info);
//}

static void read_save_framework_info() {
	strcpy(system_info.framework_info.core_version, CORE_DEV_VERSION_NUMBER);
#ifdef APP_DEV_VERSION_NUMBER
	strcpy(system_info.framework_info.app_version, APP_DEV_VERSION_NUMBER);
#else
	strcpy(system_info.framework_info.app_version, "NO APP VERSION FOUND");
#endif
}

void read_save_system_info() {
	read_save_framework_info();
	read_save_platform_info(&system_info.platform_info);
	is_systeminfo_acquired = true;
}

void print_system_info() {
	printf("\n");
	CORE_LOGW(TAG, "=============================================================================");
	CORE_LOGW(TAG, "System Information");
	CORE_LOGW(TAG, "=============================================================================");
	print_system_specific_info(&system_info.platform_info);
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG, "Framework Information");
	CORE_LOGW(TAG, "-----------------------------");
	CORE_LOGW(TAG, "EmMate Version: %s", system_info.framework_info.core_version);
	CORE_LOGW(TAG, "Application Version: %s", system_info.framework_info.app_version);
	CORE_LOGW(TAG, "=============================================================================");
	printf("\n");
}

SystemInfo * get_system_info() {
	if (!is_systeminfo_acquired) {
		return NULL;
	}

	return &system_info;
}

PlatformInfo * get_platform_info() {
	if (!is_systeminfo_acquired) {
		return NULL;
	}

	return &system_info.platform_info;
}

HardwareInfo * get_hardware_info() {
	if (!is_systeminfo_acquired) {
		return NULL;
	}

	return &system_info.platform_info.hw_info;
}

SDKInfo * get_sdk_info() {
	if (!is_systeminfo_acquired) {
		return NULL;
	}

	return &system_info.platform_info.sdk_info;
}

FrameworkInfo * get_framework_info() {
	if (!is_systeminfo_acquired) {
		return NULL;
	}

	return &system_info.framework_info;
}
