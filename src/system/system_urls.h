/*
 * system_urls.h
 *
 *  Created on: 12-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_URLS_H_
#define SYSTEM_URLS_H_

#ifdef __cplusplus
extern "C" {
#endif

#if CONFIG_USE_MIGCLOUD
#include "migcloud_urls.h"
#endif

#define _DEV_SERVER		0
#define _STAGE_SERVER	0

#if _DEV_SERVER
#define IQ_HOST							"192.168.0.123:8443"
#define IQ_HOST_PORT 					8443
#define IQ_SOM_REGISTRATION_POST_URL 		"https://"IQ_HOST"/mig/rest/som/register"
#define IQ_SYS_HWIDENTIFY_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/identify"
#define IQ_SYS_HEARTBEAT_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/heartbeat"
#define IQ_SYS_APPCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/app"
#define IQ_SYS_APPCONFIG_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/configuration/update/status"
#define IQ_SYS_SYSCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/system"
#define IQ_SYS_FOTASTATUS_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/fota/update/status"
#define IQ_SYS_DEVCFG_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/devcfg/update/status"
#define IQ_SYS_SYS_STANDBY_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/standby/update/status"
#define IQ_SYS_RELSOM_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/relsom/update/status"
#define IQ_SYS_APPPOSTDATA_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/post/data"
#define IQ_SYS_STATUS_UPDATED_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/update/status"
//#define IQ_SYS_BIGDATA_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/bigdata"

#define IQ_HOST_ROOTCA		"-----BEGIN CERTIFICATE-----\n\
MIIDpzCCAo+gAwIBAgIEB2ncVzANBgkqhkiG9w0BAQsFADCBgzELMAkGA1UEBhMC\n\
OTExFDASBgNVBAgTC1dlc3QgQmVuZ2FsMRAwDgYDVQQHEwdLb2xrYXRhMR8wHQYD\n\
VQQKExZJcXVlc3RlciBTb2x1dGlvbnMgTExQMRMwEQYDVQQLEwpXZWIgU2VydmVy\n\
MRYwFAYDVQQDEw0xOTIuMTY4LjAuMTIzMB4XDTE5MTExNTA2MDIzMloXDTIwMDIx\n\
MzA2MDIzMlowgYMxCzAJBgNVBAYTAjkxMRQwEgYDVQQIEwtXZXN0IEJlbmdhbDEQ\n\
MA4GA1UEBxMHS29sa2F0YTEfMB0GA1UEChMWSXF1ZXN0ZXIgU29sdXRpb25zIExM\n\
UDETMBEGA1UECxMKV2ViIFNlcnZlcjEWMBQGA1UEAxMNMTkyLjE2OC4wLjEyMzCC\n\
ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKTG5saW0b/9vJjtsI/sy3ID\n\
/eCisfkgi+9IgZXLuKJ0pP57n4/5kUBQWOUjuWya5dl83n7zLj+tRW3Yf0aUIYB7\n\
gat2jQWbxQak4PQO8fP3SL6dMm0KXTU3mwVZs3yVM0H/u2MHNyu+YD4E9tjjz0sj\n\
ZdhhWSGlqpnr5nf15bcBNxdmFczcfmDJu6GRsDIpggFPu5Xi7eJHgvA3Y+3xxO9Z\n\
BDCLbyKEZ9VOlVxtlKSJ/k+LbAQA1mVlPTxot3A5AGIddKD4W6O8z3qXOSRGeX8n\n\
AOCdDyfaIqoQckbjH/73CFyd16eh8d5V9q6FiUk4KX0bjFZAuMrej7SwI4tT+N8C\n\
AwEAAaMhMB8wHQYDVR0OBBYEFDLN1gvUrUgpTBvlIqKmwHVXpBedMA0GCSqGSIb3\n\
DQEBCwUAA4IBAQB6zY2rLhI5e13/bCc6OmsP/22DYjsenPd85S41CkuxMKxl4YQI\n\
P74nLXBecE3ceDViX8e2IbUdQipe+EzE3gAW/0y6GmJ6pXHoNiBkcqXPta2oc6Qh\n\
s69nUv1ZjZ1sQbqs6Nvp+wQfe2lgGAAusIjDNEL4id8P20UKL5svc4WFH41QZgj5\n\
5LLkrjZuW6WBsqyPCca0cQisq4jq+dqoaonNQ1cC3iwQg3P1t2w3P79siAuH7WQ6\n\
Gxr/jhMB0KeAZeNsZ5qjxSZbdLIa3iMasC6Dt9OAiFPFxMaNVzKApabc0R6A4+pz\n\
QMfsY0Z6pYAu8aUovQDjwjxExUeVBm6LSlIq\n\
-----END CERTIFICATE-----"



#elif _STAGE_SERVER
#define IQ_HOST 			"dev.iquesters.com"			/*!< Core_embedded's HTTP Host name*/
#define IQ_HOST_PORT 		443												/*!< Core_embedded's HTTPs Port */
#define IQ_MAIN_GET_URL 	"https://"IQ_HOST"/emb_api/test_apis/ota.json"	/*!< Core_embedded's OTA url */
#define IQ_MAIN_POST_URL	"https://"IQ_HOST"/emb_api/hitandget.php"		/*!< Core_embedded's HTTPs post url */

#define IQ_SOM_REG_HOST 	"192.168.1.2:8080"
#define IQ_SOM_REGISTRATION_POST_URL 	"http://"IQ_SOM_REG_HOST"/mig/rest/som/register" /*!< SOM registration URL */
#define IQ_SYS_HWIDENTIFY_POST_URL		IQ_MAIN_POST_URL
#define IQ_SYS_HEARTBEAT_POST_URL		"https://"IQ_HOST"/emb_api/test_apis/api/heartbeat.php"

#define IQ_HOST_ROOTCA  "-----BEGIN CERTIFICATE-----\n\
MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n\
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n\
SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n\
GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n\
AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n\
q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n\
SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n\
Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n\
a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n\
/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n\
AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n\
CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n\
bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n\
c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n\
VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n\
ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n\
MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n\
Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n\
AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n\
uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n\
wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n\
X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n\
PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n\
KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n\
-----END CERTIFICATE-----"														/*!< Hostgator's RootCA Certificate*/

#endif


#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_URLS_H_ */
