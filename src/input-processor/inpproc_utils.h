/*
 * File Name: inpproc_utils.h
 * File Path: /emmate/src/input-processor/inpproc_utils.h
 * Description:
 *
 *  Created on: 30-Apr-2019
 *      Author: Noyel Seth
 */

#ifndef INPPROC_UTILS_H_
#define INPPROC_UTILS_H_

#include "core_error.h"
#include "core_config.h"
#if CONFIG_USE_PARSON
#include "parson.h"
#endif

/**
 * @brief 	Copies a JSON key's value from a root object to a destination buffer
 * 			The key's type must be string
 *
 * 	@param[in]	rootObj A pointer JSON root object of type JSON_Object
 * 	@param[in]	key		A JSON key
 * 	@param[out] dest	A destination buffer pointer where the key's value has to be copied.
 * 						Buffer memory must be allocated by the caller
 *
 * @return
 *
 **/
core_err cpy_json_str_obj(JSON_Object * rootObj, char* key, char* dest);

#endif /* INPPROC_UTILS_H_ */
