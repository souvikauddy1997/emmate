set(srcs
	parson.c
	)

add_library(parson STATIC ${srcs})

target_include_directories(parson 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)