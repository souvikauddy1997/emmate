/*
 * File Name: led_helper.c
 * File Path: /emmate/src/hmi/led/led_helper.c
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#include <stdarg.h>
#include <string.h>

#include "led_helper.h"
#include "led_platform.h"
#include "threading.h"
#include "heap_debug.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "module_thread_priorities.h"
#include "thing.h"

#if CONFIG_USE_LED

#define TAG	LTAG_HMI_LED

#define PRINT_LED_NOTIFICATION_INFO	0

#define CONTINUE_NOTIFICATION(period)	(period == NOTIFY_TIME_FOREVER) ? true : (--period > 0) ? true : false
#define ON	true
#define OFF	false

#if (LEDS_NUMBER > 0)
#define SET_LED_STATE(state, pin) (state == ON) ? on_bsp_board_led(pin_to_led_idx_bsp_board(pin)) : off_bsp_board_led(pin_to_led_idx_bsp_board(pin))
#else
#define SET_LED_STATE(state, pin)
#error "LEDS_NUMBER is either not defined or set to 0. Please set this MACRO in thing.h"
#endif

typedef enum {
	LED_NOTIFICATION_UNINITIALIZED = 0,
	LED_NOTIFICATION_INITIALIZING,
	LED_NOTIFICATION_INITIALIZED,
	LED_NOTIFICATION_RUNNING,
	LED_NOTIFICATION_TOBE_STOPPED,	// only used for next_state
	LED_NOTIFICATION_UNKNOWN
} LED_NOTIFICATION_STATES;

typedef struct {
	QueueHandle led_queue;
	ThreadHandle led_thread;
	LED_NOTIFICATION_STATES curr_state;
	LED_NOTIFICATION_STATES next_state;
	bool lib_init_stat;
	bool busy_stat;
} LedHelperData;

/* Static and Global Variables */
static LED_HELPER_MODE init_mode = LED_HELPER_MODE_MAX;
static LedHelperData led_helper_data[LED_HELPER_MODE_MAX] = {
								{ 0, 0, LED_NOTIFICATION_UNINITIALIZED, LED_NOTIFICATION_UNKNOWN, false, false },
								{ 0, 0, LED_NOTIFICATION_UNINITIALIZED, LED_NOTIFICATION_UNKNOWN, false, false } };

static LedNotifyData led_notify_data[LED_HELPER_MODE_MAX] = { { 0, 0, NULL }, { 0, 0, NULL } };

/**
 * Static Functions
 */
static uint32_t get_color_from_code(uint32_t code) {
	if (IS_COLOR_RED(code))
		return COLOR_RED;
	else if (IS_COLOR_GREEN(code))
		return COLOR_GREEN;
	else if (IS_COLOR_BLUE(code))
		return COLOR_BLUE;
	else if (IS_COLOR_WHITE(code))
		return COLOR_WHITE;
	else if (IS_COLOR_MAGENTA(code))
		return COLOR_MAGENTA;
	else if (IS_COLOR_YELLOW(code))
		return COLOR_YELLOW;
	else if (IS_COLOR_CYAN(code))
		return COLOR_CYAN;
	else
		return 0;
}

/**
 *
 */
static core_err set_led_state(LED_TYPE led_type, uint8_t m_r_pin, uint8_t g_pin, uint8_t b_pin, uint32_t color,
bool state) {
	/* Check type of LED */
	if (led_type == MONO_LED) {
		SET_LED_STATE(state, m_r_pin);
	} else if (led_type == RGB_LED) {
		/* Check LED color */
		switch (get_color_from_code(color)) {
		case COLOR_RED:
			SET_LED_STATE(state, m_r_pin);
			break;
		case COLOR_GREEN:
			SET_LED_STATE(state, g_pin);
			break;
		case COLOR_BLUE:
			SET_LED_STATE(state, b_pin);
			break;
		case COLOR_WHITE:
			SET_LED_STATE(state, m_r_pin);
			SET_LED_STATE(state, g_pin);
			SET_LED_STATE(state, b_pin);
			break;
		case COLOR_MAGENTA:
			SET_LED_STATE(state, m_r_pin);
			SET_LED_STATE(state, b_pin);
			break;
		case COLOR_YELLOW:
			SET_LED_STATE(state, m_r_pin);
			SET_LED_STATE(state, g_pin);
			break;
		case COLOR_CYAN:
			SET_LED_STATE(state, g_pin);
			SET_LED_STATE(state, b_pin);
			break;
		default:
			/* TODO: Call PWM module to generate other colors */
			break;
		}
	} else {
		return CORE_FAIL;
	}
	return CORE_OK;
}

/**
 *
 */
static void cleanup_LedNotifyData(LedNotifyData *nd) {
	uint8_t led_count = nd->led_count;

	CORE_LOGD(TAG, "CLEANUP: Led Count = %d", led_count);

	for (uint8_t i = 0; i < led_count; i++) {
		if (nd->led_info != NULL) {
			CORE_LOGV(TAG, "CLEANUP[%d]: led_info != NULL", i);

			if (nd->led_info[i].led_pattern != NULL) {
				set_led_state(nd->led_info[i].led_type, nd->led_info[i].m_rled_pin, nd->led_info[i].gled_pin,
						nd->led_info[i].bled_pin, nd->led_info[i].led_pattern[i].color_code, OFF);
			}

			CORE_LOGV(TAG, "CLEANUP[%d]: set_led_state done", i);

			nd->led_info[i].m_rled_pin = 0;
			nd->led_info[i].gled_pin = 0;
			nd->led_info[i].bled_pin = 0;
			nd->led_info[i].led_type = 0;
			nd->led_info[i].pattern_count = 0;

			CORE_LOGV(TAG, "CLEANUP[%d]: going to free led_pattern", i);

			free(nd->led_info[i].led_pattern);
		}
	}

	CORE_LOGV(TAG, "CLEANUP: after loop");

	nd->led_count = 0;
	nd->notify_period = 0;

	CORE_LOGV(TAG, "CLEANUP: going to free led_info");
	free(nd->led_info);
}

/**
 *
 */
#if PRINT_LED_NOTIFICATION_INFO
static void print_notification_info(LedNotifyData *nd) {
	CORE_LOGI(TAG, "\n\n-------------- LED Notification Details --------------");
	CORE_LOGI(TAG, "Led Count = %d", nd->led_count);
	CORE_LOGI(TAG, "Notify Period = %d", nd->notify_period);
	CORE_LOGI(TAG, "Printing individual Led Info:\n----------");
	for (uint8_t i = 0; i < nd->led_count; i++) {
		CORE_LOGI(TAG, "%d", i);
		CORE_LOGI(TAG, "M_R Pin = %d", nd->led_info[i].m_rled_pin);
		CORE_LOGI(TAG, "G Pin = %d", nd->led_info[i].gled_pin);
		CORE_LOGI(TAG, "B Pin = %d", nd->led_info[i].bled_pin);
		CORE_LOGI(TAG, "Led Type = %d", nd->led_info[i].led_type);
		CORE_LOGI(TAG, "Pattern Count = %d", nd->led_info[i].pattern_count);
	}
	CORE_LOGI(TAG, "-------------- LED Notification Details --------------\n\n");
}
#endif

/**
 *
 */
static void stop_blink_led_threads(int led_count, ThreadHandle *thread_id, LedNotifyData *nd) {
	/* Notification ended, stop threads and cleanup memory */
	for (int i = 0; i < led_count; i++) {
		if (thread_id[i] != NULL) {
			CORE_LOGD(TAG, "Deleting thread [%d] with ID = %p", i, thread_id[i]);
			TaskDelete(thread_id[i]);
		}
	}
	free(thread_id);

	/* Cleanup LedNotifyData */
	cleanup_LedNotifyData(nd);
}

/**
 *  Thread: Blink LED
 */
void blink_led(void * params) {
	CORE_LOGD(TAG, "blink-led task started");
	LedInfo *led = (LedInfo*) params;

	if (led == NULL) {
		CORE_LOGE(TAG, "LedInfo is NULL");
		led_helper_data[LED_HELPER_MODE_SYSTEM].next_state = LED_NOTIFICATION_TOBE_STOPPED;
		led_helper_data[LED_HELPER_MODE_APP].next_state = LED_NOTIFICATION_TOBE_STOPPED;
		TaskDelay(THREADING_MAX_DELAY);
	}

	if (led->pattern_count == 0) {
		CORE_LOGE(TAG, "Led pattern count is 0");
		led_helper_data[LED_HELPER_MODE_SYSTEM].next_state = LED_NOTIFICATION_TOBE_STOPPED;
		led_helper_data[LED_HELPER_MODE_APP].next_state = LED_NOTIFICATION_TOBE_STOPPED;
		TaskDelay(THREADING_MAX_DELAY);
	}

	while (1) {
		for (int i = 0; i < led->pattern_count; i++) {
			if (led->led_pattern != NULL) {
				for (int j = 0; j < led->led_pattern[i].repeat_count; j++) {
					set_led_state(led->led_type, led->m_rled_pin, led->gled_pin, led->bled_pin,
							led->led_pattern[i].color_code, ON);
					TaskDelay(led->led_pattern[i].on_ms / TICK_RATE_TO_MS);
					set_led_state(led->led_type, led->m_rled_pin, led->gled_pin, led->bled_pin,
							led->led_pattern[i].color_code, OFF);
					TaskDelay(led->led_pattern[i].off_ms / TICK_RATE_TO_MS);
				}
			}
		}
	}
}

/**
 * Thread: Used to start the Blink LED Thread
 */
void show_notification(void * params) {

	CORE_LOGD(TAG, "show_led_notifications task started");
	LedNotifyData nd;

	LED_HELPER_MODE mode = *(LED_HELPER_MODE *) params;
	init_mode = LED_HELPER_MODE_MAX;

	LedHelperData *lhd = &led_helper_data[mode];

	while (1) {
//		/* Check queue, if no items then continue loop */
//		if (QueueMessagesWaiting(lhd->led_queue) <= 0) {
//			TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);
//			continue;
//		}
		/* Get Notification Info from queue */
		QueueReceive(lhd->led_queue, &nd, THREADING_MAX_DELAY);

#if PRINT_LED_NOTIFICATION_INFO
		print_notification_info(&nd);
#endif

		/* Get the no. of leds */
		uint8_t led_count = nd.led_count;

		/* Allocate memory for 'n' number of ThreadHandle
		 * Each LED will have its own thread */
		ThreadHandle *thread_id = (ThreadHandle*) calloc(led_count, sizeof(ThreadHandle));
		if (thread_id == NULL) {
			CORE_LOGE(TAG, "Failed to allocate memory: %s, %d", (char*) __FILE__, __LINE__);
			return;
		}

#define BLINK_THREAD_PRIORITY	THREAD_PRIORITY_14

		BaseType thread_stat;
		int thread_creation_success_count = 0;
		for (int i = 0; i < led_count; i++) {
			thread_stat = TaskCreate(blink_led, "blink-led", TASK_STACK_SIZE_2K, &nd.led_info[i], BLINK_THREAD_PRIORITY,
					&(thread_id[i]));
			if (thread_stat == false) {
				CORE_LOGE(TAG, "Failed to create thread: %s, %d", (char*) __FILE__, __LINE__);
//				return;
			} else {
				thread_creation_success_count++;
			}
			CORE_LOGD(TAG, "Creating thread [%d] with ID = %p", i, thread_id[i]);
		}

		if (thread_creation_success_count == led_count) {
			CORE_LOGD(TAG, "All blink-led threads created successfully");
			lhd->curr_state = LED_NOTIFICATION_RUNNING;
		} else {
			CORE_LOGE(TAG, "Could not create all blink-led threads!");
			// TODO: do error handling
//			stop_blink_led_threads(led_count, thread_id, &nd);
//			continue;
		}

		while (1) {
			if (CONTINUE_NOTIFICATION(nd.notify_period)) {
				/* Check queue, if nothing then continue loop, else break */
//				if (QueueMessagesWaiting(lhd->led_queue) <= 0) {
				/* If a request for stopping the running notification arrives, then we break from the loop */
				if (/*(lhd->curr_state == LED_NOTIFICATION_RUNNING) &&*/(lhd->next_state
						== LED_NOTIFICATION_TOBE_STOPPED)) {
					CORE_LOGD(TAG, "Request to stop notification received");
					break;
				}
				TaskDelay(DELAY_100_MSEC / TICK_RATE_TO_MS);
//					continue;
//				} else {
//					/* New notification request has arrived */
//					break;
//				}
			} else {
				/* Notification time period over */
				CORE_LOGD(TAG, "Notification time period over");
				break;
			}
		}

		stop_blink_led_threads(led_count, thread_id, &nd);
		lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;	//LED_NOTIFICATION_TOBE_STOPPED;
		lhd->next_state = LED_NOTIFICATION_UNKNOWN;

//		/* Notification ended, stop threads and cleanup memory */
//		for (int i = 0; i < led_count; i++) {
//			if (thread_id[i] != NULL) {
//				CORE_LOGD(TAG, "Deleting thread [%d] with ID = %p", i, thread_id[i]);
//				TaskDelete(thread_id[i]);
//			}
//		}
//		free(thread_id);
//
//		/* Cleanup LedNotifyData */
//		cleanup_LedNotifyData(&nd);
	}
}

//static uint8_t notify_init = 0;

/**
 *
 */
core_err init_led_notification(LED_HELPER_MODE mode, uint8_t led_count, int period) {

	if (mode >= LED_HELPER_MODE_MAX) {
		return CORE_ERR_NOT_SUPPORTED;
	}

	LedHelperData *lhd = &led_helper_data[mode];
	LedNotifyData *lnd = &led_notify_data[mode];

	/* Check if notification initializer is busy */
	if (lhd->busy_stat) {
		CORE_LOGE(TAG, "init_led_notification is busy!");
		return CORE_ERR_INVALID_STATE;
	}
	lhd->busy_stat = true;

	if (lhd->lib_init_stat == false) {
		CORE_LOGE(TAG, "led helper module is not initialized!");
		return CORE_ERR_INVALID_STATE;
	}

	if (led_count < 1) {
		CORE_LOGE(TAG, "Invalid arguments passed to init_led_notification");
		return CORE_ERR_INVALID_ARG;
	}

//	if (++notify_init > 1) {
//		CORE_LOGI(TAG,
//				"One notification is already initialized, start it by calling start_led_notification() before initializing the next one");
//		return CORE_ERR_NOT_SUPPORTED;
//	}

	core_err ret = CORE_FAIL;

	/* If a notification is running, we stop it first and then initialize a new one */
	if (lhd->curr_state == LED_NOTIFICATION_RUNNING) {
		lhd->next_state = LED_NOTIFICATION_TOBE_STOPPED;
		CORE_LOGD(TAG, "Waiting for previous notification to stop");
		/* Wait until the current notification stops */
//			while (lhd->curr_state != LED_NOTIFICATION_TOBE_STOPPED) {
		while (lhd->curr_state == LED_NOTIFICATION_RUNNING) {
			TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
		}
	}

	/* We can initialize a new notification only if there is no notification or 1 is currently running */
	if ((lhd->curr_state == LED_NOTIFICATION_UNINITIALIZED)/* || (lhd->curr_state == LED_NOTIFICATION_RUNNING)*/) {

		lhd->curr_state = LED_NOTIFICATION_INITIALIZING;
		CORE_LOGD(TAG, "Initializing LED notification");

		lnd->led_count = led_count;
		lnd->notify_period = period;
		lnd->led_info = (LedInfo*) calloc(led_count, sizeof(LedInfo));
		if (lnd->led_info == NULL) {
			CORE_LOGE(TAG, "Failed to allocate memory: %s, %d", (char*) __FILE__, __LINE__);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			ret = CORE_ERR_NO_MEM;
			goto end_of_func;
		}

		/* Set to default values */
		for (uint8_t i = 0; i < led_count; i++) {
			lnd->led_info[i].idx = -1;
			lnd->led_info[i].m_rled_pin = 0;
			lnd->led_info[i].gled_pin = 0;
			lnd->led_info[i].bled_pin = 0;
			lnd->led_info[i].led_type = 0;
			lnd->led_info[i].pattern_count = 0;
			lnd->led_info[i].led_pattern = NULL;
		}

		/* Notification initialization done */
		lhd->curr_state = LED_NOTIFICATION_INITIALIZED;
		CORE_LOGD(TAG, "Initialized LED notification");
		ret = CORE_OK;
	} else {
		CORE_LOGE(TAG,
				"%d One notification is already initialized, start it by calling start_led_notification() before initializing the next one",
				lhd->curr_state);
//		cleanup_LedNotifyData(&lnd);
//		lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
		ret = CORE_ERR_NOT_SUPPORTED;
	}

	end_of_func: lhd->busy_stat = false;
	return ret;
}

/**
 *
 */
core_err set_led_type_idx(LED_HELPER_MODE mode, uint8_t led_idx, LED_TYPE type, uint8_t m_r_idx, uint8_t g_idx,
		uint8_t b_idx) {

//	if (lhd->curr_state != LED_NOTIFICATION_INITIALIZED) {
//		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
//		return CORE_ERR_INVALID_STATE;
//	}

//	if (lnd->led_info == NULL) {
//		CORE_LOGE(TAG, "led notification is not initialized!");
//		return CORE_ERR_INVALID_STATE;
//	}

	if (mode >= LED_HELPER_MODE_MAX) {
		return CORE_ERR_NOT_SUPPORTED;
	}

	LedHelperData *lhd = &led_helper_data[mode];
	LedNotifyData *lnd = &led_notify_data[mode];

	if (lhd->curr_state == LED_NOTIFICATION_INITIALIZED) {
		if (led_idx <= 0) {
			CORE_LOGE(TAG, "Invalid arguments passed to set_led_type_idx");
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_INVALID_ARG;
		}

		if (type == MONO_LED) {

		} else if (type == RGB_LED) {

		} else {
			CORE_LOGE(TAG, "Invalid arguments passed to set_led_type_idx");
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_INVALID_ARG;
		}

		uint8_t idx = led_idx - 1;
		lnd->led_info[idx].idx = idx;
		lnd->led_info[idx].led_type = type;
		lnd->led_info[idx].m_rled_pin = m_r_idx;
		lnd->led_info[idx].gled_pin = g_idx;
		lnd->led_info[idx].bled_pin = b_idx;
		lnd->led_info[idx].led_pattern = NULL;

		return CORE_OK;
	} else {
		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
		return CORE_ERR_INVALID_STATE;
	}
}

/**
 *
 */
core_err map_patterns_to_led(LED_HELPER_MODE mode, uint8_t led_idx, int argc, ...) {

//	if (lhd->curr_state != LED_NOTIFICATION_INITIALIZED) {
//		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
////		cleanup_LedNotifyData(&lnd);
//		return CORE_ERR_INVALID_STATE;
//	}

//	if (lnd->led_info == NULL) {
//		CORE_LOGE(TAG, "led notification is not initialized!");
//		return CORE_ERR_INVALID_STATE;
//	}

	if (mode >= LED_HELPER_MODE_MAX) {
		return CORE_ERR_NOT_SUPPORTED;
	}

	LedHelperData *lhd = &led_helper_data[mode];
	LedNotifyData *lnd = &led_notify_data[mode];

	if (lhd->curr_state == LED_NOTIFICATION_INITIALIZED) {
		if ((led_idx <= 0) || (argc <= 0)) {
			CORE_LOGE(TAG, "Invalid arguments passed to map_patterns_to_led");
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_INVALID_ARG;
		}

		uint8_t idx = led_idx - 1;
		/* Allocate memory for argc number of patterns */
		lnd->led_info[idx].led_pattern = (LedPattern*) calloc(argc, sizeof(LedPattern));
		if (lnd->led_info[idx].led_pattern == NULL) {
			CORE_LOGE(TAG, "Failed to allocate memory: %s, %d", (char*) __FILE__, __LINE__);
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_NO_MEM;
		}

		/* Store the pattern count */
		lnd->led_info[idx].pattern_count = argc;

		va_list valist;

		/* initialize valist for number of arguments */
		va_start(valist, argc);

		for (int i = 0; i < argc; i++) {
			/* Get the next LEDPattern from the list */
			LedPattern *pat = va_arg(valist, LedPattern*);

			/* Copy the pattern into our LedInfo structure */
			if (pat != NULL)
				memcpy(&lnd->led_info[idx].led_pattern[i], pat, sizeof(LedPattern));

			/* Free the pre-allocated pattern of the list */
			free(pat);
		}

		/* clean memory reserved for valist */
		va_end(valist);

		return CORE_OK;
	} else {
		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
//		cleanup_LedNotifyData(&lnd);
		return CORE_ERR_INVALID_STATE;
	}
}

/**
 *
 */
core_err start_led_notification(LED_HELPER_MODE mode) {

	if (mode >= LED_HELPER_MODE_MAX) {
		return CORE_ERR_NOT_SUPPORTED;
	}

	LedHelperData *lhd = &led_helper_data[mode];
	LedNotifyData *lnd = &led_notify_data[mode];
//	core_err ret = CORE_FAIL;

//	if (lhd->curr_state != LED_NOTIFICATION_INITIALIZED) {
//		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
//		return CORE_ERR_INVALID_STATE;
//	}

	if (lhd->curr_state == LED_NOTIFICATION_INITIALIZED) {
		if ((lhd->led_queue == 0) || (lnd->led_info == NULL)) {
			CORE_LOGE(TAG, "led helper module or led notification is not initialized!");
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_INVALID_STATE;
		}

		uint8_t led_count = lnd->led_count;
		bool no_pattern = false;
		for (uint8_t i = 0; i < led_count; i++) {
			if (lnd->led_info[i].pattern_count == 0) {
				no_pattern = true;
				break;
			}
		}
		if (no_pattern) {
			CORE_LOGE(TAG, "Pattern Count cannot be 0! Call map_patterns_to_led() first");
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_ERR_INVALID_STATE;
		}

		bool stat = QueueSend(lhd->led_queue, lnd, 100/TICK_RATE_TO_MS);
		if (stat == false) {
			CORE_LOGE(TAG, "QUEUE_SEND_CORE Failed: %s, %d", (char*) __FILE__, __LINE__);
			/* Cleanup LedNotifyData */
			cleanup_LedNotifyData(lnd);
			lhd->curr_state = LED_NOTIFICATION_UNINITIALIZED;
			return CORE_FAIL;
		} else {
			return CORE_OK;
		}
	} else {
		CORE_LOGE(TAG, "led notification is not initialized! Call init_led_notification() first");
		return CORE_ERR_INVALID_STATE;
	}

//	if (notify_init > 0)
//		notify_init = 0;
//	return ret;
}

/**
 *
 */
void stop_led_notification(LED_HELPER_MODE mode) {

	if (mode >= LED_HELPER_MODE_MAX) {
		return;
	}

	LedHelperData *lhd = &led_helper_data[mode];

	lhd->next_state = LED_NOTIFICATION_TOBE_STOPPED;
	CORE_LOGD(TAG, "Waiting for previous notification to stop");

	/* Wait until the current notification stops */
//			while (lhd->curr_state != LED_NOTIFICATION_TOBE_STOPPED) {
	while (lhd->curr_state == LED_NOTIFICATION_RUNNING) {
		TaskDelay(DELAY_5_MSEC / TICK_RATE_TO_MS);
	}
}

static bool led_mode_sys_init = false;
static bool led_mode_app_init = false;

core_err init_leds(LED_HELPER_MODE mode) {

	if (mode >= LED_HELPER_MODE_MAX) {
		return CORE_ERR_INVALID_ARG;
	}

	/* If a mode of SYSTEM or APP has been initialized, do not initialize again */
	if ((mode == LED_HELPER_MODE_SYSTEM) && (led_mode_sys_init == true)) {
		return CORE_ERR_INVALID_STATE;
	}
	if ((mode == LED_HELPER_MODE_APP) && (led_mode_app_init == true)) {
		return CORE_ERR_INVALID_STATE;
	}

	/* Set mode initialization variables to true */
	if (mode == LED_HELPER_MODE_SYSTEM) led_mode_sys_init = true;
	if (mode == LED_HELPER_MODE_APP) 	led_mode_app_init = true;

	init_mode = mode;

	LedHelperData *lhd = &led_helper_data[mode];

	/* If the LED Helper library is initialized once then no need to reinit again */
	if (lhd->lib_init_stat) {
		return CORE_OK;
	}

	/* Initialize the LEDs */
	init_platform_leds(1);

	/* Create a Notification Queue to receive Notification Data from other modules*/
	lhd->led_queue = QueueCreate(1, sizeof(LedNotifyData));
	if (lhd->led_queue == 0) {
		CORE_LOGE(TAG, "Failed to create queue");
		lhd->lib_init_stat = false;
		return CORE_FAIL;
	}

	/* Create a thread to perform the notifications */
	BaseType thread_stat;
	thread_stat = TaskCreate(show_notification, "led-notify", TASK_STACK_SIZE_2K, &init_mode,
			THREAD_PRIORITY_LED_HELPER, &lhd->led_thread);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create thread");
		lhd->lib_init_stat = false;
		return CORE_FAIL;
	}

	TaskDelay(DELAY_5_MSEC / TICK_RATE_TO_MS);

	CORE_LOGD(TAG, "led-helper module initialized successfully");
	/* Led Helper library initialized successfully */
	lhd->lib_init_stat = true;

	return CORE_OK;
}

#endif	/* CONFIG_USE_LED */
