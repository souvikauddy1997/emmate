/*
 * File Name: core_utils.h
 * File Path: /emmate/src/utils/core_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef CORE_UTILS_H_
#define CORE_UTILS_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

/**
 * @brief	This is a helper function which is called from the system module when the device boots
 * 			This function is meant to be used by the core framework only and not by the application
 * 			This function sets the required LOG Levels for modules. The application may use logger
 * 			module's APIs to set/reset log levels
 * */
void set_all_log_levels_at_init();

/*
 * @brief	This function is internally called by GET_VAR_NAME() to extract a variable name.
 * 			See GET_VAR_NAME() documentation for detailed usage.
 *
 * @note	This function must not be called directly. Always call GET_VAR_NAME() instead
 */
char* extract_variable_name(char *var, char* delim);

/*
 * @brief	This macro is used to extract the name of the passed variable. This works with a normal int or char variable
 * 			and also with a structure variable
 *
 * @note	Usage
 *
 * typedef struct {
 * 		bool mybool;
 * } struct2;
 *
 * typedef struct {
 * 		int sint;
 * 		struct2 st2;
 * 		struct2 *pst2;
 * 	} struct1;
 *
 * void myfunc()
 * {
 * 		int myint;
 * 		char mychar;
 * 		float myfloat;
 * 		struct1 st1;
 * 		struct1 *pst1;
 *
 * 		printf("%s\n", GET_VAR_NAME(myint, NULL));
 * 		printf("%s\n", GET_VAR_NAME(mychar, NULL));
 * 		printf("%s\n", GET_VAR_NAME(myfloat, NULL));
 * 		printf("%s\n", GET_VAR_NAME(st1.sint, "."));
 * 		printf("%s\n", GET_VAR_NAME(st1.st2, "."));
 * 		printf("%s\n", GET_VAR_NAME(st1.st2.mybool, "."));
 *
 * 		printf("%s\n", GET_VAR_NAME(pst1->st2.mybool, "."));
 * 		printf("%s\n", GET_VAR_NAME(pst1->pst2->mybool, "->"));
 *
 * 		printf("%s\n", GET_VAR_NAME(pst1->pst2->mybool, "adcbhadb"));
 * 	}
 *
 * @param[in]	var The variable whose name has to be extracted
 * @param[in]	delim Pass either "." or "->" is structure variable name is required else pass NULL, see example
 *
 * @return 		char* Returns the extracted name of the variable if delim was ok else returns NULL is pass delim was not found
 */
#define GET_VAR_NAME(var, delim)		extract_variable_name(#var, delim)

/*
 * @brief
 *
 * @param[in]	mac
 * @param[out]	fmt_mac
 *
 * @return		0	Successfully formatted the input MAC
 * 				-1	Either input param 'mac' or the output param 'fmt_mac' is NULL
 * 				1	The input param 'mac' has all zeros. In this case formatting will not be done
 * */
int format_mac(uint8_t *mac, char *fmt_mac);

/**
 * @brief	Converts an array of uint8_t to a string represented as hexadecimal numbers.
 * 			Each byte is denoted as 2 HEX characters. See the below usage for better understanding
 *
 * Usage
 * 		#define ID_SZ	8
 * 		uint8_t hex_arr[ID_SZ] = { 0x3a, 0x30, 0x2a, 0x4e, 0x0, 0x0, 0x0, 0xc2 };
 * 		char str[(ID_SZ * 2) + 1] = { 0 };
 *
 * 		hexbytearray_to_str(hex_arr, sizeof(hex_arr), str);
 * 		printf("HEX_STR = %s\n", str);
 *
 * 		Output: HEX_STR = 3a302a4e000000c2
 *
 *
 * @param[in]	in_hex_arr 		uint8_t array having the HEX data
 * @param[in]	num_bytes 		Number of bytes to be converted into HEX string
 * @param[out]	out_hex_str		The string to be populated. Memory must be allocated by caller
 *
 * @note
 * 			1. Size of 'out_hex_str' must be greater than or equal to (num_bytes*2)+1
 * 			2. Memory allocation of 'out_hex_str' must be done before calling this function
 * 			3. If the size of 'out_hex_str' is less than (num_bytes*2)+1, then the resulting string may be erroneous
 *
 * @return
 *		- CORE_OK       			Successful
 *  	- CORE_ERR_INVALID_ARG     	If num_bytes is <= 0 or pointers are NULL
 */
core_err hexbytearray_to_str(uint8_t *in_hex_arr, size_t num_bytes, char *out_hex_str/*, size_t str_size*/);

/**
 * @brief	Converts a string containing data represented as hexadecimal numbers in to an array of uint8_t
 *
 * Usage
 * 		#define ID_SZ	8
 * 		char hex_str = "3a302a4e000000c2";
 * 		uint8_t hex_arr[ID_SZ] = { 0 };
 *
 * 		str_to_hexbytearray(hex_str, hex_arr, sizeof(hex_arr));
 * 		printf("HEX_ARR = %02x %02x %02x %02x %02x %02x %02x %02x\n", hex_arr[0], hex_arr[1], hex_arr[2], hex_arr[3], hex_arr[4],
 * 				hex_arr[5], hex_arr[6], hex_arr[7]);
 *
 * 		Output: HEX_ARR = 3a 30 2a 4e 00 00 00 c2
 *
 *
 * @param[in]	in_hex_str 		String containing data represented as hexadecimal numbers
 * @param[out]	out_hex_arr		The array of uint8_t to be populated. Memory must be allocated by caller
 * @param[in]	num_bytes		Number of bytes to be converted from the in_hex_str. Each byte = 2 chars of in_hex_str
 *
 * @note
 * 			1. Size of 'out_hex_arr' must be greater than or equal to (num_bytes/2)
 * 			2. Memory allocation of 'out_hex_arr' must be done before calling this function
 *
 * @return
 *		- CORE_OK       			Successful
 *  	- CORE_ERR_INVALID_ARG     	If num_bytes is <= 0 or pointers are NULL
 */
core_err str_to_hexbytearray(char *in_hex_str, /*size_t str_size,*/ uint8_t *out_hex_arr, size_t num_bytes);


#endif /* CORE_UTILS_H_ */
