add_library(emmate_config INTERFACE)

# Common libraries to be exposed to all emmate applications
#	- common
#	- hmi
#	- logging
#	- peripherals
#	- persistent
#	- system
#	- threading
#	- utils

# Private linkages


#if(CONFIG_USE_COMMON)
#	list(APPEND EXTRA_LIBS common)
#	if(CONFIG_USE_ERRORS)
#		list(APPEND EXTRA_LIBS errors)
#	endif()
#endif()
#if(CONFIG_USE_LOGGING)
#	list(APPEND EXTRA_LIBS logging)
#endif()
#if(CONFIG_USE_THREADING)
#	list(APPEND EXTRA_LIBS threading)
#endif()


list(APPEND EXTRA_LIBS 
				common
				logging
				peripherals
				system
				threading
				utils
				)
if(CONFIG_USE_HMI)
	list(APPEND EXTRA_LIBS hmi)
endif()
if(CONFIG_USE_PERSISTENT)
	list(APPEND EXTRA_LIBS persistent)
endif()

# Link the libraries
target_link_libraries(emmate_config INTERFACE ${EXTRA_LIBS})

target_include_directories(emmate_config 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)