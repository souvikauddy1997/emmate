/*
 * File Name: spi_slave_platform.c
 *
 * Description:
 *
 *  Created on: 30-Sep-2019
 *      Author: Noyel Seth
 */

#include "spi_core_common.h"
#include "spi_slave_platform.h"
#include "threading.h"
#include <string.h>

/**
 *
 */
core_err spi_slave_pf_device_interface_config(uint8_t host, int miso_io_num, int mosi_io_num, int sclk_io_num,
		int cs_io_num) {
	core_err ret = CORE_FAIL;

	//Configuration for the SPI bus
	spi_bus_config_t buscfg = { .mosi_io_num = mosi_io_num, .miso_io_num = miso_io_num, .sclk_io_num = sclk_io_num };

	//Configuration for the SPI slave interface
	spi_slave_interface_config_t slvcfg = { .spics_io_num = cs_io_num };

	//Initialize SPI slave interface
	ret = spi_slave_pf_initialize(host, &buscfg, &slvcfg, 1);

	return ret;
}

/**
 *
 */
core_err spi_slave_pf_device_transmit(uint8_t host, void * tx_data, void* rx_data,
		size_t tx_length_in_byte) {
	core_err ret = CORE_FAIL;

	spi_slave_pf_transaction_t tran_data;
	memset(&tran_data, 0, sizeof(tran_data));

	tran_data.length = tx_length_in_byte * 8;
	tran_data.tx_buffer = tx_data;
	tran_data.rx_buffer = rx_data;

	ret=spi_slave_pf_transmit(host, (spi_slave_transaction_t *)&tran_data, THREADING_MAX_DELAY);

	return ret;
}


/**
 *
 */
core_err spi_slave_pf_bus_free(uint8_t host) {
	core_err ret = CORE_FAIL;
	ret = spi_slave_pf_free(host);
	return ret;
}
