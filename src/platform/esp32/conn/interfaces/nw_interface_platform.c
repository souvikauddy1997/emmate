/*
 * File Name: nw_interface_platform.c
 * File Path: /emmate/src/platform/esp/conn/nw_interface_platform.c
 * Description:
 *
 *  Created on: 05-May-2019
 *      Author: Rohan Dey
 */

#include "nw_interface_platform.h"

core_err get_nw_iface_mac_platform(plat_nw_interface ifx, uint8_t *mac) {
	core_err ret = esp_wifi_get_mac(ifx, mac);
	if (ret != CORE_OK) {
		CORE_LOGE(LTAG_CONN, "get_nw_iface_mac_platform failed: err = %d", ret);
	}
	return ret;
}
