#include "core_config.h"

#define UNASSIGNED_PIN	-1

#if CONFIG_SOM_ESP32_DEVKIT_C_V4

/*
 * This Header file is to represent the SOM pin connection with the Microcontroller gpio pins.
 * This will is also reflect the functions in comment section.
 *
 * SOM pin header for ESP32 chip
 *
 * Created By:- Sourav Gupta (Hardware desk)
 * Created For:-Embeded Team
 * -------------------------------------------------------------------------------------------
 * Date:- 7.09.2019
 * Revision:- 1 . Initial pin paths located in the header files
 *
 *
 */

/*Analog pins*/

/*This pins can only be used as input*/
#define SOM_PIN_2 		39 //ADC1 Channel 3
#define SOM_PIN_8 		36
//------------------------------------------------------------------------------------------------------------------------------------------------

/*GPIO*/
#define SOM_PIN_79		16	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_85		17	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_97		21
#define SOM_PIN_98		22
#define SOM_PIN_192		2
#define SOM_PIN_101		25
#define SOM_PIN_25		26
#define SOM_PIN_27		18
#define SOM_PIN_47		14
#define SOM_PIN_49		4
#define SOM_PIN_51		12      /*Firmware Reset - Soft Reset*/
#define SOM_PIN_53		13
#define /*SOM_PIN_190*/ SOM_PIN_33		15

/*UART Pins*/
//UART 1/2
#define SOM_PIN_36 		34 // RXD
#define SOM_PIN_38 		 0 // TXD

//UART 0

#define SOM_PIN_190 /*SOM_PIN_33*/ 		 3 // RXD
#define SOM_PIN_35		 1 // TXD
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SPI Pins*/
#define SOM_PIN_86 		33 // CS
#define SOM_PIN_88 		32 // CLK
#define SOM_PIN_90 		35 // MISO / RXD
#define SOM_PIN_92 		19 // MOSI / TXD
//------------------------------------------------------------------------------------------------------------------------------------------------

/*I2C*/
#define SOM_PIN_194 	23	//SDA
#define SOM_PIN_196 	 5	//SCL
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Power Pins*/
/*This pins are used for power delivery across the SOM, hence not reflecting across the SOM SODIMM module as gpios*/
#define SOM_PIN_42  UNASSIGNED_PIN //3.3V
#define SOM_PIN_84  UNASSIGNED_PIN //3.3V
#define SOM_PIN_108 UNASSIGNED_PIN //3.3V
#define SOM_PIN_148 UNASSIGNED_PIN //3.3V
#define SOM_PIN_198 UNASSIGNED_PIN //3.3V
#define SOM_PIN_200 UNASSIGNED_PIN //3.3V
#define SOM_PIN_39  UNASSIGNED_PIN //GND
#define SOM_PIN_41  UNASSIGNED_PIN //GND
#define SOM_PIN_83  UNASSIGNED_PIN //GND
#define SOM_PIN_109 UNASSIGNED_PIN //GND
#define SOM_PIN_147 UNASSIGNED_PIN //GND
#define SOM_PIN_181 UNASSIGNED_PIN //GND
#define SOM_PIN_197 UNASSIGNED_PIN //GND
#define SOM_PIN_199 UNASSIGNED_PIN //GND

/*Special Purpose Pins*/
#define SOM_PIN_26  UNASSIGNED_PIN //EN //This pin is used for reset the ESP module.
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Unused Pins*/
#define SOM_PIN_3		25
#define SOM_PIN_4		27
#define SOM_PIN_5		UNASSIGNED_PIN
#define SOM_PIN_6		UNASSIGNED_PIN
#define SOM_PIN_1		UNASSIGNED_PIN
#define SOM_PIN_7		UNASSIGNED_PIN
#define SOM_PIN_9		UNASSIGNED_PIN
#define SOM_PIN_10		UNASSIGNED_PIN
#define SOM_PIN_11		UNASSIGNED_PIN
#define SOM_PIN_12		UNASSIGNED_PIN
#define SOM_PIN_13		UNASSIGNED_PIN
#define SOM_PIN_14		UNASSIGNED_PIN
#define SOM_PIN_15		UNASSIGNED_PIN
#define SOM_PIN_16		UNASSIGNED_PIN
#define SOM_PIN_17		UNASSIGNED_PIN
#define SOM_PIN_18		UNASSIGNED_PIN
#define SOM_PIN_19		UNASSIGNED_PIN
#define SOM_PIN_20      UNASSIGNED_PIN
#define SOM_PIN_21      UNASSIGNED_PIN
#define SOM_PIN_22      UNASSIGNED_PIN
#define SOM_PIN_23      UNASSIGNED_PIN
#define SOM_PIN_24      UNASSIGNED_PIN
#define SOM_PIN_28      UNASSIGNED_PIN
#define SOM_PIN_30      UNASSIGNED_PIN
#define SOM_PIN_31      UNASSIGNED_PIN
#define SOM_PIN_32      UNASSIGNED_PIN
#define SOM_PIN_34      UNASSIGNED_PIN
#define SOM_PIN_37      UNASSIGNED_PIN
#define SOM_PIN_40      UNASSIGNED_PIN
#define SOM_PIN_43      UNASSIGNED_PIN
#define SOM_PIN_44      UNASSIGNED_PIN
#define SOM_PIN_45      UNASSIGNED_PIN
#define SOM_PIN_46      UNASSIGNED_PIN
#define SOM_PIN_48      UNASSIGNED_PIN
#define SOM_PIN_50      UNASSIGNED_PIN
#define SOM_PIN_52      UNASSIGNED_PIN
#define SOM_PIN_54      UNASSIGNED_PIN
#define SOM_PIN_55      UNASSIGNED_PIN
#define SOM_PIN_56      UNASSIGNED_PIN
#define SOM_PIN_57      UNASSIGNED_PIN
#define SOM_PIN_58      UNASSIGNED_PIN
#define SOM_PIN_59      UNASSIGNED_PIN
#define SOM_PIN_60      UNASSIGNED_PIN
#define SOM_PIN_61      UNASSIGNED_PIN
#define SOM_PIN_62      UNASSIGNED_PIN
#define SOM_PIN_63      UNASSIGNED_PIN
#define SOM_PIN_64      UNASSIGNED_PIN
#define SOM_PIN_65      UNASSIGNED_PIN
#define SOM_PIN_66      UNASSIGNED_PIN
#define SOM_PIN_67      UNASSIGNED_PIN
#define SOM_PIN_68      UNASSIGNED_PIN
#define SOM_PIN_69      UNASSIGNED_PIN
#define SOM_PIN_70      UNASSIGNED_PIN
#define SOM_PIN_71      UNASSIGNED_PIN
#define SOM_PIN_72      UNASSIGNED_PIN
#define SOM_PIN_73      UNASSIGNED_PIN
#define SOM_PIN_74      UNASSIGNED_PIN
#define SOM_PIN_75      UNASSIGNED_PIN
#define SOM_PIN_76      UNASSIGNED_PIN
#define SOM_PIN_77      UNASSIGNED_PIN
#define SOM_PIN_78      UNASSIGNED_PIN
#define SOM_PIN_80      UNASSIGNED_PIN
#define SOM_PIN_81      UNASSIGNED_PIN
#define SOM_PIN_82      UNASSIGNED_PIN
#define SOM_PIN_87      UNASSIGNED_PIN
#define SOM_PIN_89      UNASSIGNED_PIN
#define SOM_PIN_91      UNASSIGNED_PIN
#define SOM_PIN_93      UNASSIGNED_PIN
#define SOM_PIN_94      UNASSIGNED_PIN
#define SOM_PIN_95      UNASSIGNED_PIN
#define SOM_PIN_96      UNASSIGNED_PIN
#define SOM_PIN_99      UNASSIGNED_PIN
#define SOM_PIN_100     UNASSIGNED_PIN
#define SOM_PIN_102     UNASSIGNED_PIN
#define SOM_PIN_103     UNASSIGNED_PIN
#define SOM_PIN_104     UNASSIGNED_PIN
#define SOM_PIN_105     UNASSIGNED_PIN
#define SOM_PIN_106     UNASSIGNED_PIN
#define SOM_PIN_107     UNASSIGNED_PIN
#define SOM_PIN_110     UNASSIGNED_PIN
#define SOM_PIN_112     UNASSIGNED_PIN
#define SOM_PIN_111     UNASSIGNED_PIN
#define SOM_PIN_113     UNASSIGNED_PIN
#define SOM_PIN_114     UNASSIGNED_PIN
#define SOM_PIN_115     UNASSIGNED_PIN
#define SOM_PIN_116     UNASSIGNED_PIN
#define SOM_PIN_117     UNASSIGNED_PIN
#define SOM_PIN_118     UNASSIGNED_PIN
#define SOM_PIN_119     UNASSIGNED_PIN
#define SOM_PIN_120     UNASSIGNED_PIN
#define SOM_PIN_121     UNASSIGNED_PIN
#define SOM_PIN_122     UNASSIGNED_PIN
#define SOM_PIN_123     UNASSIGNED_PIN
#define SOM_PIN_124     UNASSIGNED_PIN
#define SOM_PIN_125     UNASSIGNED_PIN
#define SOM_PIN_126     UNASSIGNED_PIN
#define SOM_PIN_127     UNASSIGNED_PIN
#define SOM_PIN_128     UNASSIGNED_PIN
#define SOM_PIN_129     UNASSIGNED_PIN
#define SOM_PIN_130     UNASSIGNED_PIN
#define SOM_PIN_131     UNASSIGNED_PIN
#define SOM_PIN_132     UNASSIGNED_PIN
#define SOM_PIN_133     UNASSIGNED_PIN
#define SOM_PIN_134     UNASSIGNED_PIN
#define SOM_PIN_135     UNASSIGNED_PIN
#define SOM_PIN_136     UNASSIGNED_PIN
#define SOM_PIN_137     UNASSIGNED_PIN
#define SOM_PIN_138     UNASSIGNED_PIN
#define SOM_PIN_139     UNASSIGNED_PIN
#define SOM_PIN_140     UNASSIGNED_PIN
#define SOM_PIN_141     UNASSIGNED_PIN
#define SOM_PIN_142     UNASSIGNED_PIN
#define SOM_PIN_143     UNASSIGNED_PIN
#define SOM_PIN_144     UNASSIGNED_PIN
#define SOM_PIN_145     UNASSIGNED_PIN
#define SOM_PIN_146     UNASSIGNED_PIN
#define SOM_PIN_149     UNASSIGNED_PIN
#define SOM_PIN_150     UNASSIGNED_PIN
#define SOM_PIN_151     UNASSIGNED_PIN
#define SOM_PIN_152     UNASSIGNED_PIN
#define SOM_PIN_153     UNASSIGNED_PIN
#define SOM_PIN_154     UNASSIGNED_PIN
#define SOM_PIN_155     UNASSIGNED_PIN
#define SOM_PIN_156     UNASSIGNED_PIN
#define SOM_PIN_157     UNASSIGNED_PIN
#define SOM_PIN_158     UNASSIGNED_PIN
#define SOM_PIN_159     UNASSIGNED_PIN
#define SOM_PIN_160     UNASSIGNED_PIN
#define SOM_PIN_161     UNASSIGNED_PIN
#define SOM_PIN_162     UNASSIGNED_PIN
#define SOM_PIN_163     UNASSIGNED_PIN
#define SOM_PIN_164     UNASSIGNED_PIN
#define SOM_PIN_165     UNASSIGNED_PIN
#define SOM_PIN_166     UNASSIGNED_PIN
#define SOM_PIN_167     UNASSIGNED_PIN
#define SOM_PIN_168     UNASSIGNED_PIN
#define SOM_PIN_169     UNASSIGNED_PIN
#define SOM_PIN_170     UNASSIGNED_PIN
#define SOM_PIN_171     UNASSIGNED_PIN
#define SOM_PIN_172     UNASSIGNED_PIN
#define SOM_PIN_173     UNASSIGNED_PIN
#define SOM_PIN_175     UNASSIGNED_PIN
#define SOM_PIN_177     UNASSIGNED_PIN
#define SOM_PIN_179     UNASSIGNED_PIN
#define SOM_PIN_182     UNASSIGNED_PIN
#define SOM_PIN_184     UNASSIGNED_PIN
#define SOM_PIN_186     UNASSIGNED_PIN
#define SOM_PIN_188     UNASSIGNED_PIN
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//The END.

#elif CONFIG_SOM_ESP32_V2_1
/*
 * This Header file is to represent the SOM pin connection with the Microcontroller gpio pins.
 * This will is also reflect the functions in comment section.
 *
 * SOM pin header for ESP32 chip
 *
 * Created By:- Sourav Gupta (Hardware desk)
 * Created For:-Embeded Team
 * -------------------------------------------------------------------------------------------
 * Date:- 05.02.2020
 * Revision:- 3
 *
 *
 */

/*Analog pins*/

/*This pins can only be used as input*/
#define SOM_PIN_2 		39 //ADC1 Channel 3
#define SOM_PIN_8 		36 //ADC1 Channel 0
//------------------------------------------------------------------------------------------------------------------------------------------------

/*EmMate Relate*/
#define SOM_PIN_98       	12     	/*Factory Reset*/
#define SOM_PIN_100      	25     	/*RED*/
#define SOM_PIN_102      	26	/*GREEN*/
#define SOM_PIN_104      	18	/*BLUE*/

/*PWM*/
#define SOM_PIN_30       	02
#define SOM_PIN_59       	13

/*GPIO*/
#define SOM_PIN_43       	17	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_45       	15
#define SOM_PIN_55       	16	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_69       	14
#define SOM_PIN_71       	4
#define SOM_PIN_73       	22
#define SOM_PIN_97		21

/*UART Pins*/
//UART 1/2
#define SOM_PIN_36 		34 // RXD
#define SOM_PIN_38 		 0 // TXD

//UART 0

#define SOM_PIN_33 		 3 // RXD
#define SOM_PIN_35		 1 // TXD
#define SOM_PIN_25		26 //CTS
#define SOM_PIN_27		18 // RTS
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SPI Pins*/
#define SOM_PIN_86 		33 // CS
#define SOM_PIN_88 		32 // CLK
#define SOM_PIN_90 		35 // MISO / RXD
#define SOM_PIN_92 		19 // MOSI / TXD
#define SOM_PIN_105		27 // CS0
#define SOM_PIN_106      	25 // CS2
//------------------------------------------------------------------------------------------------------------------------------------------------

/*I2C*/
#define SOM_PIN_194 	23	//SDA
#define SOM_PIN_196 	 5	//SCL
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Power Pins*/
/*This pins are used for power delivery across the SOM, hence not reflecting across the SOM SODIMM module as gpios*/
#define SOM_PIN_42  	UNASSIGNED_PIN 	//3.3V
#define SOM_PIN_84  	UNASSIGNED_PIN 	//3.3V
#define SOM_PIN_108 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_148 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_198 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_200 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_39  	UNASSIGNED_PIN	//GND
#define SOM_PIN_41  	UNASSIGNED_PIN	//GND
#define SOM_PIN_83  	UNASSIGNED_PIN	//GND
#define SOM_PIN_109 	UNASSIGNED_PIN	//GND
#define SOM_PIN_147 	UNASSIGNED_PIN	//GND
#define SOM_PIN_181 	UNASSIGNED_PIN	//GND
#define SOM_PIN_197 	UNASSIGNED_PIN	//GND
#define SOM_PIN_199 	UNASSIGNED_PIN	//GND

/*Special Purpose Pins*/
#define SOM_PIN_26  //EN //This pin is used for reset the ESP module.
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Unused Pins*/
#define SOM_PIN_3        UNASSIGNED_PIN
#define SOM_PIN_4	 	 UNASSIGNED_PIN
#define SOM_PIN_5        UNASSIGNED_PIN
#define SOM_PIN_6        UNASSIGNED_PIN
#define SOM_PIN_1        UNASSIGNED_PIN
#define SOM_PIN_7        UNASSIGNED_PIN
#define SOM_PIN_9        UNASSIGNED_PIN
#define SOM_PIN_10       UNASSIGNED_PIN
#define SOM_PIN_11       UNASSIGNED_PIN
#define SOM_PIN_12       UNASSIGNED_PIN
#define SOM_PIN_13       UNASSIGNED_PIN
#define SOM_PIN_14       UNASSIGNED_PIN
#define SOM_PIN_15       UNASSIGNED_PIN
#define SOM_PIN_16       UNASSIGNED_PIN
#define SOM_PIN_17       UNASSIGNED_PIN
#define SOM_PIN_18       UNASSIGNED_PIN
#define SOM_PIN_19       UNASSIGNED_PIN
#define SOM_PIN_20       UNASSIGNED_PIN
#define SOM_PIN_21       UNASSIGNED_PIN
#define SOM_PIN_22       UNASSIGNED_PIN
#define SOM_PIN_23       UNASSIGNED_PIN
#define SOM_PIN_24       UNASSIGNED_PIN
#define SOM_PIN_28       UNASSIGNED_PIN

#define SOM_PIN_31       UNASSIGNED_PIN
#define SOM_PIN_32       UNASSIGNED_PIN
#define SOM_PIN_34       UNASSIGNED_PIN
#define SOM_PIN_37       UNASSIGNED_PIN
#define SOM_PIN_40       UNASSIGNED_PIN
#define SOM_PIN_44       UNASSIGNED_PIN
#define SOM_PIN_46       UNASSIGNED_PIN
#define SOM_PIN_47		 UNASSIGNED_PIN
#define SOM_PIN_48       UNASSIGNED_PIN
#define SOM_PIN_49		 UNASSIGNED_PIN
#define SOM_PIN_50       UNASSIGNED_PIN
#define SOM_PIN_51		 UNASSIGNED_PIN
#define SOM_PIN_52       UNASSIGNED_PIN
#define SOM_PIN_53		 UNASSIGNED_PIN
#define SOM_PIN_54       UNASSIGNED_PIN

#define SOM_PIN_56       UNASSIGNED_PIN
#define SOM_PIN_57       UNASSIGNED_PIN
#define SOM_PIN_58       UNASSIGNED_PIN

#define SOM_PIN_60       UNASSIGNED_PIN
#define SOM_PIN_61       UNASSIGNED_PIN
#define SOM_PIN_62       UNASSIGNED_PIN
#define SOM_PIN_63       UNASSIGNED_PIN
#define SOM_PIN_64       UNASSIGNED_PIN
#define SOM_PIN_65       UNASSIGNED_PIN
#define SOM_PIN_66       UNASSIGNED_PIN
#define SOM_PIN_67       UNASSIGNED_PIN
#define SOM_PIN_68       UNASSIGNED_PIN

#define SOM_PIN_70       UNASSIGNED_PIN

#define SOM_PIN_72       UNASSIGNED_PIN

#define SOM_PIN_74       UNASSIGNED_PIN
#define SOM_PIN_75       UNASSIGNED_PIN
#define SOM_PIN_76       UNASSIGNED_PIN
#define SOM_PIN_77       UNASSIGNED_PIN
#define SOM_PIN_78       UNASSIGNED_PIN
#define SOM_PIN_79	 	 UNASSIGNED_PIN
#define SOM_PIN_80       UNASSIGNED_PIN
#define SOM_PIN_81       UNASSIGNED_PIN
#define SOM_PIN_82       UNASSIGNED_PIN
#define SOM_PIN_85	 	 UNASSIGNED_PIN
#define SOM_PIN_87       UNASSIGNED_PIN
#define SOM_PIN_89       UNASSIGNED_PIN
#define SOM_PIN_91       UNASSIGNED_PIN
#define SOM_PIN_93       UNASSIGNED_PIN
#define SOM_PIN_94       UNASSIGNED_PIN
#define SOM_PIN_95       UNASSIGNED_PIN

#define SOM_PIN_99       UNASSIGNED_PIN

#define SOM_PIN_101	 	 UNASSIGNED_PIN

#define SOM_PIN_103      UNASSIGNED_PIN

#define SOM_PIN_107      UNASSIGNED_PIN
#define SOM_PIN_110      UNASSIGNED_PIN
#define SOM_PIN_112      UNASSIGNED_PIN
#define SOM_PIN_111      UNASSIGNED_PIN
#define SOM_PIN_113      UNASSIGNED_PIN
#define SOM_PIN_114      UNASSIGNED_PIN
#define SOM_PIN_115      UNASSIGNED_PIN
#define SOM_PIN_116      UNASSIGNED_PIN
#define SOM_PIN_117      UNASSIGNED_PIN
#define SOM_PIN_118      UNASSIGNED_PIN
#define SOM_PIN_119      UNASSIGNED_PIN
#define SOM_PIN_120      UNASSIGNED_PIN
#define SOM_PIN_121      UNASSIGNED_PIN
#define SOM_PIN_122      UNASSIGNED_PIN
#define SOM_PIN_123      UNASSIGNED_PIN
#define SOM_PIN_124      UNASSIGNED_PIN
#define SOM_PIN_125      UNASSIGNED_PIN
#define SOM_PIN_126      UNASSIGNED_PIN
#define SOM_PIN_127      UNASSIGNED_PIN
#define SOM_PIN_128      UNASSIGNED_PIN
#define SOM_PIN_129      UNASSIGNED_PIN
#define SOM_PIN_130      UNASSIGNED_PIN
#define SOM_PIN_131      UNASSIGNED_PIN
#define SOM_PIN_132      UNASSIGNED_PIN
#define SOM_PIN_133      UNASSIGNED_PIN
#define SOM_PIN_134      UNASSIGNED_PIN
#define SOM_PIN_135      UNASSIGNED_PIN
#define SOM_PIN_136      UNASSIGNED_PIN
#define SOM_PIN_137      UNASSIGNED_PIN
#define SOM_PIN_138      UNASSIGNED_PIN
#define SOM_PIN_139      UNASSIGNED_PIN
#define SOM_PIN_140      UNASSIGNED_PIN
#define SOM_PIN_141      UNASSIGNED_PIN
#define SOM_PIN_142      UNASSIGNED_PIN
#define SOM_PIN_143      UNASSIGNED_PIN
#define SOM_PIN_144      UNASSIGNED_PIN
#define SOM_PIN_145      UNASSIGNED_PIN
#define SOM_PIN_146      UNASSIGNED_PIN
#define SOM_PIN_149      UNASSIGNED_PIN
#define SOM_PIN_150      UNASSIGNED_PIN
#define SOM_PIN_151      UNASSIGNED_PIN
#define SOM_PIN_152      UNASSIGNED_PIN
#define SOM_PIN_153      UNASSIGNED_PIN
#define SOM_PIN_154      UNASSIGNED_PIN
#define SOM_PIN_155      UNASSIGNED_PIN
#define SOM_PIN_156      UNASSIGNED_PIN
#define SOM_PIN_157      UNASSIGNED_PIN
#define SOM_PIN_158      UNASSIGNED_PIN
#define SOM_PIN_159      UNASSIGNED_PIN
#define SOM_PIN_160      UNASSIGNED_PIN
#define SOM_PIN_161      UNASSIGNED_PIN
#define SOM_PIN_162      UNASSIGNED_PIN
#define SOM_PIN_163      UNASSIGNED_PIN
#define SOM_PIN_164      UNASSIGNED_PIN
#define SOM_PIN_165      UNASSIGNED_PIN
#define SOM_PIN_166      UNASSIGNED_PIN
#define SOM_PIN_167      UNASSIGNED_PIN
#define SOM_PIN_168      UNASSIGNED_PIN
#define SOM_PIN_169      UNASSIGNED_PIN
#define SOM_PIN_170      UNASSIGNED_PIN
#define SOM_PIN_171      UNASSIGNED_PIN
#define SOM_PIN_172      UNASSIGNED_PIN
#define SOM_PIN_173      UNASSIGNED_PIN
#define SOM_PIN_175      UNASSIGNED_PIN
#define SOM_PIN_177      UNASSIGNED_PIN
#define SOM_PIN_179      UNASSIGNED_PIN
#define SOM_PIN_182      UNASSIGNED_PIN
#define SOM_PIN_184      UNASSIGNED_PIN
#define SOM_PIN_186      UNASSIGNED_PIN
#define SOM_PIN_188      UNASSIGNED_PIN
#define SOM_PIN_190	 	 UNASSIGNED_PIN
#define SOM_PIN_192	 	 UNASSIGNED_PIN
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//The END.

#elif CONFIG_SOM_ESP32_ETH
/*
 * This Header file is to represent the SOM pin connection with the Microcontroller gpio pins.
 * This will is also reflect the functions in comment section.
 *
 * SOM pin header for ESP32 with ETH chip
 *
 * Created By:- Sourav Gupta (Hardware desk)
 * Created For:-Embeded Team
 * -------------------------------------------------------------------------------------------
 * Date:- 3.06.2019
 * Revision:- 1 . Initial pin paths located in the header files
 *
 *
 */

/*Analog pins*/

/*This pins can only be used as input*/
#define SOM_PIN_2 		39 //ADC1 Channel 3
//------------------------------------------------------------------------------------------------------------------------------------------------

/*GPIO*/
#define SOM_PIN_79		16	// GPIO /* Used in the framework as System's GREEN LED */ /* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_85		17	// GPIO /* Used in the framework as System's BLUE LED */ /* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_97		2 	// GPIO /* Used in the framework as System's RED/MONO LED */
#define SOM_PIN_101		36 	// GPIO /* Used in the framework as System's Factory Reset Button */

/*UART Pins*/
//UART 1/2
#define SOM_PIN_36 		34 // RXD
#define SOM_PIN_38 		14 // TXD

//UART 0
#define SOM_PIN_27		18 // RTS
#define SOM_PIN_29		 5 // DSR /*This pin is also used in I2C*/
#define SOM_PIN_33 		 3 // RXD
#define SOM_PIN_35		 1 // TXD
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SPI Pins*/
#define SOM_PIN_86 		33 // CS  /*This pin is also used in I2S*/
#define SOM_PIN_88 		32 // CLK /*This pin is also used in I2S*/
#define SOM_PIN_90 		35 // MISO
#define SOM_PIN_92 		12 // MOSI
//------------------------------------------------------------------------------------------------------------------------------------------------

/*I2S Pins*/
#define SOM_PIN_174		32 // LRCLK /*This pin is also used in SPI*/

//***This pin (GPIO33) is dupilcated for IN/OUT operation in I2S. However, colibri T20 module employs DIN and DOUT as separate pins, but in ESP32 this are a single pin which have both functions***//
#define SOM_PIN_176		33 // DIN   /*This pin is also used in SPI*/
#define SOM_PIN_178		33 // DOUT  /*This pin is also used in SPI*/
#define SOM_PIN_180		23 //BCLK   /*This pin is also used in I2C*/
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SD CARD*/
/* NOT SUPPORTED. Use SPI Mode to interface SDMCC Cards */
//------------------------------------------------------------------------------------------------------------------------------------------------
/*I2C*/
#define SOM_PIN_194 	23	//SDA  /* This pin is also used in I2S */
#define SOM_PIN_196 	 5	//SCL  /* This pin is also used in UART 0 DSR */
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Ethernet*/
/*This pins are internally connected with the Ethernet hardware in the SOM, hence not reflecting across the SOM SODIMM module as a gpios*/
#define SOM_PIN_98		0	// 50 MHz clock to be provided as input to this pin
#define SOM_PIN_53		13	// PHY PWR Control 	/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_190		15	// MDC				/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_49		 4	// MDIO				/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_183 // Ethernet Link Status LED
#define SOM_PIN_185 // Ethernet Speed Status LED
#define SOM_PIN_187 // Ethernet TXO-
#define SOM_PIN_189 // Ethernet TXO+
#define SOM_PIN_191 // Ethernet GND
#define SOM_PIN_193 // Ethernet RXI-
#define SOM_PIN_195 // Ethernet RXI+
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Power Pins*/
/*This pins are used for power delivery across the SOM, hence not reflecting across the SOM SODIMM module as gpios*/
#define SOM_PIN_42  	UNASSIGNED_PIN 	//3.3V
#define SOM_PIN_84  	UNASSIGNED_PIN 	//3.3V
#define SOM_PIN_108 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_148 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_198 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_200 	UNASSIGNED_PIN	//3.3V
#define SOM_PIN_39  	UNASSIGNED_PIN	//GND
#define SOM_PIN_41  	UNASSIGNED_PIN	//GND
#define SOM_PIN_83  	UNASSIGNED_PIN	//GND
#define SOM_PIN_109 	UNASSIGNED_PIN	//GND
#define SOM_PIN_147 	UNASSIGNED_PIN	//GND
#define SOM_PIN_181 	UNASSIGNED_PIN	//GND
#define SOM_PIN_197 	UNASSIGNED_PIN	//GND
#define SOM_PIN_199 	UNASSIGNED_PIN	//GND

/*Special Purpose Pins*/
#define SOM_PIN_26  //EN //This pin is used for reset the ESP module.
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Unused Pins*/
#define SOM_PIN_3			UNASSIGNED_PIN
#define SOM_PIN_4			UNASSIGNED_PIN
#define SOM_PIN_5			UNASSIGNED_PIN
#define SOM_PIN_6			UNASSIGNED_PIN
#define SOM_PIN_1			UNASSIGNED_PIN
#define SOM_PIN_7			UNASSIGNED_PIN
#define SOM_PIN_8			UNASSIGNED_PIN
#define SOM_PIN_9			UNASSIGNED_PIN
#define SOM_PIN_10			UNASSIGNED_PIN
#define SOM_PIN_11			UNASSIGNED_PIN
#define SOM_PIN_12			UNASSIGNED_PIN
#define SOM_PIN_13			UNASSIGNED_PIN
#define SOM_PIN_14			UNASSIGNED_PIN
#define SOM_PIN_15			UNASSIGNED_PIN
#define SOM_PIN_16			UNASSIGNED_PIN
#define SOM_PIN_17			UNASSIGNED_PIN
#define SOM_PIN_18			UNASSIGNED_PIN
#define SOM_PIN_19			UNASSIGNED_PIN
#define SOM_PIN_20			UNASSIGNED_PIN
#define SOM_PIN_21			UNASSIGNED_PIN
#define SOM_PIN_22			UNASSIGNED_PIN
#define SOM_PIN_23			UNASSIGNED_PIN
#define SOM_PIN_24			UNASSIGNED_PIN
#define SOM_PIN_25			UNASSIGNED_PIN
#define SOM_PIN_28			UNASSIGNED_PIN
#define SOM_PIN_30			UNASSIGNED_PIN
#define SOM_PIN_31			UNASSIGNED_PIN
#define SOM_PIN_32			UNASSIGNED_PIN
#define SOM_PIN_34			UNASSIGNED_PIN
#define SOM_PIN_37			UNASSIGNED_PIN
#define SOM_PIN_40			UNASSIGNED_PIN
#define SOM_PIN_43			UNASSIGNED_PIN
#define SOM_PIN_44			UNASSIGNED_PIN
#define SOM_PIN_45			UNASSIGNED_PIN
#define SOM_PIN_46			UNASSIGNED_PIN
#define SOM_PIN_47			UNASSIGNED_PIN
#define SOM_PIN_48			UNASSIGNED_PIN
#define SOM_PIN_50			UNASSIGNED_PIN
#define SOM_PIN_51			UNASSIGNED_PIN
#define SOM_PIN_52			UNASSIGNED_PIN
#define SOM_PIN_54			UNASSIGNED_PIN
#define SOM_PIN_55			UNASSIGNED_PIN
#define SOM_PIN_56			UNASSIGNED_PIN
#define SOM_PIN_57			UNASSIGNED_PIN
#define SOM_PIN_58			UNASSIGNED_PIN
#define SOM_PIN_59			UNASSIGNED_PIN
#define SOM_PIN_60			UNASSIGNED_PIN
#define SOM_PIN_61			UNASSIGNED_PIN
#define SOM_PIN_62			UNASSIGNED_PIN
#define SOM_PIN_63			UNASSIGNED_PIN
#define SOM_PIN_64			UNASSIGNED_PIN
#define SOM_PIN_65			UNASSIGNED_PIN
#define SOM_PIN_66			UNASSIGNED_PIN
#define SOM_PIN_67			UNASSIGNED_PIN
#define SOM_PIN_68			UNASSIGNED_PIN
#define SOM_PIN_69			UNASSIGNED_PIN
#define SOM_PIN_70			UNASSIGNED_PIN
#define SOM_PIN_71			UNASSIGNED_PIN
#define SOM_PIN_72			UNASSIGNED_PIN
#define SOM_PIN_73			UNASSIGNED_PIN
#define SOM_PIN_74			UNASSIGNED_PIN
#define SOM_PIN_75			UNASSIGNED_PIN
#define SOM_PIN_76			UNASSIGNED_PIN
#define SOM_PIN_77			UNASSIGNED_PIN
#define SOM_PIN_78			UNASSIGNED_PIN
#define SOM_PIN_80			UNASSIGNED_PIN
#define SOM_PIN_81			UNASSIGNED_PIN
#define SOM_PIN_82			UNASSIGNED_PIN
#define SOM_PIN_87			UNASSIGNED_PIN
#define SOM_PIN_89			UNASSIGNED_PIN
#define SOM_PIN_91			UNASSIGNED_PIN
#define SOM_PIN_93			UNASSIGNED_PIN
#define SOM_PIN_94			UNASSIGNED_PIN
#define SOM_PIN_95			UNASSIGNED_PIN
#define SOM_PIN_96			UNASSIGNED_PIN
#define SOM_PIN_99			UNASSIGNED_PIN
#define SOM_PIN_100			UNASSIGNED_PIN
#define SOM_PIN_102			UNASSIGNED_PIN
#define SOM_PIN_103			UNASSIGNED_PIN
#define SOM_PIN_104			UNASSIGNED_PIN
#define SOM_PIN_105			UNASSIGNED_PIN
#define SOM_PIN_106			UNASSIGNED_PIN
#define SOM_PIN_107			UNASSIGNED_PIN
#define SOM_PIN_110			UNASSIGNED_PIN
#define SOM_PIN_112			UNASSIGNED_PIN
#define SOM_PIN_111			UNASSIGNED_PIN
#define SOM_PIN_113			UNASSIGNED_PIN
#define SOM_PIN_114			UNASSIGNED_PIN
#define SOM_PIN_115			UNASSIGNED_PIN
#define SOM_PIN_116			UNASSIGNED_PIN
#define SOM_PIN_117			UNASSIGNED_PIN
#define SOM_PIN_118			UNASSIGNED_PIN
#define SOM_PIN_119			UNASSIGNED_PIN
#define SOM_PIN_120			UNASSIGNED_PIN
#define SOM_PIN_121			UNASSIGNED_PIN
#define SOM_PIN_122			UNASSIGNED_PIN
#define SOM_PIN_123			UNASSIGNED_PIN
#define SOM_PIN_124			UNASSIGNED_PIN
#define SOM_PIN_125			UNASSIGNED_PIN
#define SOM_PIN_126			UNASSIGNED_PIN
#define SOM_PIN_127			UNASSIGNED_PIN
#define SOM_PIN_128			UNASSIGNED_PIN
#define SOM_PIN_129			UNASSIGNED_PIN
#define SOM_PIN_130			UNASSIGNED_PIN
#define SOM_PIN_131			UNASSIGNED_PIN
#define SOM_PIN_132			UNASSIGNED_PIN
#define SOM_PIN_133			UNASSIGNED_PIN
#define SOM_PIN_134			UNASSIGNED_PIN
#define SOM_PIN_135			UNASSIGNED_PIN
#define SOM_PIN_136			UNASSIGNED_PIN
#define SOM_PIN_137			UNASSIGNED_PIN
#define SOM_PIN_138			UNASSIGNED_PIN
#define SOM_PIN_139			UNASSIGNED_PIN
#define SOM_PIN_140			UNASSIGNED_PIN
#define SOM_PIN_141			UNASSIGNED_PIN
#define SOM_PIN_142			UNASSIGNED_PIN
#define SOM_PIN_143			UNASSIGNED_PIN
#define SOM_PIN_144			UNASSIGNED_PIN
#define SOM_PIN_145			UNASSIGNED_PIN
#define SOM_PIN_146			UNASSIGNED_PIN
#define SOM_PIN_149			UNASSIGNED_PIN
#define SOM_PIN_150			UNASSIGNED_PIN
#define SOM_PIN_151			UNASSIGNED_PIN
#define SOM_PIN_152			UNASSIGNED_PIN
#define SOM_PIN_153			UNASSIGNED_PIN
#define SOM_PIN_154			UNASSIGNED_PIN
#define SOM_PIN_155			UNASSIGNED_PIN
#define SOM_PIN_156			UNASSIGNED_PIN
#define SOM_PIN_157			UNASSIGNED_PIN
#define SOM_PIN_158			UNASSIGNED_PIN
#define SOM_PIN_159			UNASSIGNED_PIN
#define SOM_PIN_160			UNASSIGNED_PIN
#define SOM_PIN_161			UNASSIGNED_PIN
#define SOM_PIN_162			UNASSIGNED_PIN
#define SOM_PIN_163			UNASSIGNED_PIN
#define SOM_PIN_164			UNASSIGNED_PIN
#define SOM_PIN_165			UNASSIGNED_PIN
#define SOM_PIN_166			UNASSIGNED_PIN
#define SOM_PIN_167			UNASSIGNED_PIN
#define SOM_PIN_168			UNASSIGNED_PIN
#define SOM_PIN_169			UNASSIGNED_PIN
#define SOM_PIN_170			UNASSIGNED_PIN
#define SOM_PIN_171			UNASSIGNED_PIN
#define SOM_PIN_172			UNASSIGNED_PIN
#define SOM_PIN_173			UNASSIGNED_PIN
#define SOM_PIN_175			UNASSIGNED_PIN
#define SOM_PIN_177			UNASSIGNED_PIN
#define SOM_PIN_179			UNASSIGNED_PIN
#define SOM_PIN_182			UNASSIGNED_PIN
#define SOM_PIN_184			UNASSIGNED_PIN
#define SOM_PIN_186			UNASSIGNED_PIN
#define SOM_PIN_188			UNASSIGNED_PIN
#define SOM_PIN_192			UNASSIGNED_PIN
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//The END.
#endif
