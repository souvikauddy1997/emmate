/*
 * File Name: tcpip_platform_errors.h
 * File Path: /emmate/src/platform/esp/tcpip/tcpip_platform_errors.h
 * Description:
 *
 *  Created on: 06-May-2019
 *      Author: Rohan Dey
 */

#ifndef TCPIP_PLATFORM_ERRORS_H_
#define TCPIP_PLATFORM_ERRORS_H_

#include "tcpip_adapter.h"

#define PF_TCPIP_ERR_INVALID_PARAMS			ESP_ERR_TCPIP_ADAPTER_INVALID_PARAMS
#define PF_TCPIP_ERR_IF_NOT_READY			ESP_ERR_TCPIP_ADAPTER_IF_NOT_READY
#define PF_TCPIP_ERR_DHCPC_START_FAILED		ESP_ERR_TCPIP_ADAPTER_DHCPC_START_FAILED
#define PF_TCPIP_ERR_DHCP_ALREADY_STARTED	ESP_ERR_TCPIP_ADAPTER_DHCP_ALREADY_STARTED
#define PF_TCPIP_ERR_DHCP_ALREADY_STOPPED	ESP_ERR_TCPIP_ADAPTER_DHCP_ALREADY_STOPPED
#define PF_TCPIP_ERR_NO_MEM					ESP_ERR_TCPIP_ADAPTER_NO_MEM
#define PF_TCPIP_ERR_DHCP_NOT_STOPPED		ESP_ERR_TCPIP_ADAPTER_DHCP_NOT_STOPPED

#endif /* TCPIP_PLATFORM_ERRORS_H_ */
