/*
 * logging.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_LOG_H_
#define PLATFORM_LOG_H_

#include <stdint.h>
#include <stdarg.h>
#include "esp_log.h"
//#include "core_config.h"

/* LOG LEVELS */
/*--------------------------------------------------------------------------------*/
/** To override default verbosity level at file or component scope, define the LOG_LOCAL_LEVEL macro. */
#define PF_LOG_LOCAL_LEVEL		LOG_LOCAL_LEVEL

#define PF_LOG_NONE		ESP_LOG_NONE
#define PF_LOG_ERROR	ESP_LOG_ERROR
#define PF_LOG_WARN		ESP_LOG_WARN
#define PF_LOG_INFO		ESP_LOG_INFO
#define PF_LOG_DEBUG	ESP_LOG_DEBUG
#define PF_LOG_VERBOSE	ESP_LOG_VERBOSE


/* ESP logging function wrappers */
/*--------------------------------------------------------------------------------*/
#define pf_log_level_set(tag, level)	/*void*/ esp_log_level_set(/*const char * */tag, /*esp_log_level_t*/ level)
#define pf_log_redirect(func)			/*vprintf_like_t*/ esp_log_set_vprintf(/*vprintf_like_t*/ func)
#define pf_log_timestamp				/*uint32_t*/ esp_log_timestamp(/*void*/)
#define pf_log_early_timestamp			/*uint32_t*/ esp_log_early_timestamp(/*void*/)



/* ESP logging MACRO wrappers */
/*--------------------------------------------------------------------------------*/

/**
 * @brief	Log a buffer of hex bytes at specified level, separated into 16 bytes each line.
 *
 * @param[in] tag 		Description tag
 * @param[in] buffer 	Pointer to the buffer array
 * @param[in] buff_len 	Length of buffer in bytez
 * @param[in] level 	Level of the log
 */
#define PF_LOG_BUFFER_HEX_LEVEL(tag, buffer, buff_len, level)	ESP_LOG_BUFFER_HEX_LEVEL(tag, buffer, buff_len, level)

/**
 * @brief	Log a buffer of characters at specified level, separated into 16 bytes each line. Buffer should contain
 * 			only printable characters.
 *
 * @param[in]	tag			Description tag
 * @param[in]	buffer		Pointer to the buffer array
 * @param[in]	buff_len	Length of buffer in bytes
 * @param[in]	level		Level of the log
 * */
#define PF_LOG_BUFFER_CHAR_LEVEL(tag, buffer, buff_len, level)	ESP_LOG_BUFFER_CHAR_LEVEL(tag, buffer, buff_len, level)

/**
 * @brief	Dump a buffer to the log at specified level.
 * 			The dump log shows just like the one below:
 * 			W (195) log_example: 0x3ffb4280   45 53 50 33 32 20 69 73  20 67 72 65 61 74 2c 20  |ESP32 is great, |
 *
 * 			W (195) log_example: 0x3ffb4290   77 6f 72 6b 69 6e 67 20  61 6c 6f 6e 67 20 77 69  |working along wi|
 *
 * 			W (205) log_example: 0x3ffb42a0   74 68 20 74 68 65 20 49  44 46 2e 00              |th the IDF..|
 *
 * @param[in]	tag			Description tag
 * @param[in]	buffer		Pointer to the buffer array
 * @param[in]	buff_len	Length of buffer in bytes
 * @param[in]	level		Level of the log
 * */
#define PF_LOG_BUFFER_HEXDUMP(tag, buffer, buff_len, level)	ESP_LOG_BUFFER_HEXDUMP(tag, buffer, buff_len, level)

/**
 * @brief	Log a buffer of hex bytes at Info level.
 * 			See		esp_log_buffer_hex_level
 *
 * @param[in]	tag			Description tag
 * @param[in]	buffer		Pointer to the buffer array
 * @param[in]	buff_len	Length of buffer in bytes
 */
#define PF_LOG_BUFFER_HEX(tag, buffer, buff_len)	ESP_LOG_BUFFER_HEX(tag, buffer, buff_len)

/**
 * @brief	Log a buffer of characters at Info level. Buffer should contain only printable characters.
 * 			See	esp_log_buffer_char_level
 *
 * @param[in]	tag			Description tag
 * @param[in]	buffer		Pointer to the buffer array
 * @param[in]	buff_len	Length of buffer in bytes
 */
#define PF_LOG_BUFFER_CHAR(tag, buffer, buff_len)	ESP_LOG_BUFFER_CHAR(tag, buffer, buff_len)

/* macro to output logs in startup code, before heap allocator and syscalls have been initialized */
#define PF_EARLY_LOGE(tag, format, ...)			ESP_EARLY_LOGE( tag, format, ##__VA_ARGS__ )
#define PF_EARLY_LOGW(tag, format, ...)			ESP_EARLY_LOGW( tag, format, ##__VA_ARGS__ )
#define PF_EARLY_LOGI(tag, format, ...)			ESP_EARLY_LOGI( tag, format, ##__VA_ARGS__ )
#define PF_EARLY_LOGD(tag, format, ...)			ESP_EARLY_LOGD( tag, format, ##__VA_ARGS__ )
#define PF_EARLY_LOGV(tag, format, ...)			ESP_EARLY_LOGV( tag, format, ##__VA_ARGS__ )

/**
 * @brief	Runtime macro to output logs at a specified level.
 *
 * @param[in]	tag		tag of the log, which can be used to change the log level by esp_log_level_set at runtime.
 * @param[in]	level	level of the output log.
 * @param[in]	format	format of the output log. see printf
 * @param[in]	...		variables to be replaced into the log. see printf
 * */
#define PF_LOG_LEVEL(level, tag, format, ...)	ESP_LOG_LEVEL( level, tag, format, ##__VA_ARGS__ )

/* runtime macro to output logs at a specified level. */
#define PF_LOG_LEVEL_LOCAL(level, tag, format, ...)	ESP_LOG_LEVEL_LOCAL( level, tag, format, ##__VA_ARGS__ )

#define PF_LOGE( tag, format, ... )			ESP_LOGE( tag, format, ##__VA_ARGS__ )
#define PF_LOGW( tag, format, ... )			ESP_LOGW( tag, format, ##__VA_ARGS__ )
#define PF_LOGI( tag, format, ... )			ESP_LOGI( tag, format, ##__VA_ARGS__ )
#define PF_LOGD( tag, format, ... )			ESP_LOGD( tag, format, ##__VA_ARGS__ )
#define PF_LOGV( tag, format, ... )			ESP_LOGV( tag, format, ##__VA_ARGS__ )

#endif /* PLATFORM_LOG_H_ */
