/*
 * File Name: nvs_helper_errors.h
 * File Path: /emmate/src/persistent/nvs/esp-idf/nvs_helper_errors.h
 * Description:
 *
 *  Created on: 15-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef NVS_HELPER_ERRORS_H_
#define NVS_HELPER_ERRORS_H_

#include "platform_error_base.h"
#include "nvs.h"

#define PLAT_NVS_ERR_NOT_INITIALIZED		ESP_ERR_NVS_NOT_INITIALIZED		/*!< The storage driver is not initialized */
#define PLAT_NVS_ERR_NOT_FOUND				ESP_ERR_NVS_NOT_FOUND			/*!< Id namespace doesn’t exist yet and mode is NVS_READONLY */
#define PLAT_NVS_ERR_TYPE_MISMATCH			ESP_ERR_NVS_TYPE_MISMATCH		/*!< The type of set or get operation doesn't match the type of value stored in NVS */
#define PLAT_NVS_ERR_READ_ONLY				ESP_ERR_NVS_READ_ONLY			/*!< Storage handle was opened as read only */
#define PLAT_NVS_ERR_NOT_ENOUGH_SPACE		ESP_ERR_NVS_NOT_ENOUGH_SPACE	/*!< There is not enough space in the underlying storage to save the value */
#define PLAT_NVS_ERR_INVALID_NAME			ESP_ERR_NVS_INVALID_NAME		/*!< Namespace name doesn’t satisfy constraints */
#define PLAT_NVS_ERR_INVALID_HANDLE			ESP_ERR_NVS_INVALID_HANDLE		/*!< Handle has been closed or is NULL */
#define PLAT_NVS_ERR_REMOVE_FAILED			ESP_ERR_NVS_REMOVE_FAILED		/*!< The value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again. */
#define PLAT_NVS_ERR_KEY_TOO_LONG			ESP_ERR_NVS_KEY_TOO_LONG		/*!< Key name is too long */
#define PLAT_NVS_ERR_PAGE_FULL				ESP_ERR_NVS_PAGE_FULL			/*!< Internal error; never returned by nvs API functions */
#define PLAT_NVS_ERR_INVALID_STATE			ESP_ERR_NVS_INVALID_STATE		/*!< NVS is in an inconsistent state due to a previous error. Call nvs_flash_init and nvs_open again, then retry. */
#define PLAT_NVS_ERR_INVALID_LENGTH			ESP_ERR_NVS_INVALID_LENGTH		/*!< String or blob length is not sufficient to store data */
#define PLAT_NVS_ERR_NO_FREE_PAGES			ESP_ERR_NVS_NO_FREE_PAGES		/*!< NVS partition doesn't contain any empty pages. This may happen if NVS partition was truncated. Erase the whole partition and call nvs_flash_init again. */
#define PLAT_NVS_ERR_VALUE_TOO_LONG			ESP_ERR_NVS_VALUE_TOO_LONG		/*!< String or blob length is longer than supported by the implementation */
#define PLAT_NVS_ERR_PART_NOT_FOUND			ESP_ERR_NVS_PART_NOT_FOUND		/*!< Partition with specified name is not found in the partition table */

#define PLAT_NVS_ERR_NEW_VERSION_FOUND		ESP_ERR_NVS_NEW_VERSION_FOUND	/*!< NVS partition contains data in new format and cannot be recognized by this version of code */
#define PLAT_NVS_ERR_XTS_ENCR_FAILED		ESP_ERR_NVS_XTS_ENCR_FAILED		/*!< XTS encryption failed while writing NVS entry */
#define PLAT_NVS_ERR_XTS_DECR_FAILED		ESP_ERR_NVS_XTS_DECR_FAILED		/*!< XTS decryption failed while reading NVS entry */
#define PLAT_NVS_ERR_XTS_CFG_FAILED			ESP_ERR_NVS_XTS_CFG_FAILED		/*!< XTS configuration setting failed */
#define PLAT_NVS_ERR_XTS_CFG_NOT_FOUND		ESP_ERR_NVS_XTS_CFG_NOT_FOUND	/*!< XTS configuration not found */
#define PLAT_NVS_ERR_ENCR_NOT_SUPPORTED		ESP_ERR_NVS_ENCR_NOT_SUPPORTED	/*!< NVS encryption is not supported in this version */
#define PLAT_NVS_ERR_KEYS_NOT_INITIALIZED	ESP_ERR_NVS_KEYS_NOT_INITIALIZED	/*!< NVS key partition is uninitialized */
#define PLAT_NVS_ERR_CORRUPT_KEY_PART		ESP_ERR_NVS_CORRUPT_KEY_PART	/*!< NVS key partition is corrupt */

#define PLAT_NVS_ERR_CONTENT_DIFFERS		ESP_ERR_NVS_CONTENT_DIFFERS		/*!< Internal error; never returned by nvs API functions.  NVS key is different in comparison */


#endif /* NVS_HELPER_ERRORS_H_ */
