/*
 * system_info.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_INFO_H_
#define SYSTEM_INFO_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "esp_system.h"

//typedef struct {
//	char chip_name[10];		// Name of the MCU/MPU
//	char chip_mfgr[10];		// Name of chip manufacturer
//	esp_chip_info_t chip_info;
//} HardwareInfo;

typedef struct {
	char chip_name[10];		// Name of the MCU/MPU
	char chip_mfgr[10];		// Name of chip manufacturer
	bool has_wifi;
	bool has_bt;
	bool has_ble;
	bool has_gsm_2g;
	bool has_gsm_3g;
	bool has_gsm_4g;
	bool has_lora;
	bool has_nbiot;
	int flash_size;
	int ram_size;
} HardwareInfo;

typedef struct {
	char sdk_name[20];
	char sdk_version[35];
} SDKInfo;

typedef struct {
	HardwareInfo hw_info;
	SDKInfo sdk_info;
} PlatformInfo;

/**
 * @brief  	This function reads the hardware info by internally calling esp_chip_info and
 * 			populates the PlatformInfo structure
 * @param[out]	pl_info Pointer to PlatformInfo
 */
void read_save_platform_info(PlatformInfo *pl_info);
/**
 * @brief  	This function reads the framework info
 */
//void read_framework_info();
/**
 * @brief  	Prints the hardware info
 * @param[in]	pl_info Pointer to PlatformInfo
 */
void print_system_specific_info(PlatformInfo *pl_info);

#endif /* SYSTEM_INFO_H_ */
