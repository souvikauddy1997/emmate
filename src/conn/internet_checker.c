/*
 * File Name: ping.c
 * File Path: /emmate/src/conn/internet_checker.c
 * Description:
 *
 *  Created on: 03-Apr-2020
 *      Author: Noyel Seth
 */

#include "internet_checker.h"

#if CONFIG_USE_INTERNET_CHECKER

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
#include <string.h>
#include "system_utils.h"
#include "event_group_core.h"

#if CONFIG_USE_WIFI
#include "wifi.h"
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif

#include "http_client_api.h"


#if CONFIG_USE_IOT_CLOUD

#if CONFIG_HAVE_IOT_CLOUD_MIGCLOUD
#include "migcloud_urls.h"
#endif

#endif

#define TAG LTAG_PING

static INTERNET_STATUS internet_stat = INTERNET_NOT_AVAILABLE;
static ThreadHandle internet_checker_task_hdl = NULL;
static InternetStatRecvCallBackHandler ping_stat_cb_hld=NULL;

/**
 *
 */
static void internet_checker_task(void *param) {

	CORE_LOGD(TAG, "%s started", __func__);

	bool is_netIf_present = false;
	INTERNET_STATUS curr_stat = INTERNET_NOT_AVAILABLE;

	CORE_LOGD(TAG, "Waiting for network IP ...");
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

	TaskDelay(DELAY_5_SEC/TICK_RATE_TO_MS);

	while (1) {
		CORE_LOGD(TAG, "%s running", __func__);

#if CONFIG_USE_WIFI
		if (get_wifi_connection_status() == WIFI_CONNECTED) {
			is_netIf_present = true;
		} else {
			is_netIf_present = false;
			curr_stat = INTERNET_NOT_AVAILABLE;
		}
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif

		if(is_netIf_present == true){

			uint16_t http_stat = 0;
			core_err ret = CORE_FAIL;

#if CONFIG_USE_IOT_CLOUD

#if CONFIG_HAVE_IOT_CLOUD_MIGCLOUD
			// checking Cloud Server
			/* Do http operation */
			ret = do_http_operation(MIGCLOUD_INTERNET_CHECKER_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
					IQ_HOST, HTTP_USER_AGENT, CONTENT_TYPE_APPLICATION_JSON, NULL, 0, NULL, NULL, 0, &http_stat);
#endif

			if (ret != CORE_OK || http_stat != HTTP_OK_RES_CODE) {

				CORE_LOGE(TAG, "IoT Cloud (%s) Failed HTTP status %d", MIGCLOUD_INTERNET_CHECKER_URL, http_stat);
				curr_stat = INTERNET_NOT_AVAILABLE;

				http_stat = 0;
				ret = do_http_operation(GOOGLE_PING_URL, GOOGLE_PING_PORT, NULL, HTTP_CLIENT_METHOD_GET,
									GOOGLE_PING_HOST, HTTP_USER_AGENT, NULL, NULL, 0, NULL, NULL, 0, &http_stat);


				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "%s Failed HTTP status %d", GOOGLE_PING_URL, http_stat);
					curr_stat = INTERNET_NOT_AVAILABLE;
					event_group_set_bits(get_system_evtgrp_hdl(), INTERNET_NOT_AVAILABLE_BIT);
				}else{
					CORE_LOGI(TAG, "%s Success HTTP status %d", GOOGLE_PING_URL, http_stat);
					curr_stat = INTERNET_AVAILABLE;
					event_group_set_bits(get_system_evtgrp_hdl(), INTERNET_AVAILABLE_BIT);
//					TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
				}

			} else {
				CORE_LOGI(TAG, "IoT Cloud (%s) Success HTTP status %d", MIGCLOUD_INTERNET_CHECKER_URL, http_stat);
				curr_stat = INTERNET_CLOUD_AVAILABLE;
				event_group_set_bits(get_system_evtgrp_hdl(), INTERNET_AVAILABLE_BIT);
//				TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
			}


#else
			ret = do_http_operation(GOOGLE_PING_URL, GOOGLE_PING_PORT, NULL, HTTP_CLIENT_METHOD_GET,
												GOOGLE_PING_HOST, HTTP_USER_AGENT, NULL, NULL, 0, NULL, NULL, 0, &http_stat);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "%s Failed HTTP status %d", GOOGLE_PING_URL, http_stat);
				curr_stat = INTERNET_NOT_AVAILABLE;
			} else {
				CORE_LOGI(TAG, "%s Success HTTP status %d", GOOGLE_PING_URL, http_stat);
				curr_stat = INTERNET_AVAILABLE;
			}

#endif
	}

		if (curr_stat != internet_stat) {
			//TODO: call ping call-back func
			internet_stat = curr_stat;
			if (ping_stat_cb_hld != NULL) {
				ping_stat_cb_hld(internet_stat);
			}

		}


//		printf("Stack remaining for task '%s' is %d bytes\r\n", TaskGetTaskName(NULL),
//								TaskGetStackHighWaterMark(NULL));



		TaskDelay(INTERNET_CHECK_INTERVAL/TICK_RATE_TO_MS);
	}

	internet_checker_task_hdl = NULL;
	TaskDelete(NULL);
}

/**
 *
 */
core_err init_internet_checker() {

	bool status = TaskCreate(internet_checker_task, "inter_chk_task", TASK_STACK_SIZE_4K, NULL,
			THREAD_PRIORITY_20, &internet_checker_task_hdl);
	if (status != true) {
		CORE_LOGE(TAG, "Failed to Start Ping task");
		return CORE_FAIL;
	}

	return CORE_OK;

}

/**
 *
 */
void deinit_internet_checker() {

	if(internet_checker_task_hdl!=NULL){
		TaskDelete(internet_checker_task_hdl);
	}
}


/**
 *
 */
INTERNET_STATUS get_internet_stat() {
	return internet_stat;
}

/**
 *
 */
core_err register_internet_stat_recv_cb_handler(InternetStatRecvCallBackHandler hdl) {
	if (hdl != NULL) {
		ping_stat_cb_hld = hdl;
		return CORE_OK;
	}
	return CORE_FAIL;
}

#endif /* CONFIG_USE_INTERNET_CHECKER */
