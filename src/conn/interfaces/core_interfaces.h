/*
 * File Name: core_interfaces.h
 * File Path: /emmate/src/conn/interfaces/core_interfaces.h
 * Description:
 *
 *  Created on: 21-May-2019
 *      Author: Rohan Dey
 */

#ifndef CORE_INTERFACES_H_
#define CORE_INTERFACES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "nw_interface_platform.h"

typedef struct {
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif
} NetworkInterface;

typedef enum {
	NW_IF_WIFI_STA	= PLAT_NW_IF_WIFI_STA, 		/**< Wifi station interface */
	NW_IF_WIFI_AP 	= PLAT_NW_IF_WIFI_AP,		/**< Wifi soft-AP interface */
	NW_IF_ETH 		= PLAT_NW_IF_ETH,			/**< Ethernet interface */
	NW_IF_MAX 		= PLAT_NW_IF_MAX
} nw_interface;

#ifdef __cplusplus
}
#endif

#endif /* CORE_INTERFACES_H_ */
