/*
 * File Name: wifi_constants.h
 * File Path: /emmate/src/conn/wifi/wifi_constants.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef WIFI_CONSTANTS_H_
#define WIFI_CONSTANTS_H_

/***************** Macros of WiFi Credential **************************/
#define WIFI_SSID_LEN			32		/**< WiFi SSID's max length */
#define WIFI_PWD_LEN			64		/**< WiFi Password's max length */
#define WIFI_MAC_LEN			6		/**< Length in bytes of Wi-Fi MAC address */
#define WIFI_STA_IP_LEN			16		/**< Length in bytes of Connected Wi-Fi station's IP (eg. 255.255.255.255) */


#endif /* WIFI_CONSTANTS_H_ */
