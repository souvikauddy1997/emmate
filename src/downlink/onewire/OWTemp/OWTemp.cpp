/*
 * OWTemp.cpp
 *
 *  Created on: 23-Jun-2019
 *      Author: Rohan Dey
 */

#include "OWTemp.h"

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#if ENABLE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG OWTEMP

OWTemp::OWTemp(RandomAccessRomIterator & selector) :
		m_switch(selector), m_temp_sensor(selector) {

}

OWTemp::~OWTemp() {
	// TODO Auto-generated destructor stub
}

//*********************************************************************
OWTemp::CmdResult OWTemp::disconnectTempSensor(void)
{
    OWTemp::CmdResult result = OWTemp::OpFailure;
    uint8_t pio_state;

    DS2413::CmdResult sw_result = m_switch.pioAccessReadChB(pio_state);
    if(sw_result == DS2413::Success)
    {
        //if high take low, falling edge puts bridge and sensor to sleep
        if(pio_state)
        {
            sw_result = m_switch.pioAccessWriteChB(0);
            if(sw_result == DS2413::Success)
            {
                result = OWTemp::Success;
            }
        }
        else
        {
            //if low, take high, then low to get falling edge
            sw_result = m_switch.pioAccessWriteChB(1);
            if(sw_result == DS2413::Success)
            {
                sw_result = m_switch.pioAccessWriteChB(0);
                if(sw_result == DS2413::Success)
                {
                    result = OWTemp::Success;
                }
            }
        }
    }

    return result;
}

//*********************************************************************
OWTemp::CmdResult OWTemp::connectTempSensor(void)
{
    OWTemp::CmdResult result = OWTemp::OpFailure;

    DS2413::CmdResult sw_result = m_switch.pioAccessWriteChB(1);
    if(sw_result == DS2413::Success)
    {
        result = OWTemp::Success;
    }

    return result;
}

//*********************************************************************
OWTemp::CmdResult OWTemp::connectOWbus(void)
{
    OWTemp::CmdResult result = OWTemp::OpFailure;

    DS2413::CmdResult sw_result = m_switch.pioAccessWriteChA(0);
    if(sw_result == DS2413::Success)
    {
        result = OWTemp::Success;
    }

    return result;
}


//*********************************************************************
OWTemp::CmdResult OWTemp::disconnectOWbus(void)
{
    OWTemp::CmdResult result = OWTemp::OpFailure;

    DS2413::CmdResult sw_result = m_switch.pioAccessWriteChA(1);
    if(sw_result == DS2413::Success)
    {
        result = OWTemp::Success;
    }

    return result;
}

//*********************************************************************
OWTemp::CmdResult OWTemp::getTemperature(float & temp)
{
	OWTemp::CmdResult result = OWTemp::OpFailure;

	OneWireSlave::CmdResult sen_result = m_temp_sensor.convertTemperature(temp);
    if(sen_result == OneWireSlave::Success)
    {
        result = OWTemp::Success;
    }

	return result;
}
