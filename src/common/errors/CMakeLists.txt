add_library(errors INTERFACE)

if(CONFIG_PLATFORM_ESP_IDF)
	list(APPEND IDF_LIBS
				idf::esp_common
			)
	target_link_libraries(errors INTERFACE ${IDF_LIBS})
endif()

target_include_directories(errors 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)