/*
 * core_error.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef CORE_ERROR_H_
#define CORE_ERROR_H_

#include <stdint.h>
#include "platform_error.h"

typedef int32_t core_err;

/* Definitions for error constants. */
#define CORE_OK          				PLATFORM_OK       /*!<  value indicating success (no error) */
#define CORE_FAIL        				PLATFORM_FAIL      /*!< Generic code indicating failure */
#define CORE_ON_PROCESS					1100
#define CORE_PERIPHERAL_INITIALIZE		1101
#define CORE_PERIPHERAL_NOT_INITIALIZE	1102

#define CORE_ERR_NO_MEM              PLATFORM_ERR_NO_MEM   			/*!< Out of memory */
#define CORE_ERR_INVALID_ARG         PLATFORM_ERR_INVALID_ARG   	/*!< Invalid argument */
#define CORE_ERR_INVALID_STATE       PLATFORM_ERR_INVALID_STATE   	/*!< Invalid state */
#define CORE_ERR_INVALID_SIZE        PLATFORM_ERR_INVALID_SIZE   	/*!< Invalid size */
#define CORE_ERR_NOT_FOUND           PLATFORM_ERR_NOT_FOUND   		/*!< Requested resource not found */
#define CORE_ERR_NOT_SUPPORTED       PLATFORM_ERR_NOT_SUPPORTED   	/*!< Operation or feature not supported */
#define CORE_ERR_TIMEOUT             PLATFORM_ERR_TIMEOUT   		/*!< Operation timed out */
#define CORE_ERR_INVALID_RESPONSE    PLATFORM_ERR_INVALID_RESPONSE	/*!< Received response was invalid */
#define CORE_ERR_INVALID_CRC         PLATFORM_ERR_INVALID_CRC   	/*!< CRC or checksum was invalid */
#define CORE_ERR_INVALID_VERSION     PLATFORM_ERR_INVALID_VERSION 	/*!< Version was invalid */
#define CORE_ERR_INVALID_MAC         PLATFORM_ERR_INVALID_MAC   	/*!< MAC address was invalid */


#define CORE_ERROR_MSG_MAX_LEN		100 		/**< Maximum length of an error message that can be stored */
#define CORE_ERROR_MSG_TERMINATOR	"..."
/*
 * @brief 	This data structure holds an error code and its corresponding error message.
 * 			The maximum length of an error message that can be stored is defined by the macro CORE_ERROR_MSG_MAX_LEN
 *
 * */
typedef struct {
	int err_code;								/**< Variable to store an error code */
	char err_msg[CORE_ERROR_MSG_MAX_LEN+4];		/**< Variable to store an error message. Max length of an error message
	 	 	 	 	 	 	 	 	 	 	 	 	 can be CORE_ERROR_MSG_MAX_LEN. If the error message length
	 	 	 	 	 	 	 	 	 	 	 	 	 exceeds then the extra bytes are truncated and 3 bytes are replaced
	 	 	 	 	 	 	 	 	 	 	 	 	 by '...' */
} CoreError;

#endif /* CORE_ERROR_H_ */
