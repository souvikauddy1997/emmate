Generic Doubly Linked List
=================
A doubly linked list implementation in C. The list stores (void) pointers to the data. The actual data is never copied, modified or deleted in this implementation. This allows for the same data to be stored in multiple different lists.

### Features
* Fast Stack operations (push/pop)
* Fast Queue operations (pushBack/pop)
* Fast Deque operations (push/pushBack and pop/popBack)
* Forward/Backward iteration with function parameter

### Library Details
* URL: https://github.com/philbot9/generic-linked-list
