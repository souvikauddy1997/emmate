# Common Module

## Overview

The common module provides APIs and MACRO definitions of all common features used in the system. All functionality present here are used by the EmMate Framework itself and can also be used by the application.

## Features

Features of the common module includes:
1. Commonly used includes
2. High level error codes
3. All constants used by the EmMate Framework
4. APIs for creating and using standard data structures like linked lists.


#### Submodules

The common module has the following submodules
1. Data Structures
2. Errors
3. Common
4. Constant
5. Bulk Data Transactor

## How to use the Common Module from an Application

### 1. Data Structures

This submodule contains the following standard data structures:
1. Linked List - See [Generic Doubly Linked List](https://github.com/philbot9/generic-linked-list) for details

##### Header(s) to include

```
gll.h
```
#### Sample APIs

Please visit [Generic Doubly Linked List](https://github.com/philbot9/generic-linked-list) for examples and sample codes

### 2. Errors
Please refer API documentation for more details
##### Header(s) to include

```
core_error.h
```

### 3. Common
Please refer API documentation for more details
##### Header(s) to include

```
core_common.h
```

### 4. Constant
Please refer API documentation for more details
##### Header(s) to include

```
core_constant.h
```

### 5. Bulk Data Transactor
Please refer the [module's documentation](bulk-data-transactor/README.md) for more information on this.
