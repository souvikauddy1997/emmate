/*
 * File Name: bulk_data_transactor.h
 * File Path: /emmate/src/common/bulk-data-transactor/bulk_data_transactor.h
 * Description:
 *
 *  Created on: 12-Jun-2019
 *      Author: Rohan Dey
 */

#ifndef BULK_DATA_TRANSACTOR_H_
#define BULK_DATA_TRANSACTOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"
#include "threading.h"
#include "bulk_data_peripheral.h"

typedef enum {
	BD_READONLY = 0,
	BD_WRITEONLY,
	BD_READWRITE
} BULKDATA_TRANSACTION_MODE;

typedef enum {
	TRANSACT_NUM_BYTES = 0,
	TRANSACT_CHAR_TERMINATOR,
	TRANSACT_STRING_TERMINATOR
} BULKDATA_TRANSACTION_TYPE;

typedef enum {
	TRANSACTION_NOT_STARTED = 0,
	TRANSACTION_ONGOING,
	TRANSACTION_DONE,
} BULKDATA_TRANSACTION_STATUS;

typedef struct {
	int num_bytes;
	char terminator;
	char *delimiter;
} TransactionEnd;

typedef void (*bulkdata_transactor_cb) (BULKDATA_TRANSACTION_STATUS stat, void* data);

typedef struct {
	BULKDATA_TRANSACTION_MODE mode;
	BULKDATA_TRANSACTION_TYPE type;
	TransactionEnd stream_end;
	BULKDATA_PERIPHERALS peripheral;
	QueueHandle transact_queue;
	ThreadHandle transact_thread;
	bulkdata_transactor_cb transaction_cb;
	BULKDATA_TRANSACTION_STATUS transaction_status;
	void *transaction_data;
} TransactionData;

/**
 * @brief
 * */
core_err init_bulkdata_transactor(BULKDATA_TRANSACTION_MODE mode, BULKDATA_TRANSACTION_TYPE type, TransactionEnd *end,
		BULKDATA_PERIPHERALS peripheral, bulkdata_transactor_cb cb);

/**
 * @brief
 * */
void deinit_bulkdata_transactor();

/**
 * @brief
 * */
BULKDATA_TRANSACTION_STATUS get_transaction_status();

/**
 * @brief
 * */
void* get_transaction_data();

#ifdef __cplusplus
}
#endif

#endif /* BULK_DATA_TRANSACTOR_H_ */
