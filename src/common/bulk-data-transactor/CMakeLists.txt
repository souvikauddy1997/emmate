set(srcs
	bd_transactor_utils.c
	bulk_data_transactor
	)

add_library(bulk-data-transactor STATIC ${srcs})

target_include_directories(bulk-data-transactor 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)