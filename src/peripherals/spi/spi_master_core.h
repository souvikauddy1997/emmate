/*
 * File Name: spi_master_core.h
 *
 * Description:
 *
 *  Created on: 28-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef SRC_PERIPHERALS_SPI_SPI_MASTER_CORE_H_
#define SRC_PERIPHERALS_SPI_SPI_MASTER_CORE_H_

#include "spi_core_common.h"
#include "spi_master_platform.h"

#ifdef __cplusplus
extern "C" {
#endif


#define SPI_MASTER_CORE_FREQ_8M   SPI_MASTER_PF_FREQ_8M
#define SPI_MASTER_CORE_FREQ_9M   SPI_MASTER_PF_FREQ_9M    /** < 8.89MHz */
#define SPI_MASTER_CORE_FREQ_10M  SPI_MASTER_PF_FREQ_10M   /** < 10MHz */
#define SPI_MASTER_CORE_FREQ_11M  SPI_MASTER_PF_FREQ_11M   /** < 11.43MHz */
#define SPI_MASTER_CORE_FREQ_13M  SPI_MASTER_PF_FREQ_13M   /** < 13.33MHz */
#define SPI_MASTER_CORE_FREQ_16M  SPI_MASTER_PF_FREQ_16M   /** < 16MHz */
#define SPI_MASTER_CORE_FREQ_20M  SPI_MASTER_PF_FREQ_20M   /** < 20MHz */
#define SPI_MASTER_CORE_FREQ_26M  SPI_MASTER_PF_FREQ_26M   /** < 26.67MHz */
#define SPI_MASTER_CORE_FREQ_40M  SPI_MASTER_PF_FREQ_40M   /** < 40MHz */
#define SPI_MASTER_CORE_FREQ_80M  SPI_MASTER_PF_FREQ_80M   /** < 80MHz */


#define SPI_CORE_DEVICE_TXBIT_LSBFIRST      SPI_PF_DEVICE_TXBIT_LSBFIRST     /** < Transmit command/address/data LSB first instead of the default MSB first */
#define SPI_CORE_DEVICE_RXBIT_LSBFIRST      SPI_PF_DEVICE_RXBIT_LSBFIRST     /** < Receive data LSB first instead of the default MSB first */
#define SPI_CORE_DEVICE_BIT_LSBFIRST        SPI_PF_DEVICE_BIT_LSBFIRST       /** < Transmit and receive LSB first */
#define SPI_CORE_DEVICE_3WIRE               SPI_PF_DEVICE_3WIRE              /** < Use MOSI (=spid) for both sending and receiving data */
#define SPI_CORE_DEVICE_POSITIVE_CS         SPI_PF_DEVICE_POSITIVE_CS        /** < Make CS positive during a transaction instead of negative */
#define SPI_CORE_DEVICE_HALFDUPLEX          SPI_PF_DEVICE_HALFDUPLEX         /** < Transmit data before receiving it, instead of simultaneously */
#define SPI_CORE_DEVICE_CLK_AS_CS           SPI_PF_DEVICE_CLK_AS_CS          /** < Output clock on CS line if CS is active */
/** There are timing issue when reading at high frequency (the frequency is related to whether iomux pins are used, valid time after slave sees the clock).
  *     - In half-duplex mode, the driver automatically inserts dummy bits before reading phase to fix the timing issue. Set this flag to disable this feature.
  *     - In full-duplex mode, however, the hardware cannot use dummy bits, so there is no way to prevent data being read from getting corrupted.
  *       Set this flag to confirm that you're going to work with output only, or read without dummy bits at your own risk.
  */
#define SPI_CORE_DEVICE_NO_DUMMY            SPI_PF_DEVICE_NO_DUMMY

typedef spi_pf_device_handle_t spi_device_handle;  ///< Handle for a device on a SPI bus

/**
 * @brief Initialize a SPI bus
 *
 * @warning For now, only supports SPI_HOST_1 and SPI_HOST_2.
 *
 * @param host SPI peripheral to allocate device on
 *
 * @param miso_io_num	GPIO pin for Master In Slave Out (=spi_q) signal, or -1 if not used.
 *
 * @param mosi_io_num	GPIO pin for Master Out Slave In (=spi_d) signal, or -1 if not used.
 *
 * @param sclk_io_num	GPIO pin for Spi CLocK signal, or -1 if not used.
 *
 * @param max_transfer_sz	Maximum transfer size, in bytes. Defaults to 4094 if 0.
 *
 * @return
 *         - CORE_ERR_INVALID_ARG   if configuration is invalid
 *         - CORE_ERR_INVALID_STATE if host already is in use
 *         - CORE_ERR_NO_MEM        if out of memory
 *         - CORE_OK                on success
 */
core_err spi_master_bus_config(uint8_t host, int miso_io_num, int mosi_io_num, int sclk_io_num,
		int max_transfer_sz);

/**
 * @brief Allocate a client device on a SPI bus
 *
 * This initializes the internal structures for a device, plus allocates a CS pin on the indicated SPI master
 * peripheral and routes it to the indicated GPIO. All SPI master devices have three CS pins and can thus control
 * up to three devices.
 *
 * @note While in general, speeds up to 80MHz on the dedicated SPI pins and 40MHz on GPIO-matrix-routed pins are
 *       supported, full-duplex transfers routed over the GPIO matrix only support speeds up to 26MHz.
 *
 * @param host SPI peripheral to allocate device on
 *
 * @param clock_speed_hz Clock speed, divisors of 80MHz, in Hz. See ``SPI_MASTER_CORE_FREQ_*``.
 *
 * @param spics_io_num	CS GPIO pin for this device, or -1 if not used
 *
 * @param spi_drv_handle Pointer to variable to hold the device handle
 *
 * @return
 *         - CORE_ERR_INVALID_ARG   if parameter is invalid
 *         - CORE_ERR_NOT_FOUND     if host doesn't have any free CS slots
 *         - CORE_ERR_NO_MEM        if out of memory
 *         - CORE_OK                on success
 */
core_err spi_master_device_interface_config(uint8_t host, int clock_speed_hz, int spics_io_num,
		spi_pf_device_handle_t *spi_drv_handle);

/**
 * @brief Send a SPI transaction, wait for it to complete, and return the result
 *
 * @note This function is not thread safe when multiple tasks access the same SPI device.
 *
 * @param handle Device handle obtained using SPI_HOST
 *
 * @param tx_data	Pointer to transmit buffer, or NULL for no MOSI phase
 *
 * @param rx_data	Pointer to receive buffer, or NULL for no MISO phase.
 *
 * @param tx_length_in_byte		length of the tx-data in byte
 *
 * @return
 *         - CORE_ERR_INVALID_ARG   if parameter is invalid
 *         - CORE_OK                on success
 */
core_err spi_master_device_transmit(spi_pf_device_handle_t spi_drv_handle, void * tx_data, void* rx_data,
		size_t tx_length_in_byte);

/**
 * @brief Remove a device from the SPI bus
 *
 * @param handle Device handle to free
 *
 * @return
 *         - CORE_ERR_INVALID_ARG   if parameter is invalid
 *         - CORE_ERR_INVALID_STATE if device already is freed
 *         - CORE_OK                on success
 */
core_err spi_master_remove_device_from_bus(spi_pf_device_handle_t spi_drv_handle);

/**
 * @brief Free a SPI bus
 *
 * @warning In order for this to succeed, all devices have to be removed first.
 *
 * @param host SPI peripheral to free
 *
 * @return
 *         - CORE_ERR_INVALID_ARG   if parameter is invalid
 *         - CORE_ERR_INVALID_STATE if not all devices on the bus are freed
 *         - CORE_OK                on success
 */
core_err spi_master_bus_free(uint8_t host);


#ifdef __cplusplus
}
#endif

#endif /* SRC_PERIPHERALS_SPI_SPI_MASTER_CORE_H_ */
