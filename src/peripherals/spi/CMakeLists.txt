set(srcs
	spi_master_core.c
	spi_slave_core.c
	spi_master_platform.c
	spi_slave_platform.c)

add_library(spi STATIC ${srcs})

target_include_directories(spi 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)