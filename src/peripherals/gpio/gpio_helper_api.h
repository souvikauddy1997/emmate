/*
 * File Name: gpio_helper_api.h
 *
 * Description:
 *
 *  Created on: 24-Aug-2019
 *      Author: Noyel Seth
 */

#ifndef GPIO_HELPER_API_H_
#define GPIO_HELPER_API_H_

#include "gpio_core.h"
#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#define HIGH 	1
#define LOW		0

#define ON		HIGH
#define OFF		LOW


#define	INTERRUPT_ATTRIBUTES	GPIO_IO_INTERRUPT

/**
 * @brief GPIO common configuration
 *
 * @param gpio_num GPIO number.
 *
 * @param direction	Configure GPIO's Direction-mode
 * 					Mode: 	GPIO_IO_MODE_DISABLE
 * 							GPIO_IO_MODE_INPUT
 * 							GPIO_IO_MODE_OUTPUT
 * 							GPIO_IO_MODE_OUTPUT_OD
 * 							GPIO_IO_MODE_INPUT_OUTPUT_OD
 * 							GPIO_IO_MODE_INPUT_OUTPUT
 *
 * @param pull_mode	Configure GPIO's Internal pull mode
 * 					Mode:	GPIO_IO_PULLUP_ONLY
 * 							GPIO_IO_PULLDOWN_ONLY
 * 							GPIO_IO_PULLUP_PULLDOWN
 * 							GPIO_IO_FLOATING
 *
 * @return
 *     - CORE_OK on success
 *     - CORE_ERR_INVALID_ARG GPIO Parameter error
 *
 */
core_err configure_gpio(uint32_t gpio_num, gpio_io_mode direction, gpio_io_pull_mode pull_mode);


/**
 * @brief Add ISR handler for the corresponding GPIO pin.
 *
 * @param gpio_num GPIO number
 *
 * @param intr_type	Configure GPIO's Input Interrupt mode
 * 					Mode:	GPIO_INTERRUPT_DISABLE
 * 							GPIO_INTERRUPT_POSEDGE
 * 							GPIO_INTERRUPT_NEGEDGE
 * 							GPIO_INTERRUPT_ANYEDGE
 * 							GPIO_INTERRUPT_LOW_LEVEL
 * 							GPIO_INTERRUPT_HIGH_LEVEL
 *
 * @param intr_isr_handler	Configure GPIO's Interrupt Service Routine callback function, which invoked when an interrupt occurs.
 * 							Its syntax looks like below.
 *
 * 							void INTERRUPT_ATTRIBUTES isr_func(void* arg){
 * 								uint32_t gpio_num = (uint32_t) arg;
 *								// ISR handler body
 * 							}
 *
 * @return
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_STATE Wrong state, the ISR service has not been initialized.
 *     - CORE_ERR_INVALID_ARG Parameter error
 */
core_err add_gpio_isr(uint32_t gpio_num, gpio_interrupt_type intr_type, gpio_pf_io_isr intr_isr_handler);


/**
 * @brief Remove ISR handler for the corresponding GPIO pin.
 *
 * @param gpio_num GPIO number
 *
 * @return
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_STATE Wrong state, the ISR service has not been initialized.
 *     - CORE_ERR_INVALID_ARG Parameter error
 */
core_err remove_gpio_isr(uint32_t gpio_num) ;


/**
 * @brief  GPIO get input value/level
 *
 * @warning If the GPIO is not configured for input (or input and output) the returned value is always 0.
 *
 * @param  gpio_num GPIO number.
 *
 * @return
 *     - 0 the GPIO input level is 0
 *     - 1 the GPIO input level is 1
 *
 */
uint8_t get_gpio_value(uint32_t gpio_num);

/**
 * @brief  GPIO set output value/level (HIGH / LOW)
 *
 * @param  gpio_num GPIO number.
 * @param  level Output level. 0: LOW ; 1: HIGH
 *
 * @return
 *     - CORE_OK Success
 *     - CORE_ERR_INVALID_ARG GPIO number error
 *
 */
core_err set_gpio_value(uint32_t gpio_num, uint8_t level);



#endif /* GPIO_HELPER_API_H_ */
