# GPIO (General Purpose Input Output)
## Overview
General Purpose Input Ouputs are the IO pins of a micro-controller/processor. Each pin can have 2 states, i.e. ON(1) or OFF(0). We can read the value from a pin, or set a desired value to a pin using this module's functions. We can even configure a pin to generate an interrupt.


## How to use this module from an application

##### Header(s) to include

```
gpio_helper_api.h		--> recommended
gpio_core.h
```

#### Module Specific Configurations

---
**This module is a mandatory requirement for the framework, please do not disable it from the Menuconfig Application**


![](img/gpio-config.png)
---

#### Module Specific APIs

**See the examples and API documentation for a better understanding**

- GPIO Configuration - `configure_gpio()`
- Get GPIO Value - `get_gpio_value()`
- Set GPIO Value - `get_gpio_value()`
- Add an ISR to a GPIO - `add_gpio_isr()`
- Remove ISR from a GPIO - `remove_gpio_isr()`