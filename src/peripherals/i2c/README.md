# I2C (Inter-Integrated Circuit)
## Overview
This module provide APIs to interface any I2C with the an EmMate application.

The I2C module supports multiple devices to be used at a time 


## How to use this module from an application

##### Header(s) to include

```
i2c_core.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

![](img/i2c-config.png)

#### Module Specific APIs

**See the examples for a better understanding**

- Initialization

```c
	i2c_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, SDA_PIN, SCL_PIN, CLK_SPEED, 1); // initialize I2C master peripheral
	if (i2c_drv_handle != NULL) {
		CORE_LOGI(TAG, "I2C driver initialize complete");
	} else {
		CORE_LOGE(TAG, "Failed to initialize the I2C driver");
	}
```

- Reading data

```c
			ret = i2c_core_master_read_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS + i, &data_rd, size, 0); // Data read and store in array
			if (ret == CORE_OK) {
				printf("%c", (char) data_rd);  // print the data
			} else {
				CORE_LOGE(TAG, "Failed to read data");
			}
```


- Writing data

```c
			ret = i2c_core_master_write_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS + i, &data_wr[i], size); // Data write
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to write '%c' data", data_wr[i]);
			}
```