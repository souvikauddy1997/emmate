set(srcs
	led-pwm/led_pwm_driver.c
	led-pwm/led_pwm_platform_driver.c)

add_library(pwm STATIC ${srcs})


# Private linkages
list(APPEND EXTRA_LIBS emmate_config)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

target_link_libraries(pwm PRIVATE ${EXTRA_LIBS})

target_include_directories(pwm 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/led-pwm
							)