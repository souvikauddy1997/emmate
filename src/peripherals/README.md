# Peripherals
## Overview
The Peripheral module provides features to the application to interface with external peripherals with the help of sub modules like ADC, I2C, SPI, UART, GPIO etc.

## Submodules

- ADC - For more details see [Details of ADC Module](adc/README.md)
- GPIO - For more details see [Details of GPIO Module](gpio/README.md)
- I2C - For more details see [Details of I2C Module](i2c/README.md)
- PWM - For more details see [Details of PWM Module](pwm/README.md)
- UART - For more details see [Details of UART Module](uart/README.md)
- SPI - not yet implemented

## How to use this module from an application

##### Header(s) to include

**There is no common header file for this module, include sub-module specific header files**

#### Module Specific Configurations

The following configurations are needed for the Peripheral module to work.

![](img/peripheral-config.png)

#### Module Specific APIs

**See sub-module's APIs for details**