set(srcs
	uart_core.c
	uart_helper_api.c
	uart_platform.c)

add_library(uart STATIC ${srcs})

# Private linkages
list(APPEND EXTRA_LIBS emmate_config)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_THREADING)
	list(APPEND EXTRA_LIBS threading)
endif()

if(CONFIG_USE_SYSTEM)
	list(APPEND EXTRA_LIBS system)
endif()

if(CONFIG_USE_UTILS)
	list(APPEND EXTRA_LIBS utils)
endif()

# Public linkages
if(CONFIG_USE_COMMON AND CONFIG_USE_DATA_STRUCTURES)
	list(APPEND EXTRA_PUBLIC_LIBS data-structures)
endif()

# Link the libraries
target_link_libraries(uart PRIVATE ${EXTRA_LIBS})
target_link_libraries(uart PUBLIC ${EXTRA_PUBLIC_LIBS})


target_include_directories(uart 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)