/*
 * File Name: sys_heartbeat.h
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYS_HEARTBEAT_H_
#define SYS_HEARTBEAT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#if CONFIG_USE_FOTA
#include "fota_core.h"
#endif
#include "version.h"
#include "sys_standby.h"
#include "netifinfo.h"

/**
 * @brief	Data structure to contain a SYS Heartbeat's POST request data to be sent to the server
 *
 * @note	SYS Heartbeat request JSON structure:
 *	{
 *		"somthing_id": "",
 *		"core_version": "",
 *		"app_version": ""
 *	}
 *
 * */
typedef struct {
	char somthing_id[SOMTHING_ID_LEN + 1]; /**< A System On Module (SOM) unique identifier. To be fetched from a hardwired silicon UID */
	char core_version[CORE_VERSION_NUMBER_LEN + 1]; /**< Core Framework's version number */
	char app_version[APP_VERSION_NUMBER_LEN + 1]; /**< Core Application's version number */
	NetIFInfo netifinfo;	/**< System's Network Peripherals info */
} SysHeartbeatRequest;

/**
 * @brief	Data structure to contain a SYS Heartbeat's response from the server
 *
 * @note	SYS Heartbeat response JSON structure:
 *
 *	{
 *		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		},
 * 		"fota": {
 * 			"id": "",						// Fota ID: all fota status activities to be made using this id
 *			"stat": true,
 *			"url_len": 62,
 *			"url": "https://dev.iquesters.com/emb_api/test_pis/upload/firmware.hex",
 *			"cert_len": 0,
 *			"cert": null,
 *			"ver": "0.0.0.1",
 *			"fname": "firmware.hex",
 *			"fsize": 18410,
 *			"sch": "1559735400"
 *		}
 *		"sysconf": true,
 *		"appconf": true
 * 		"networkreset": true,				// If true, then the device stop  all operations go to device configuration mode
 * 		"hw_rel": true,						// Whether a SOM has been released from a SoMThing or not
 *	}
 *
 * */
typedef struct {
	int status; /**< Sys Heartbeat Status. Success or Failure */
	CoreError error; /**< Error object */
#if CONFIG_USE_FOTA
	FotaInfo fota; /**< FOTA Info */
#endif
	int sysconf; /**< check if there is system specific configurations */
	int appconf; /**< check if application has app specific configurations */
	int networkreset; /**< Whether to do a device configuration reset or not */
	int hw_rel; /**< Whether a SOM has been released from a SoMThing or not */
} SysHeartbeatResponse;

/**
 * @brief	This function starts the system's heartbeat process.
 *
 * 			A heartbeat is a process where a device hits the server at a fixed time interval to let the server know
 * 			that it is alive.
 *
 * 			This function internally starts a low priority thread/task which loops at a fixed interval which can be
 * 			configured using the macro CONFIG_SYSTEM_HEARTBEAT_INTERVAL.
 *
 * 			In this heartbeat process the system informs the following informations to the server:
 * 			1. The device's SOM ID
 * 			2. The running core and application version numbers
 * 			3. Basic system diagnostics report
 *
 * 			As a response from the server, the system receives the following informations:
 * 			1. The status of this post with any error
 * 			2. The FOTA information
 *
 * @note	init_system_heartbeat() must be called before calling this function
 *
 * 	@return
 * 			- CORE_OK	If the heartbeat was started successfully
 * 			- CORE_FAIL	If failure
 * */
core_err migcloud_start_heartbeat();

#ifdef __cplusplus
}
#endif

#endif /* SYS_HEARTBEAT_H_ */
