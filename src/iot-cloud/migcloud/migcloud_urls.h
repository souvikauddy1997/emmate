/*
 * migcloud_urls.h
 *
 *  Created on: 25-Jan-2020
 *      Author: Rohan Dey
 */

#ifndef MIGCLOUD_URLS_H_
#define MIGCLOUD_URLS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define MIGCLOUD_PROD_SERVER	1
#define MIGCLOUD_DEV_SERVER		0

#if MIGCLOUD_PROD_SERVER

#define IQ_HOST								"migcloudiot.ml"	// AWS
#define IQ_HOST_PORT 						8443			// AWS

#define MIGCLOUD_INTERNET_CHECKER_URL 		"https://"IQ_HOST""
#define IQ_SOM_REGISTRATION_POST_URL 		"https://"IQ_HOST"/mig/rest/som/register"
#define IQ_SYS_HWIDENTIFY_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/identify"
#define IQ_SYS_HEARTBEAT_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/heartbeat"
#define IQ_SYS_APPCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/app"
#define IQ_SYS_APPCONFIG_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/configuration/update/status"
#define IQ_SYS_SYSCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/system"
#define IQ_SYS_FOTASTATUS_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/fota/update/status"
#define IQ_SYS_DEVCFG_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/devcfg/update/status"
#define IQ_SYS_SYS_STANDBY_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/standby/update/status"
#define IQ_SYS_RELSOM_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/relsom/update/status"
#define IQ_SYS_APPPOSTDATA_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/post/data"
#define IQ_SYS_STATUS_UPDATED_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/update/status"

#define IQ_HOST_ROOTCA "-----BEGIN CERTIFICATE-----\n\
MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n\
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n\
SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n\
GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n\
AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n\
q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n\
SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n\
Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n\
a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n\
/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n\
AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n\
CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n\
bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n\
c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n\
VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n\
ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n\
MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n\
Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n\
AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n\
uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n\
wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n\
X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n\
PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n\
KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n\
-----END CERTIFICATE-----"

#elif MIGCLOUD_DEV_SERVER

#define IQ_HOST								"migclouddev.ml"
#define IQ_HOST_PORT 						8443

#define MIGCLOUD_INTERNET_CHECKER_URL 		"https://"IQ_HOST""
#define IQ_SOM_REGISTRATION_POST_URL 		"https://"IQ_HOST"/mig/rest/som/register"
#define IQ_SYS_HWIDENTIFY_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/identify"
#define IQ_SYS_HEARTBEAT_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/heartbeat"
#define IQ_SYS_APPCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/app"
#define IQ_SYS_APPCONFIG_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/configuration/update/status"
#define IQ_SYS_SYSCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration/system"
#define IQ_SYS_FOTASTATUS_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/fota/update/status"
#define IQ_SYS_DEVCFG_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/devcfg/update/status"
#define IQ_SYS_SYS_STANDBY_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/standby/update/status"
#define IQ_SYS_RELSOM_STATUS_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/relsom/update/status"
#define IQ_SYS_APPPOSTDATA_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/post/data"
#define IQ_SYS_STATUS_UPDATED_POST_URL		"https://"IQ_HOST"/mig/rest/somthing/update/status"

#define IQ_HOST_ROOTCA "-----BEGIN CERTIFICATE-----\n\
MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n\
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n\
SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n\
GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n\
AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n\
q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n\
SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n\
Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n\
a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n\
/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n\
AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n\
CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n\
bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n\
c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n\
VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n\
ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n\
MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n\
Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n\
AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n\
uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n\
wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n\
X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n\
PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n\
KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n\
-----END CERTIFICATE-----"

#endif

#ifdef __cplusplus
}
#endif

#endif /* MIGCLOUD_URLS_H_ */
