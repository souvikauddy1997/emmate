/*
 * release_somthing.h
 *
 *  Created on: 11-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef RELEASE_SOMTHING_H_
#define RELEASE_SOMTHING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

/**
 * @brief	Function pointer type to point to a release somthing event notification callback
 *
 * @note	The function signature should be as follows:
 *
 * 			void app_release_somthing(void)
 */
typedef void (*migcloud_releasesomthing_function)(void);

/**
 * @brief		This function registers a function of type migcloud_releasesomthing_function, which will be called when a release somthing
 * 				request is received from the server. This request can be received when a authentic user presses the release som
 * 				button in the migCloud portal
 *
 * @param[in]	releasesomthing_func This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err migcloud_register_release_somthing_event_function(migcloud_releasesomthing_function releasesomthing_func);


#ifdef __cplusplus
}
#endif

#endif /* RELEASE_SOMTHING_H_ */
