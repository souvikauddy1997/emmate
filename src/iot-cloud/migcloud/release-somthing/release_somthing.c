/*
 * release_somthing.c
 *
 *  Created on: 11-Nov-2019
 *      Author: Rohan Dey
 */


#include "release_somthing.h"
#include "migcloud_http_status_update.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_HMI
#include "system_hmi_led_notification.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include "threading.h"
#include <string.h>

#define TAG LTAG_SYSTEM_RELSOM
#if 0
#define RELSOM_STATUS_RESPONSE_SIZE	(sizeof(int) + sizeof(CoreError) + 1)

typedef struct {
	int relsom_stat;
} ReleaseSomthingRequest;

typedef struct {
	int status; /**< Status. Success or Failure */
	CoreError error; /**< Error object */
} ReleaseSomthingResponse;

static core_err make_relsom_status_request(char **ppbuf, int *plen, ReleaseSomthingRequest *relsomstat_req) {
//	core_err ret = CORE_FAIL;
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	CORE_LOGV(TAG, "Going to make Device Configuration Reset Status JSON with the following values:");

	/* Set JSON key value */
	CORE_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(relsomstat_req->relsom_stat, "->"), relsomstat_req->relsom_stat);
	json_object_set_number(root_object, GET_VAR_NAME(relsomstat_req->relsom_stat, "->"),
			(int) relsomstat_req->relsom_stat);

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "Device Configuration Reset Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_devcfg_status_request malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;
}

static core_err parse_relsom_status_response(char *json_buff, ReleaseSomthingResponse *relsomstat_resp) {
	core_err ret = CORE_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &relsomstat_resp->status, &relsomstat_resp->error);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = CORE_FAIL;
	}

	return ret;
}

static core_err send_relsom_stat_to_server(bool relsom_stat) {
	core_err ret = CORE_FAIL;

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len = 0;

	ReleaseSomthingRequest relsomstat_req;
	memset(&relsomstat_req, 0, sizeof(ReleaseSomthingRequest));

	relsomstat_req.relsom_stat = (int) relsom_stat;

	ret = make_relsom_status_request(&json_buf, &json_len, &relsomstat_req);
	CORE_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(RELSOM_STATUS_RESPONSE_SIZE, sizeof(char));
	if (http_resp == NULL) {
		CORE_LOGE(TAG, "memory allocation for http response failed");
		goto free_memory;
	}
	memset(http_resp, 0, RELSOM_STATUS_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(IQ_SYS_RELSOM_STATUS_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
	IQ_HOST, HTTP_USER_AGENT,
	CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len, RELSOM_STATUS_RESPONSE_SIZE,
			&http_stat);

	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

		ReleaseSomthingResponse relsomstat_resp;
		memset(&relsomstat_resp, 0, sizeof(ReleaseSomthingResponse));

		ret = parse_relsom_status_response(http_resp, &relsomstat_resp);
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Release SOM Status Response parsing failed, don't know what to do yet!!");
			// TODO: Handle error
		}
	} else {
		CORE_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
		ret = CORE_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	CORE_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
#endif

static migcloud_releasesomthing_function m_releasesomthing_func = NULL;

core_err migcloud_register_release_somthing_event_function(migcloud_releasesomthing_function releasesomthing_func) {
	if (releasesomthing_func != NULL) {
		m_releasesomthing_func = releasesomthing_func;
		return CORE_OK;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}

static void notify_release_somthing_event() {
	if (m_releasesomthing_func != NULL)
		m_releasesomthing_func();
}

void do_release_somthing_operation() {
	core_err ret = CORE_FAIL;

	CORE_LOGW(TAG, "Going to do SOMTHING release operations ...");

	/* Notify the application about the release somthing event */
	notify_release_somthing_event();

	show_system_resetting_notification();
	TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);

	/* Notify the application about a system reboot event */
	notify_system_reboot_event();

#if CONFIG_USE_PERSISTENT
	CORE_LOGW(TAG, "Erasing the SOMTHING ID ...");
	ret = erase_somthing_id_from_persistent_mem();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to erase persistent memory, SOMTHING ID release is not successful");
		ret = CORE_FAIL;
	}
#endif

#if 1	// Open this once the server side is developed
	/* Inform the server */
	if (ret == CORE_OK) {
		ret = migcloud_send_status_via_http(MIGCLOUD_TASK_RELEASE_SOMTHING, MIGCLOUD_STATUS_PROCESSED, NULL);
	} else {
		ret = migcloud_send_status_via_http(MIGCLOUD_TASK_RELEASE_SOMTHING, MIGCLOUD_STATUS_PROCESS_FAILED, NULL);
	}
#endif

	CORE_LOGW(TAG, "Dispatching reboot event ...");
	core_system_restart();
}
