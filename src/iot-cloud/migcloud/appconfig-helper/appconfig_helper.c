/*
 * appconfig_helper.c
 *
 *  Created on: 07-Aug-2019
 *      Author: Rohan Dey
 */

#include "appconfig_helper.h"
#include "appconfig_helper_status.h"
#include "migcloud_http_status_update.h"
#include "http_client_api.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
#include "migcloud_urls.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_CONN
#include "conn.h"
#endif

#include <string.h>

#define TAG	LTAG_MIG_APPCFG

// The value of CONFIG_APPCONFIG_MAX_SIZE comes from menuconfig
#define MAX_APPCONFIG_SIZE		CONFIG_APPCONFIG_MAX_SIZE
#define MAX_RESPONSE_BUFF_SIZE	(CONFIG_APPCONFIG_MAX_SIZE + sizeof(int) + sizeof(CoreError) + 1)

static migcloud_appconfig_function m_appcfg_func = NULL;

/**
 * @brief	Data structure to contain a APP Configuration's response from the server
 *
 * @note	APP Configuration response JSON structure:
 *
 *	{
 *		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		},
 * 		"config": "{}"
 * 	}
 * */
typedef struct {
	int status; /**< Status. Success or Failure */
	CoreError error; /**< Error object */
	char config[MAX_APPCONFIG_SIZE]; /**< Application configuration */
} AppConfigHelperResponse;

static core_err _parse_appconfig_response_json(char *json_buff, AppConfigHelperResponse *appcfg_resp) {
	core_err ret = CORE_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched");
			ret = CORE_FAIL;
//			goto free_memory;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &appcfg_resp->status, &appcfg_resp->error);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = CORE_FAIL;
				goto free_memory;
			}

			/* Check if response is successful */
			if (appcfg_resp->status) {
				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(appcfg_resp->config, "->"), appcfg_resp->config);
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(appcfg_resp->config, "->"));
					ret = CORE_FAIL;
					goto free_memory;
				}
				ret = CORE_OK;
			}
		}

		free_memory: json_value_free(root_value); /* clear root_value */
	} else {
		CORE_LOGE(TAG, "Could not create JSON root object");
		ret = CORE_FAIL;
	}
	return ret;
}

static void _print_appconfig_response_json(AppConfigHelperResponse *appcfg_resp) {
	CORE_LOGD(TAG, "Parsing App Get Configuration response completed... The following data was received:");
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(appcfg_resp->status, "->"), appcfg_resp->status);
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(appcfg_resp->error.err_code, "->"), appcfg_resp->error.err_code);
	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(appcfg_resp->config, "->"), appcfg_resp->config);
}

core_err get_configurations_for_application(char *req_data, int req_len) {
	core_err ret = CORE_FAIL;

	if (
#if CONFIG_USE_INTERNET_CHECKER
			(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE
#else

			((get_network_conn_status() & NETCONNSTAT_DISCONNECTED)	== NETCONNSTAT_DISCONNECTED) ||  ((get_network_conn_status() & NETCONNSTAT_CONNECTING)	== NETCONNSTAT_CONNECTING)
#endif
	) {
#if CONFIG_USE_INTERNET_CHECKER
				CORE_LOGE(TAG, "Failed to complete AppConfiguration, IoT Cloud not present");
				return ret = CORE_ERR_INVALID_STATE;
#else
				CORE_LOGE(TAG, "Failed to complete AppConfiguration, Network not present");
				return ret = CORE_ERR_INVALID_STATE;
#endif
	}

	CORE_LOGI(TAG, "APPCONFIG Request: %.*s", req_len, req_data);

	AppConfigHelperResponse *appcfg_resp = NULL;
	char *http_resp = NULL;
	size_t http_resp_len = 0;

	/* Allocate memory for AppConfigHelperResponse */
	appcfg_resp = (AppConfigHelperResponse*) calloc(1, sizeof(AppConfigHelperResponse));
	if (appcfg_resp == NULL) {
		CORE_LOGE(TAG, "memory allocation for AppConfigHelperResponse failed");
		ret = CORE_FAIL;
		goto free_memory;
	}
	memset(appcfg_resp, 0, sizeof(AppConfigHelperResponse));

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(MAX_RESPONSE_BUFF_SIZE, sizeof(char));
	if (http_resp == NULL) {
		CORE_LOGE(TAG, "memory allocation for http response failed");
		ret = CORE_FAIL;
		goto free_memory;
	}
	memset(http_resp, 0, MAX_RESPONSE_BUFF_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
#if (!MIGCLOUD_API_TEST)
	ret = do_http_operation(IQ_SYS_APPCONFIG_POST_URL, IQ_HOST_PORT,
			IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
			IQ_HOST, HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, req_data, req_len, http_resp,
			&http_resp_len, MAX_RESPONSE_BUFF_SIZE, &http_stat);
#else
		ret = do_http_operation("http://api.myjson.com/bins/j0jjm", 8080, NULL, HTTP_CLIENT_METHOD_GET, "api.myjson.com",
		HTTP_USER_AGENT,
		CONTENT_TYPE_APPLICATION_JSON, req_data, req_len, http_resp, &http_resp_len, MAX_RESPONSE_BUFF_SIZE, &http_stat);
#endif

	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "APPCONFIG Response: %.*s", http_resp_len, http_resp);

		ret = _parse_appconfig_response_json(http_resp, appcfg_resp);
		if (ret == CORE_OK) {
			_print_appconfig_response_json(appcfg_resp);

			/* Call the application function to send the configurations */
			if (m_appcfg_func != NULL) {
				ret = m_appcfg_func(appcfg_resp->config,
						strlen(appcfg_resp->config));
				if (ret == CORE_OK) {
					ret = migcloud_send_status_via_http(
							MIGCLOUD_TASK_APP_CONFIG, MIGCLOUD_STATUS_UPDATED,
							NULL);
				} else {
					ret = migcloud_send_status_via_http(
							MIGCLOUD_TASK_APP_CONFIG,
							MIGCLOUD_STATUS_UPDATE_FAILED, NULL);
					ret = CORE_FAIL;
				}
			} else {
				/* No callback is registered by the application */
				ret = CORE_FAIL;
			}
		} else {
			CORE_LOGE(TAG,
					"Failed to parse application configuration response json");
			ret = CORE_FAIL;
		}
	} else {
		CORE_LOGE(TAG, "HTTP failed with status code = [ %d ]", http_stat);
		ret = CORE_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	CORE_LOGD(TAG, "Freeing allocated memory");
	free(appcfg_resp);
	free(http_resp);

	return ret;
}

core_err migcloud_register_app_config_function(migcloud_appconfig_function appcfg_func) {
	if (appcfg_func != NULL) {
		m_appcfg_func = appcfg_func;
		return CORE_OK;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}
