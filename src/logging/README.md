# Logging Module
## Overview
The Logging module provides API to print log messages. The logging module is very flexible and offers many runtime features which will help an application developer to configure and manage their log messages.

## Features
- Log Levels - None, Verbose, Debug, Info, Warning, Error
- Log ouput - Console (UART), Network (UDP streaming), File (to SDMMC Card)
- Ability to change the log level in runtime
- Ability to change the log output in runtime *(not yet implemented)*
- Group log message through TAG
- Use the TAG as an identifier to change/modify the log level at runtime

#### Submodules
- file-appender - Saves log message to a local file (SD Card). *(not yet implemented)*
- udp-log-streamer - Streams log messages to a UDP server *(under development)*

## How to use this module from an application
To get a good understanding about this module, please see the examples. Currently, only console logging example is available. Other examples will be available soon.

##### Header(s) to include

```
core_logger.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

- Console Logging

![](img/logging-config.png)

#### Module Specific APIs
To get a good understanding about this module, please see the examples. Currently, only console logging example is available. Other examples will be available soon.