/*
 * File Name: udp_log_streamer.h
 * Description:
 *
 *  Created on: 03-May-2019
 *      Author: Rohan Dey
 */

#ifndef UDP_LOG_STREAMER_H_
#define UDP_LOG_STREAMER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_common.h"
#include "core_constant.h"
#include <stdarg.h>

#define UDP_STREAMER_MIN_TRANSMIT_SIZE		CONFIG_UDP_STREAMER_MAX_TRANSMIT_SIZE
#define UDP_STREAMER_MAX_QUEUE_SIZE			(CONFIG_UDP_STREAMER_MAX_TRANSMIT_SIZE * 4)
#define UDP_LOG_STREAMER_SERVER				CONFIG_UDP_LOG_STREAMER_SERVER
#define UDP_LOG_STREAMER_PORT				CONFIG_UDP_LOG_STREAMER_PORT

/*
 * @brief This function starts the UDP Log Streamer
 *
 * @return
 *		- CORE_OK		Successfully started
 *  	- CORE_FAIL		Could not start
 * */
core_err start_udp_log_streamer();

/*
 * @brief This function stops the UDP Log Streamer
 *
 * @return
 *		- CORE_OK		Successfully stopped
 *  	- CORE_FAIL		Could not stop
 * */
core_err kill_udp_log_streamer();

/*
 * @brief This function returns if the UDP Log Streamer is ready to operate or not
 *
 * @return
 *		- true		The streamer is ready to accept data
 *  	- false		The streamer is not ready
 * */
bool is_udp_log_streamer_ready();

/*
 * @brief This function enqueues the log to be sent to udp server
 *
 * @param[in]	log		The formatted string
 * @param[in]	list	va_list
 *
 * @return
 *		- true		The streamer is ready to accept data
 *  	- false		The streamer is not ready
 * */
#if 0
core_err send_log_to_udp_server(const char *buf, int len);
#else
void send_log_to_udp_streamer(const char *str, va_list list, int len);
#endif

#ifdef __cplusplus
}
#endif

#endif /* UDP_LOG_STREAMER_H_ */
