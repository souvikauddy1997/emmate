/*
 * logging.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include <string.h>
#include <stdbool.h>

#include "core_logger.h"
#include "logger_constants.h"
#include "threading.h"
#if (CONFIG_USE_UDP_LOG_STREAMER)
#include "udp_log_streamer.h"
#endif

/* Platform logging function wrappers */
/*-------------------------------------------------------------------------------------------------------------------*/
#define core_log_level_set(tag, level)	/*void*/ pf_log_level_set(/*const char * */tag, /*CORE_LOG_LEVEL*/ level)
#define core_log_redirect(func)			/*vprintf_like_t*/ pf_log_redirect(/*vprintf_like_t*/ func)
#define core_log_timestamp				/*uint32_t*/ pf_log_timestamp(/*void*/)
#define core_log_early_timestamp		/*uint32_t*/pf_log_early_timestamp(/*void*/)


/* Logger datatypes */
/*-------------------------------------------------------------------------------------------------------------------*/
typedef struct {
	bool is_log_enabled;
	CORE_LOG_LEVEL user_set_level;
	uint8_t log_output;
#if (CONFIG_USE_FILE_APPENDER)
	ThreadHandle file_appender_hndl;
	QueueHandle file_appender_que;
#endif
} CoreLoggerCntl;

/* Static variables and functions */
/*-------------------------------------------------------------------------------------------------------------------*/
static CoreLoggerCntl log_cntl;

/* all logs will be finally sent to this function.
 * final output will be sent from here
 * */
static int core_log_handler(const char *str, va_list list) {

	switch (log_cntl.log_output) {

	case CORE_LOG_OUTPUT_CONSOLE: {
		vprintf(str, list);
		break;
	}
	case CORE_LOG_OUTPUT_FILE: {
#if (CONFIG_USE_FILE_APPENDER)
		// TODO: send data to "file-appender" queue
#endif
		break;
	}
	case CORE_LOG_OUTPUT_NETWORK: {
#if (CONFIG_USE_UDP_LOG_STREAMER)
//		send_log_to_udp_streamer(str, list, len);
#endif
		break;
	}
	case 3: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE
		vprintf(str, list);
#if (CONFIG_USE_FILE_APPENDER)
		// TODO: send data to "file-appender" queue
#endif
		break;
	}
	case 5: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_NETWORK
		vprintf(str, list);
#if (CONFIG_USE_UDP_LOG_STREAMER)
//		send_log_to_udp_streamer(str, list, len);
#endif
		break;
	}
	case 6: { // CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK
#if (CONFIG_USE_FILE_APPENDER)
		// TODO: send data to "file-appender" queue
#endif
#if (CONFIG_USE_UDP_LOG_STREAMER)
//		send_log_to_udp_streamer(str, list, len);
#endif
		break;
	}
	case 7: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK
		int len = vprintf(str, list);
#if (CONFIG_USE_FILE_APPENDER)
		// TODO: send data to "file-appender" queue
#endif
#if (CONFIG_USE_UDP_LOG_STREAMER)
		send_log_to_udp_streamer(str, list, len);
#endif
		break;
	}
	default: {
		// Do nothing
		break;
	}
	} // switch (log_cntl.log_output)

	return 0;
}

#if (CONFIG_USE_FILE_APPENDER)
static core_err start_file_appender() {
	core_err ret = CORE_FAIL;

	return ret;
}

static core_err kill_file_appender() {
	core_err ret = CORE_FAIL;

	return ret;
}
#endif

/* APIs to be called from other modules and application */
/*-------------------------------------------------------------------------------------------------------------------*/

/* set the output where the log is to be sent
 * the input param can be 1 or more values ORed together
 * */
core_err set_core_logger_output(uint8_t output) {
	log_cntl.log_output = output;

	switch (log_cntl.log_output) {

	case CORE_LOG_OUTPUT_CONSOLE: {
		break;
	}
	case CORE_LOG_OUTPUT_FILE: {
#if (CONFIG_USE_FILE_APPENDER)
#if (CONFIG_USE_UDP_LOG_STREAMER)
		kill_udp_log_streamer();
#endif
		start_file_appender();
#else
		return CORE_ERR_INVALID_ARG;
#endif
		break;
	}
	case CORE_LOG_OUTPUT_NETWORK: {
#if (CONFIG_USE_UDP_LOG_STREAMER)
#if (CONFIG_USE_FILE_APPENDER)
		kill_file_appender();
#endif
		start_udp_log_streamer();
#else
		return CORE_ERR_INVALID_ARG;
#endif
		break;
	}
	case 3: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE
#if (CONFIG_USE_FILE_APPENDER)
#if (CONFIG_USE_UDP_LOG_STREAMER)
		kill_udp_log_streamer();
#endif
		start_file_appender();
#else
		return CORE_ERR_INVALID_ARG;
#endif
		break;
	}
	case 5: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_NETWORK
#if (CONFIG_USE_UDP_LOG_STREAMER)
#if (CONFIG_USE_FILE_APPENDER)
		kill_file_appender();
#endif
		start_udp_log_streamer();
#else
		return CORE_ERR_INVALID_ARG;
#endif
		break;
	}
	case 6: { // CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK
#if (CONFIG_USE_FILE_APPENDER)
		return (start_file_appender());
#endif
#if (CONFIG_USE_UDP_LOG_STREAMER)
		return (start_udp_log_streamer());
#endif
		break;
	}
	case 7: { // CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK
#if (CONFIG_USE_FILE_APPENDER)
		return (start_file_appender());
#endif
#if (CONFIG_USE_UDP_LOG_STREAMER)
		return (start_udp_log_streamer());
#endif
		break;
	}
	default: {
		// Do nothing
		break;
	}
	} // switch (log_cntl.log_output)
	return CORE_OK;
}

/* set module wise log level */
void set_core_logger_level(const char *tag, CORE_LOG_LEVEL level) {
	if (strcmp(tag, LTAG_ALL_MODULE) == 0)
		log_cntl.user_set_level = level;
	core_log_level_set(tag, level);
}

void enable_core_logger() {
	set_core_logger_level(LTAG_ALL_MODULE, log_cntl.user_set_level);
}

void disable_core_logger() {
	set_core_logger_level(LTAG_ALL_MODULE, CORE_LOG_NONE);
}

/*
 * init_core_logger sets the underlying platform logging module with a set of values as sent by the calling function
 * Parameters:
 * 		output = output peripheral like console, file network etc.
 * 		level = log level as in CORE_LOG_LEVEL enum
 * */
core_err init_core_logger(uint8_t output, CORE_LOG_LEVEL level) {
	core_err ret = CORE_FAIL;

	log_cntl.is_log_enabled = true;
	log_cntl.user_set_level = level;

	/* set to default values */
#if (CONFIG_USE_FILE_APPENDER)
	log_cntl.file_appender_hndl = 0;
	log_cntl.file_appender_que = 0;
#endif

	/* Set the log output as per input param */
	ret = set_core_logger_output(output);

	/* Set the log level as per input param */
	core_log_level_set(LTAG_ALL_MODULE, level);

	/* Redirect all logs to the core logger's handler */
	core_log_redirect(core_log_handler);

	return ret;
}

