if(CONFIG_USE_BOARDS)
	add_subdirectory(boards)
endif()

if(CONFIG_USE_COMMON)
	add_subdirectory(common)
endif()

if(CONFIG_USE_CONN)
	add_subdirectory(conn)
endif()

if(CONFIG_USE_DOWNLINK)
	add_subdirectory(downlink)
endif()

add_subdirectory(emmate_config)

if(CONFIG_USE_EVENT_GROUP)
	add_subdirectory(event-group)
endif()

if(CONFIG_USE_EXTERNAL_ICS)
	add_subdirectory(external-ics)
endif()

if(CONFIG_USE_FILE_SYSTEM)
	add_subdirectory(file-system)
endif()

if(CONFIG_USE_HMI)
	add_subdirectory(hmi)
endif()

if(CONFIG_USE_INPUT_PROCESSOR)
	add_subdirectory(input-processor)
endif()

if(CONFIG_USE_IOT_CLOUD)
	add_subdirectory(iot-cloud)
endif()

if(CONFIG_USE_LOGGING)
	add_subdirectory(logging)
endif()

if(CONFIG_USE_PERIPHERALS)
	add_subdirectory(peripherals)
endif()

if(CONFIG_USE_PERSISTENT)
	add_subdirectory(persistent)
endif()

add_subdirectory(som)

if(CONFIG_USE_SYSTEM)
	add_subdirectory(system)
endif()

if(CONFIG_USE_TCPIP)
	add_subdirectory(tcpip)
endif()

if(CONFIG_USE_THREADING)
	add_subdirectory(threading)
endif()

if(CONFIG_USE_UTILS)
	add_subdirectory(utils)
endif()

if(CONFIG_USE_VERSION)
	add_subdirectory(version)
endif()
