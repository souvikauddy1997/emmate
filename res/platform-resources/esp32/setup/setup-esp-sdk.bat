@ECHO OFF

rem ######## Setup ESP-IDF(SDK)	

set CURR_DIR=%cd%
rem echo %CURR_DIR%
set PWD=%cd%

set ESP_DIR_PATH=%CURR_DIR%

rem #create ESP directory in $HOME
set ESP_IDF_PATH=%ESP_DIR_PATH%\esp-idf

rem #get the OS type
set OS_TYPE=< python %EMMATE_RELEASE_PATH%\platform-resources\getOSPlatform.py
rem ECHO %OS_TYPE%

rem # DO NOT CHANGE, this is the Checkout id for "v4.2-dev-701-g0ae960f2f"
rem Get Commit id :- git rev-parse HEAD
set ESP_IDF_CHECKOUT_ID=0ae960f2fe92de1ee9c7c624b7115d06faca119e

rem #change directory to $ESP_DIR_PATH
cd %ESP_DIR_PATH%
rem echo %cd%

echo ### Starting ESP-IDF SDK Download #########################
git clone --recursive https://github.com/espressif/esp-idf.git

IF %ERRORLEVEL% EQU 0 (
	cd %ESP_IDF_PATH%
	rem echo %cd%
	
	rem echo ################################
	rem git describe --tags --dirty
	rem echo ################################
	
	rem #git branch

	git checkout -b %ESP_IDF_CHECKOUT_ID%
		
	IF %ERRORLEVEL% EQU 0 (
	
		git submodule update --recursive
		
		IF %ERRORLEVEL% EQU 0 (
			git describe --tags --dirty
		) else (
			echo ### Failed to Update Subcodule #########################
			echo.
			cd %CURR_DIR%
			exit /b %ERRORLEVEL%
		)
	) else (
		echo ### Failed to CheckOut ESP-IDF SDK #########################
		echo.
		cd %CURR_DIR%
		exit /b %ERRORLEVEL%
	)

	rem #git describe --tags --dirty

) else (
	echo ### Failed to Download ESP-IDF SDK #########################
	echo.
	cd %CURR_DIR%
	exit /b %ERRORLEVEL%
)

echo ### Completed ESP-IDF SDK Download #########################
echo.
echo.

rem echo ### Setup SDK Environment Variable #########################
rem echo Please follow the below instructions to set the ESP-IDF SDK environment variables
rem echo.
rem echo Step 1: Set up IDF_PATH by adding the following line to "IDF_PATH=%ESP_DIR_PATH%\esp-idf"
rem echo.
rem echo Step 2: Log off and log in back to make this change effective. Run the following command to check if IDF_PATH
rem echo.
rem echo echo %IDF_PATH%
rem echo.
cd %CURR_DIR%
exit /b %ERRORLEVEL%
