# EmMate Framework

The EmMate Framework is a Open Source multi-architecture, platform-independant middleware that can be used for any micro to small scale embedded application development.

The prime objective of the EmMate Framework is to provide an abstraction between an application and the hardware. The EmMate Framework provides an unified interface for application development, code compilation, application deployment and debugging for multiple embedded platforms.