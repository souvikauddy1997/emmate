@ECHO OFF

:: setting current directory
set CURR_DIR=%cd%
::ECHO %CURR_DIR%

set CURRENT_DATE=%date%
set CURRENT_TIME=%time%

set TOOLS_DIR=%CURR_DIR%\tools\
set DIST_DIR=%TOOLS_DIR%\dist\

rem 7zip related variable
set INSTALLER_7Z_FILE_NAME="7z-installer.exe"
set EXECUTABLE_7ZIP_PATH=""

if not exist %DIST_DIR%\NUL mkdir %DIST_DIR%

echo.
echo #####################################################################
echo Starting Emmate Installation
echo #####################################################################


IF "%EMMATE_PATH%"=="" (
	echo Environment variable 'EMMATE_PATH' is not set
	echo Set The Environment variable 'EMMATE_PATH' = %CURR_DIR%
	setx EMMATE_PATH "%CURR_DIR%"
	
) else (
	echo The Environment variable 'EMMATE_PATH' = %EMMATE_PATH%
)

echo.
rem Unzip tools\python3.zip #############################################################################################################

py -3 -V >nul 2>nul

if %errorlevel% NEQ 0 (
	if "%PROCESSOR_ARCHITECTURE%" == "AMD64" (
		echo Python3 not installed, Install Python3 for Windows 64bit
	) else (
		echo Python3 not installed, Install Python3 for Windows 32bit
	)
	goto end
)

for /f "delims=" %%i in ('py -3 -V') do Current Python Version: '%%i

rem #####################################################################################################################################


rem Set 7z.exe path  #############################################################################################################

if "%PROCESSOR_ARCHITECTURE%" == "AMD64" (
	set EXECUTABLE_7ZIP_PATH="%TOOLS_DIR%\7zip_x64"
) else (
	set EXECUTABLE_7ZIP_PATH="%TOOLS_DIR%\7zip_x86"
)

echo EXECUTABLE_7ZIP_PATH = %EXECUTABLE_7ZIP_PATH%

pause
rem #####################################################################################################################################

rem Unzip sdk\esp-idf.zip #############################################################################################################

echo.
echo ............................
echo Unzip sdk\esp-idf.zip to %CURR_DIR%\sdk
echo ............................

rem unzip at destination folder/directory and overwrite if exist
"%EXECUTABLE_7ZIP_PATH%\7z.exe" x -o"%CURR_DIR%\sdk\" "%CURR_DIR%\sdk\esp-idf.zip" -aoa

rem if %errorlevel% NEQ 0 (
rem 	echo Failed to Unzip sdk\esp-idf.zip.
rem 	goto end
rem )

rem #####################################################################################################################################




echo.
echo.
set IDF_PATH=%CURR_DIR%\sdk\esp-idf
echo %IDF_PATH%
echo.
echo ............................
echo Start SDK installation.
echo ............................

call %IDF_PATH%\install.bat

if "%errorlevel%" EQU "0" (

	call %IDF_PATH%\export.bat
	
	echo.
	echo #####################################################################
	echo Emmate Installation Complete
	echo #####################################################################
	pause
	exit /b 0
	
) else (
	echo Failed to Install the SDK..
	goto end
)


:end
	echo.
	echo #####################################################################
	echo Emmate Installation Failed
	echo #####################################################################
	pause
	exit /b 1 
