#!/bin/bash

if [ -z "$EMMATE_PATH" ]
then
	echo "Environment variable 'EMMATE_PATH' is not set"
	echo "You need to update your 'EMMATE_PATH' environment variable in the ~/.profile file."
	exit 1
fi

#setting current directory
export CURR_DIR=`pwd`

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

echo
echo "#####################################################################"
echo Starting Emmate Installation
echo "#####################################################################"

echo
echo ............................
echo Unzip SDK to $CURR_DIR/sdk
echo ............................

# unzip at destination folder/directory and overwrite if exist 
unzip -o $CURR_DIR/sdk/esp-idf.zip -d $CURR_DIR/sdk/

ret_stat=$?

if [ $ret_stat -eq 0 ]
then
	export IDF_PATH=$CURR_DIR/sdk/esp-idf/
	echo
	echo ............................
	echo Start SDK installation.
	echo ............................
	
	$IDF_PATH/install.sh
	
	ret_stat=$?
	if [ $ret_stat -eq 0 ]
	then
		. $IDF_PATH/export.sh
		
		echo
		echo "#####################################################################"
		echo Emmate Installation Complete
		echo "#####################################################################"
		exit 0
	else
		echo "Failed to Install the SDK.."
		echo
		echo "#####################################################################"
		echo Emmate Installation Failed
		echo "#####################################################################"
		exit 1
	fi
	
else
	echo "Failed to Unzip the SDK.."
	echo
	echo "#####################################################################"
	echo Emmate Installation Failed
	echo "#####################################################################"
	exit 1
fi

