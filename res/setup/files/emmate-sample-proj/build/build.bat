@ECHO OFF

rem  ---------- get the OS type ----------
echo EMMATE_PATH = %EMMATE_PATH%
rem set OS_TYPE=< python %EMMATE_PATH%\platform-resources\getOSPlatform.py
python %EMMATE_PATH%\platform-resources\getOSPlatform.py > tmpFile
rem set /p OS_TYPE= < tmpFile 
for /f "delims=" %%i in (tmpFile) do (
 		rem echo %%i
 		set OS_TYPE=%%i
)
del tmpFile
echo OS_TYPE = %OS_TYPE%

rem  ---------- setting the directories and paths ----------
set CURR_DIR=%cd%
set PWD=%cd%
rem ECHO %CURR_DIR%

rem  ---------- setting the configuration file paths ----------
set CONFIG_PATH=%CURR_DIR%\build.conf
rem ECHO %CONFIG_PATH%
set EMMATE_CONFIG_FILE_PATH=%CURR_DIR%\..\emmate_config
rem ECHO %EMMATE_CONFIG_FILE_PATH%

rem ---------- get the build config variables ----------
rem eol stops comments from being parsed
rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%CONFIG_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set %%a=%%b
)
rem ECHO %BUILD_MODE%
rem pause

rem ---------- set the current date and time ----------
set CURRENT_DATE=%date%
set CURRENT_TIME=%time%


rem ---------- importing the build cmd option helper script ---------- 
set BUILD_OPTION_HELPER_PATH=%CURR_DIR%\build-support\build-helpes.bat

rem ---------- importing the menu configuration script ----------
set MENUCONFIG_EXECUTION_SCRIPT=%CURR_DIR%\build-support\execute-menuconfig.bat
set MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=%CURR_DIR%\menuconfig-gen-execution-status

rem ---------- set project name, src and other variables ----------
rem go to Project Directory
cd ..
set PROJECT_DIR=%cd%
for /f "delims=" %%A in ('cd') do (
     set foldername=%%~nxA
)
set PROJECT_NAME=%foldername%
set APP_SRC_PATH=%PROJECT_DIR%\src
set BIN_DIRECTORY=%PROJECT_DIR%\bin
set CMAKE_BIN_DIR=%PROJECT_DIR%\cmake_build
rem pause

rem ---------- return to current directory ----------
cd %CURR_DIR%

rem ---------- start running the build script ---------- 
ECHO .....................................................................
ECHO Initiating build for %PROJECT_NAME%
ECHO .....................................................................
ECHO.
rem # if BUILD_MODEis not set then set it to gui-config
rem if "%BUILD_MODE%"==""(
rem 	set BUILD_MODE="gui-config"
rem )else(
rem 	echo "all set"
rem )
rem pause
echo.
echo .....................................................................
echo Build Mode             : %BUILD_MODE%
echo Build Date             : %CURRENT_DATE%
echo Build Time             : %CURRENT_TIME%
echo Build By               : %USERNAME%
echo .....................................................................
echo Project Location : %PROJECT_DIR%
echo .....................................................................
echo Distribution Location : %BIN_DIRECTORY%
echo .....................................................................
echo.
rem echo %cd%

rem TODO: remove the PROJ_BUILD_MODE related code from here if not required
rem ---------- setting the build mode ----------
set PROJ_BUILD_MODE=%BUILD_MODE%
echo PROJ_BUILD_MODE = %PROJ_BUILD_MODE%

@setlocal EnableDelayedExpansion

rem ---------- read menuconfig tool's execution status ----------
if exist %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
	echo MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH = %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
	rem eol stops comments from being parsed
	rem otherwise split lines at the = char into two tokens
	for /F "eol=# tokens=1,* delims==" %%a in (%MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%) do (
	    rem proper lines have both a and b set
	    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
	    if NOT "%%a"=="" set %%a=%%b
	    
	 	if "%%a" == "MENUCONFIG_EXECUTION_STATUS" (
			rem echo inside MENUCONFIG_EXECUTION_STATUS=%%b..
			if "%%b" == "y " (
		 		rem #echo "If menuconfig execute then Deleting BIN_DIRECTORY/.PROJECT_NAME if present" >> $LOG_FILE
		 		rem # delete hidden .PROJECT_NAME/* files
		 		rd /s /q %BIN_DIRECTORY%
		 		rem rd /s /q %CMAKE_BIN_DIR%
		 		del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
	 		)
		)	
	)
)

rem ---------- set the build log directory and file ----------
set LOG_DIR=%BIN_DIRECTORY%\log
set LOG_FILE=%LOG_DIR%\build.log
md %LOG_DIR%

rem ---------- start the emmate build ----------
rem --------------------------------------------------
echo Setting up the build...
call %CURR_DIR%\build-support\build_setup.bat > %LOG_FILE%
echo.
echo.

rem ---------- creating binary output directory ----------
mkdir %BIN_DIRECTORY%


rem ################################################################### Execute Menuconfig operation
IF "%1" ==  "-menuconfig"  (
	echo. 
	echo ## Executing EmMate Menuconfig ################################
	call %MENUCONFIG_EXECUTION_SCRIPT% -menuconfig >> %LOG_FILE%
	echo %errorlevel%
	echo.
	echo ## Executing EmMate Menuconfig Successful ################################
	exit /b 0
) 



rem ################################################################### Execute Make operation
if "%1"=="-make" (
	rem TODO: check if any config has changed and then run the menuconfig script
	echo ## Executing EmMate Menuconfig ################################
	call %MENUCONFIG_EXECUTION_SCRIPT% >> %LOG_FILE%
	rem echo %errorlevel%
	rem echo.
	echo ## EmMate Menuconfig Successful ################################

	echo ## Executing EmMate Project Build ################################
	rem #Creating Application Version
	echo Updating the version ...
	call %CURR_DIR%\build-support\version.bat >> %LOG_FILE%
	
	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		)
	)
	
	rem ---------- call the cmake config generation script ----------
	call %CURR_DIR%\build-support\platform-process.bat  >> %LOG_FILE%
	
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Build
		
		rem Copy the esp32 platform specific code to the emmate directory
		EMMATE_RESOURCE=%CURR_DIR%\..\emmate-resource
		EMMATE_SOURCE=%CURR_DIR%\..\emmate
		xcopy /d /y /f %$EMMATE_RESOURCE%\platform %EMMATE_SOURCE%
		
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		rem echo IDF_EXPORT = !IDF_EXPORT!
		call !IDF_EXPORT!
		
		if not exist %CMAKE_BIN_DIR%\NUL mkdir %CMAKE_BIN_DIR%
		cd %CMAKE_BIN_DIR%
		
		call cmake .. -DCMAKE_TOOLCHAIN_FILE=!IDF_PATH!\tools\cmake\toolchain-esp32.cmake -GNinja
		call cmake --build .
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Build

		rem Copy the simulator code to the emmate directory
		EMMATE_RESOURCE=%CURR_DIR%\..\emmate-resource
		EMMATE_SOURCE=%CURR_DIR%\..\emmate
		xcopy /d /y /f %$EMMATE_RESOURCE%\simulator %EMMATE_SOURCE%

	)

	echo errorlevel: %errorlevel%
	
	if "%errorlevel%" NEQ "0" ( 
		echo ## EmMate Build Failed ################################ 
	) else ( 
		echo ## EmMate Build Successful ################################ 
	)
	exit /b 0
)


rem ################################################################### Execute Erase-Flash operation
if "%1"=="-erase-flash" (

	call set com_port=%2
	call set com_baud=%3
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)
	
	echo !com_baud!|find "-b" >nul
	if !errorlevel! EQU 0 ( 
		call set com_baud=%3
		call set com_baud=!com_baud:-b=!
		echo com_baud = !com_baud!
	) else (
		call set "com_baud="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
			if "%%a" == "CONFIG_SERIAL_BAUD_RATE" (
		    	if "!com_baud!"=="" set com_baud=!val!
		    	echo com_baud = !com_baud!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Erase Flash
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		cd %CMAKE_BIN_DIR%
		python !IDF_PATH!\components\esptool_py\esptool\esptool.py -p !com_port! -b !com_baud! erase_flash
		exit /b 0
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Erase Flash
		exit /b 0
	)
)



rem ################################################################### Execute Flash operation
if "%1"=="-flash" (

	call set com_port=%2
	call set com_baud=%3
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)
	
	echo !com_baud!|find "-b" >nul
	if !errorlevel! EQU 0 ( 
		call set com_baud=%3
		call set com_baud=!com_baud:-b=!
		echo com_baud = !com_baud!
	) else (
		call set "com_baud="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
			if "%%a" == "CONFIG_SERIAL_BAUD_RATE" (
		    	if "!com_baud!"=="" set com_baud=!val!
		    	echo com_baud = !com_baud!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Flash
		
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		rem call %IDF_PATH%\export.bat
		rem cd !PROJECT_DIR!\bin\.!PROJECT_NAME!
		rem idf.py flash
	
		cd %CMAKE_BIN_DIR%
		python !IDF_PATH!\components\esptool_py\esptool\esptool.py -p !com_port! -b !com_baud! write_flash @flash_project_args
		exit /b 0
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Flash
		exit /b 0
	)
)



rem ################################################################### Execute Log operation
if "%1"=="-log" (
	
	call set com_port=%2
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Monitor
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		call !IDF_EXPORT!

		cd %CMAKE_BIN_DIR%
		rem set PROJECT_ELF=%PROJECT_NAME%.elf
		rem echo PROJECT_NAME = %PROJECT_NAME%
		rem echo PROJECT_ELF = %PROJECT_ELF%
		python !IDF_PATH!\tools\idf_monitor.py -p !com_port! %PROJECT_NAME%.elf
		exit /b 0
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Monitor
		exit /b 0
	)
)


rem ################################################################### Execute Clean operation
if "%1"=="-clean" (
	rd /s /q %BIN_DIRECTORY% 
rem ------------- CLEAN CMAKE BIN DIRECTORY -------------
	rd /s /q %CMAKE_BIN_DIR%
rem -----------------------------------------------------
	del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% 

	exit /b 0
)

%BUILD_OPTION_HELPER_PATH%

pause