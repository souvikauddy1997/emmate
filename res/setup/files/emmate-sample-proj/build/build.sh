#!/bin/bash

echo EMMATE_PATH = $EMMATE_PATH

# get the OS type
export OS_TYPE=$(python $EMMATE_PATH/platform-resources/getOSPlatform.py)
echo OS_TYPE = $OS_TYPE

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export BUILD_CONFIG_PATH=$CURR_DIR/build.conf
export EMMATE_CONFIG_FILE_PATH=$CURR_DIR/../emmate_config

source $BUILD_CONFIG_PATH
export $(grep --regexp ^[A-Z] $BUILD_CONFIG_PATH | cut -d= -f1)

#importing MenuConfiguration File
export MENUCONFIG_EXECUTION_SCRIPT=$CURR_DIR/build-support/execute-menuconfig.sh
export MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=$CURR_DIR/menuconfig-gen-execution-status
#source $MENUCONFIG_EXECUTION_SCRIPT

export BUILD_HELP_SCRIPT=$CURR_DIR/build-support/build-helps.sh

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

#Collect Project Path & Project Name
#go to Project Directory
cd ..
export PROJECT_DIR=$PWD
export PROJECT_NAME=$(basename $PWD)
export APP_SRC_PATH=$PROJECT_DIR/src
export BIN_DIRECTORY=$PROJECT_DIR/bin
export CMAKE_BIN_DIR=$PROJECT_DIR/cmake_build

#return to Current Directory
cd $CURR_DIR


## Script Functions ###############################################################################
make_executable() {
	local script=$1
	if [ ! -x $script ]
	then
		echo "The script file is not executable...making the script executable"
		#make is executable
		chmod a+x $script
	fi
}

execute_build_helper_menu(){
	echo "Build Helps..."
	make_executable $BUILD_HELP_SCRIPT
	$BUILD_HELP_SCRIPT
	exit 0;	
}

build_initiating_logging_execution(){
	#initiating logging
	export LOG_DIR="$BIN_DIRECTORY/log"
	export LOG_FILE="$LOG_DIR/build.log"

	mkdir -p $LOG_DIR


	echo Setting up the build...
	SETUP_SCRIPT=./build-support/build_setup.sh
	make_executable $SETUP_SCRIPT
	$SETUP_SCRIPT > $LOG_FILE
	echo
	echo
}



## Script Logics ###############################################################################

echo .....................................................................
echo Initiating build for $PROJECT_NAME
echo .....................................................................
echo

# get return value of last executed script
ret_stat=$?
if [ $ret_stat -eq 0 ]
then
# Validate script is successful
	:
else
	exit 1
fi

# if BUILD_MODEis not set then set it to gui-config
if [ -z "$BUILD_MODE" ]
then
	BUILD_MODE="gui-config"
fi

echo
echo .....................................................................
echo Build Mode             : $BUILD_MODE
echo Build Date             : $CURRENT_DATE
echo Build Time             : $CURRENT_TIME
echo Build By               : $USERNAME
echo .....................................................................
echo Project Location : $PROJECT_DIR
echo .....................................................................
echo Distribution Location : $BIN_DIRECTORY
echo .....................................................................
echo


# Getting platform info
#export PLATFORM_CONFIG_PATH=$CURR_DIR/build-support/platform.conf
#source $PLATFORM_CONFIG_PATH

# export 'PLATFORM_TARGET' Environment variable for Menuconfig 
#export PLATFORM_TARGET=$PLATFORM

#Creating BIN_DIRECTORY
mkdir -v -p $BIN_DIRECTORY


#setting Build Mode
export PROJ_BUILD_MODE=$BUILD_MODE


# read menuconfig execution status
if [ -f $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH ]
then
	source $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH
	echo MENUCONFIG_EXECUTION_STATUS = $MENUCONFIG_EXECUTION_STATUS
	if [[ $MENUCONFIG_EXECUTION_STATUS = 'y' ]]
	then
		# rem -rf -v {directory}/.??* use for delete hidden directory or files
		# echo "If menuconfig execute then Deleting BIN_DIRECTORY/.PROJECT_NAME if present" >> $LOG_FILE
		# delete hidden .PROJECT_NAME/* files
		rm -rf -v $BIN_DIRECTORY/.??* #>> $LOG_FILE
		rm -rf -v $CMAKE_BIN_DIR/.??* # >> $LOG_FILE
		rm -rf -v  $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH >> $LOG_FILE
	fi
fi

# setup build Logging Files and directory
build_initiating_logging_execution



echo


#Creating Application Version
#echo Updating the version ...
#APP_VERSION_SCRIPT=./build-support/version.sh
#make_executable $APP_VERSION_SCRIPT
#$APP_VERSION_SCRIPT >> $LOG_FILE

# Getting platform info
#export PLATFORM_CONFIG_PATH=$CURR_DIR/build-support/platform.conf
#source $PLATFORM_CONFIG_PATH

com_port=""
com_baud=""

if [[ $1 == "-menuconfig" ]]	############################################# Execute Menuconfig operation
then
	echo 
	echo "## Execute EmMate Menuconfig ################################"
	#run menuconf_gen
	
	echo "Execute MenuConfig..."
	make_executable $MENUCONFIG_EXECUTION_SCRIPT
	$MENUCONFIG_EXECUTION_SCRIPT "-menuconfig" >> $LOG_FILE
	RETURN=$?
	if [ $RETURN -eq 0 ]
	then
		:
	else
		echo
		echo "## Execute EmMate Menuconfig Failed ################################"
		exit $RETURN
	fi
	echo
	echo "## Execute EmMate Menuconfig Successful ################################"
	exit 0
	
elif [[ $1 == "-make" ]]	############################################# Execute Make operation
then
	#Creating Application Version
	echo Updating the version ...
	APP_VERSION_SCRIPT=./build-support/version.sh
	make_executable $APP_VERSION_SCRIPT
	$APP_VERSION_SCRIPT >> $LOG_FILE
	
	
	#run menuconf_gen
	make_executable $MENUCONFIG_EXECUTION_SCRIPT
	$MENUCONFIG_EXECUTION_SCRIPT  >> $LOG_FILE
	RETURN=$?
	if [ $RETURN -eq 0 ]
	then
		:
	else
		echo "## Execute EmMate Project Build Failed ################################"
		exit $RETURN_STAT
	fi
	

	source $EMMATE_CONFIG_FILE_PATH
	export $(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f1)
	
	make_executable ./build-support/platform-process.sh
	./build-support/platform-process.sh	>> $LOG_FILE

	if [[ $CONFIG_PLATFORM_TARGET == "esp32" ]]
	then
		echo
		echo "Executing ESP32 Platform Build"
		
		# Copy the esp32 platform specific code to the emmate directory
		EMMATE_RESOURCE=$CURR_DIR/../emmate-resource
		EMMATE_SOURCE=$CURR_DIR/../emmate
		cp -u -r $EMMATE_RESOURCE/platform $EMMATE_SOURCE

		# set IDF_PATH and run the export script
		export IDF_PATH=$EMMATE_PATH/sdk/esp-idf
		IDF_EXPORT=$IDF_PATH/export.sh
		source $IDF_EXPORT
		
		mkdir -v -p $CMAKE_BIN_DIR
		cd $CMAKE_BIN_DIR
		cmake .. -DCMAKE_TOOLCHAIN_FILE=$IDF_PATH/tools/cmake/toolchain-esp32.cmake -G 'Unix Makefiles'
		cmake --build .
	elif [[ $CONFIG_PLATFORM_TARGET == "simulator" ]]
	then
		echo
		echo "Executing Simulator Platform Build"
		
		# Copy the simulator code to the emmate directory
		EMMATE_RESOURCE=$CURR_DIR/../emmate-resource
		EMMATE_SOURCE=$CURR_DIR/../emmate
		cp -u -r $EMMATE_RESOURCE/simulator $EMMATE_SOURCE
	fi
	
	RETURN_STAT=$?
	#echo $arg $RETURN_STAT
	if [ $RETURN_STAT -eq 0 ]
	then
		echo "## Execute EmMate Project Build Successful ################################"
		:
	else
		echo "## Execute EmMate Project Build Failed ################################"
		exit $RETURN_STAT
	fi
elif [[ $1 == "-erase-flash" ]]		############################################# Execute Erase-Flash operation
then

	source $EMMATE_CONFIG_FILE_PATH
	export $(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f1)
	
# check COM port --------------------------------
	if [[ "$2" =~ .*"-p".* ]]
	then 				
		#echo "-p present in $2"
		com_port=$(sed "s/-p//g" <<< "$2")
		#echo com_port = $com_port
	fi
	
	if [[ $com_port == "" ]]
	then
		com_port=$CONFIG_SERIAL_COM_PORT
	fi
	
# check COM baud-rate --------------------------------
	if [[ "$3" =~ .*"-b".* ]]
	then 
		#echo "-b present in $3"
		com_baud=$(sed "s/-b//g" <<< "$3")
		#echo com_baud = $com_baud
	fi
	
	if [[ $com_baud == "" ]]
	then
		com_port=$CONFIG_SERIAL_BAUD_RATE
	fi
	
	
# execute erase-flash command ------------------------------------------------------------------------------
	if [[ $CONFIG_PLATFORM_TARGET == "esp32" ]]
	then
		export IDF_PATH=$EMMATE_PATH/sdk/esp-idf
		cd $CMAKE_BIN_DIR
		python $IDF_PATH/components/esptool_py/esptool/esptool.py -p $com_port -b $com_baud erase_flash

	elif [[ $CONFIG_PLATFORM_TARGET == "simulator" ]]
	then
		echo
		echo Executing Simulator Platform Build
	fi

elif [[ $1 == "-flash" ]]	############################################# Execute Flash operation
then
	source $EMMATE_CONFIG_FILE_PATH
	export $(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f1)
	
# check COM port --------------------------------
	if [[ "$2" =~ .*"-p".* ]]
	then 				
		#echo "-p present in $2"
		com_port=$(sed "s/-p//g" <<< "$2")
		#echo com_port = $com_port
	fi
	
	if [[ $com_port == "" ]]
	then
		com_port=$CONFIG_SERIAL_COM_PORT
	fi
	
# check COM baud-rate --------------------------------
	if [[ "$3" =~ .*"-b".* ]]
	then 
		#echo "-b present in $3"
		com_baud=$(sed "s/-b//g" <<< "$3")
		#echo com_baud = $com_baud
	fi
	
	if [[ $com_baud == "" ]]
	then
		com_port=$CONFIG_SERIAL_BAUD_RATE
	fi
	
	
# execute flash command ------------------------------------------------------------------------------
	if [[ $CONFIG_PLATFORM_TARGET == "esp32" ]]
	then
		export IDF_PATH=$EMMATE_PATH/sdk/esp-idf
		cd $CMAKE_BIN_DIR
		python $IDF_PATH/components/esptool_py/esptool/esptool.py -p $com_port -b $com_baud write_flash @flash_project_args

	elif [[ $CONFIG_PLATFORM_TARGET == "simulator" ]]
	then
		echo
		echo Executing Simulator Platform Build
	fi

elif [[ $1 == "-log" ]]		############################################# Execute LOG operation
then
	source $EMMATE_CONFIG_FILE_PATH
	export $(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f1)
	
# check COM port --------------------------------
	if [[ "$2" =~ .*"-p".* ]]
	then 				
		#echo "-p present in $2"
		com_port=$(sed "s/-p//g" <<< "$2")
		#echo com_port = $com_port
	fi
	
	if [[ $com_port == "" ]]
	then
		com_port=$CONFIG_SERIAL_COM_PORT
	fi
				
# execute log command ------------------------------------------------------------------------------
	if [[ $CONFIG_PLATFORM_TARGET == "esp32" ]]
	then
		export IDF_PATH=$EMMATE_PATH/sdk/esp-idf
		IDF_EXPORT=$IDF_PATH/export.sh
		source $IDF_EXPORT

		cd $CMAKE_BIN_DIR
		PROJECT_ELF=$PROJECT_NAME.elf
		python $IDF_PATH/tools/idf_monitor.py -p $com_port $PROJECT_ELF
	
	elif [[ $CONFIG_PLATFORM_TARGET == "simulator" ]]
	then
		echo
		echo Executing Simulator Platform Build
	fi
	
elif [[ $1 == "-clean" ]]	############################################# Execute Clean operation
then
	echo
	echo "## Execute EmMate Clean ################################"
	
	# .??* delete all hidden files
	rm -rf -v $BIN_DIRECTORY/.??*	$BIN_DIRECTORY/*
	rm -rf -v $BIN_DIRECTORY

# ----------------------------------- CLEAN CMAKE BIN DIRECTORY------------------------------------
	rm -rf $CMAKE_BIN_DIR/.??*	$CMAKE_BIN_DIR/*
	rm -rf -v $CMAKE_BIN_DIR
# -------------------------------------------------------------------------------------------------

	echo
	echo "## Execute EmMate Clean Successful ################################"
	
else
	execute_build_helper_menu
fi
# ----------------------------------------------------------------------------------------------------



