#!/bin/bash

## Script Functions ###############################################################################
make_executable() {
	local script=$1
	if [ ! -x $script ]
	then
		echo "The script file is not executable...making the script executable"
		#make is executable
		chmod a+x $script
	fi
}

echo
# echo "Selected Platform is ESP32"

DESTINATION_PATH=$BIN_DIRECTORY/".$PROJECT_NAME"
echo $DESTINATION_PATH


#create CMake config files
make_executable ./build-support/gen-cmakeconfig.sh
echo "Creating ESP Project's CMake config files..."
./build-support/gen-cmakeconfig.sh $PROJECT_DIR	$PROJECT_DIR >> $LOG_FILE
