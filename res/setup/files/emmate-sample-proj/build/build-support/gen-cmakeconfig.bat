@ECHO OFF

rem echo gen-cmakeconfig.bat

set DESTINATION_PATH=%1
set PROJECT_PATH=%2

echo #### Create Project's CMake Config ###########################################

set EMMATE_CONFIG_FILE_PATH=%CURR_DIR%\..\emmate_config
set CMAKE_CONFIG_FILE_PATH=%DESTINATION_PATH%\emmate_config.cmake
ECHO %CMAKE_CONFIG_FILE_PATH%
set TEMP_CMAKE_CONFIG_FILE_PATH=%DESTINATION_PATH%\emmate_config_cmake.tmp

echo # CMake Configuration > %TEMP_CMAKE_CONFIG_FILE_PATH%

@setlocal EnableDelayedExpansion 
rem rem eol stops comments from being parsed
rem rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    rem if NOT "%%a"=="" set %%a=%%b
    
    rem echo %%a=%%b
    
    call set val=%%b
	set val=!val:"=!
    
    rem echo %%a=!val!
    
    
    rem set "cmake_conf=set( %%a "!val!" )"
	rem echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	
	if not "%%b" == "" ( 
		set "cmake_conf=set( %%a "!val!" )"
		echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	) else (
		set "cmake_conf=set( %%a "!val!" )"
		echo !cmake_conf! >> %TEMP_CMAKE_CONFIG_FILE_PATH%
	)
	   
)

rem type %TEMP_CMAKE_CONFIG_FILE_PATH%


if not exist %CMAKE_CONFIG_FILE_PATH% (
	echo "copy  %TEMP_CMAKE_CONFIG_FILE_PATH% == %CMAKE_CONFIG_FILE_PATH%"
	type %TEMP_CMAKE_CONFIG_FILE_PATH% > %CMAKE_CONFIG_FILE_PATH%
) else (
 	FC %TEMP_CMAKE_CONFIG_FILE_PATH%  %CMAKE_CONFIG_FILE_PATH%
	if !errorlevel! NEQ 0 (
		echo "copy  %TEMP_CMAKE_CONFIG_FILE_PATH% ---- %CMAKE_CONFIG_FILE_PATH%"
		type %TEMP_CMAKE_CONFIG_FILE_PATH% > %CMAKE_CONFIG_FILE_PATH%
	)
)

del /s /f /q %TEMP_CMAKE_CONFIG_FILE_PATH%

echo ################################################################################

