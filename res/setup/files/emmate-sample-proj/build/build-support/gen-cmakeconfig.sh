#!/bin/bash

DESTINATION_PATH=$1
PROJECT_PATH=$2

echo #### Create Project's CMake Config ###########################################

EMMATE_CONFIG_FILE_PATH=$CURR_DIR/../emmate_config
CMAKE_CONFIG_FILE_PATH=$DESTINATION_PATH/emmate_config.cmake
TEMP_CMAKE_CONFIG_FILE_PATH=$DESTINATION_PATH/emmate_config_cmake.tmp

source $EMMATE_CONFIG_FILE_PATH

echo "# CMake Configuration" > $TEMP_CMAKE_CONFIG_FILE_PATH

config_var=$(grep --regexp ^[A-Z] $EMMATE_CONFIG_FILE_PATH | cut -d= -f 1)

for argv in $config_var
do
	echo -e "set ( $argv \"${!argv}\" )" >> $TEMP_CMAKE_CONFIG_FILE_PATH
done

if [ -f $CMAKE_CONFIG_FILE_PATH ]
then
	file_diff=$(diff $CMAKE_CONFIG_FILE_PATH $TEMP_CMAKE_CONFIG_FILE_PATH)
	if [ -z "$file_diff" ]
	then
		# do nothing
		# : used for pass from this (if/else) state
		: 
	else
		#echo execute_component.mk file_check
		cat $TEMP_CMAKE_CONFIG_FILE_PATH > $CMAKE_CONFIG_FILE_PATH
	fi
else
	cat $TEMP_CMAKE_CONFIG_FILE_PATH > $CMAKE_CONFIG_FILE_PATH
fi


# remove all temp files from current directory
rm -fv $TEMP_CMAKE_CONFIG_FILE_PATH



echo ################################################################################