@ECHO OFF

echo --------------------- EmMate Build Help ---------------------

echo Usage: .\build.bat [options]

echo Option:
echo. 

echo -menuconfig					Execute the EmMate Project Menuconfig
echo.

echo -help						Show Help Information of the EmMate Build CMD
echo.

echo -make						Build the EmMate project(currently make into ESP Project structure)
echo.

echo -flash -p[port] -b[baud]			Flash the binaries to the EmMate Device (By default flash execute with menuconfigured port and baud rate.
echo 							but port and baud-rate can be enter via this cmd like: build.bat -flash -pCOM1 -b9600 )
echo.

echo -erase-flash -p[port] -b[baud]			Erase the running binaries from the EmMate Device (By default flash execute with menuconfigured port and baud rate.
echo 							but port and baud-rate can be enter via this cmd like: build.bat -erase-flash -pCOM1 -b9600 )
echo.

echo -log -p[port]					View the Output Log of the EmMate Device (By default flash execute with menuconfigured port and baud rate.
echo 							but port and baud-rate can be enter via this cmd like: build.bat -log -pCOM1)
echo.

echo.
echo.