cmake_minimum_required(VERSION 3.10)

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
#message("Project ID: " ${ProjectId})
project(${ProjectId} C CXX ASM)

#set_property(GLOBAL PROPERTY GLOBAL_DEPENDS_DEBUG_MODE 1)

# Include EmMate Configurations and add its subdirectories 
include(emmate_config.cmake)
add_subdirectory(emmate)
add_subdirectory(src)

if(CONFIG_PLATFORM_ESP_IDF)
	# Include for ESP-IDF build system functions
	include($ENV{IDF_PATH}/tools/cmake/idf.cmake)
	
	if(CONFIG_USE_BLE OR CONFIG_USE_FOTA)
		idf_build_process(esp32
				    COMPONENTS esp32 freertos bt esp_https_ota esp_adc_cal esptool_py
				    SDKCONFIG_DEFAULTS ${CMAKE_SOURCE_DIR}/build/build-support/esp32/sdkconfig.bt_ota
				    SDKCONFIG ${CMAKE_BINARY_DIR}/sdkconfig
				    BUILD_DIR ${CMAKE_BINARY_DIR})
	elseif(CONFIG_USE_FILE_SYSTEM)
		idf_build_process(esp32
				    COMPONENTS esp32 freertos esp_adc_cal fatfs esptool_py
				    SDKCONFIG_DEFAULTS ${CMAKE_SOURCE_DIR}/build/build-support/esp32/sdkconfig.defaults
				    SDKCONFIG ${CMAKE_BINARY_DIR}/sdkconfig
				    BUILD_DIR ${CMAKE_BINARY_DIR})
	else()
		idf_build_process(esp32
				    COMPONENTS esp32 freertos esp_adc_cal esptool_py
				    SDKCONFIG_DEFAULTS ${CMAKE_SOURCE_DIR}/build/build-support/esp32/sdkconfig.defaults
				    SDKCONFIG ${CMAKE_BINARY_DIR}/sdkconfig
				    BUILD_DIR ${CMAKE_BINARY_DIR})
	endif()
endif()

# Make the executable
set(elf_file ${CMAKE_PROJECT_NAME}.elf)
add_executable(${elf_file} src/main.c)

# Link the static libraries to the executable
list(APPEND EXTRA_LIBS 
					emmate_config
					common
					system
					${CMAKE_PROJECT_NAME}
					)
					
#if(CONFIG_PLATFORM_ESP_IDF)
#	list(APPEND IDF_LIBS 
#				idf::esp32
#				idf::freertos
#				idf::spi_flash
#				)
#endif()					
					
target_link_libraries(${elf_file} PRIVATE ${EXTRA_LIBS})

if(CONFIG_PLATFORM_ESP_IDF)
#	target_link_libraries(${elf_file} PRIVATE ${IDF_LIBS})
	# Attach additional targets to the executable file for flashing,
	# linker script generation, partition_table generation, etc.
	idf_build_executable(${elf_file})
endif()

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

