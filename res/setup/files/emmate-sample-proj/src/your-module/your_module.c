/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "your_module.h"

#define TAG	"your_module"

void your_module_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */

	CORE_LOGI(TAG, "Returning from your_module_init");
}

void your_module_loop() {
	CORE_LOGI(TAG, "In your_module_loop");
}
