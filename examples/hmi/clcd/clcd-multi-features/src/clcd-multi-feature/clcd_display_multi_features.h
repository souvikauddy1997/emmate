/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using clcd display
 *
 *
 *
 */

#ifndef YOUR_MODULE_H_
#define YOUR_MODULE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "clcd.h"

/**
 * @brief	Init function for clcd_display_string module
 *
 */
void clcd_display_init();

/**
 * @brief	Execution function for clcd_display_string module
 *
 */
void clcd_display_string();

/**
 * @brief	Execution function for clcd_display_string module in loop
 *
 */
void clcd_display_string_loop();

#endif	/* YOUR_MODULE_H_ */
