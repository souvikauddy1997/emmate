/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_button.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using multiple buttons
 *
 *
 *
 */

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for multi_button module
 *
 */
void multi_button_init();

/**
 * @brief	Execution function for multi_button module
 *
 */
void multi_button_loop();

