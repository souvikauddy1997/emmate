add_subdirectory(include)

# Add source files to the application
set(srcs
	core_app_main.c
	
	# Add your module's source files here.
	# If your source files are inside a directory like your_module.c, then add the full path
	button-interaction/button_interaction.c
	
	)

add_library(${CMAKE_PROJECT_NAME} STATIC ${srcs})

# List the libraries on which this application is dependent
list(APPEND EMMATE_LIBS 
					emmate_config
					${CMAKE_PROJECT_NAME}_inc
					# Add more libraries below this line
					gpio
					button
					)
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE ${EMMATE_LIBS})

# Add include directories
target_include_directories(${CMAKE_PROJECT_NAME} 
							PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
							# Add more include directories below this line
							
							)
