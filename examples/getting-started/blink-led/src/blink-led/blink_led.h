/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   blink_led.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Blink of a LED.
 *
 *
 *
 */

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for blink_led module
 *
 */
void blink_led_init();

/**
 * @brief	Execution function for blink_led module
 *
 */
void blink_led_loop();

