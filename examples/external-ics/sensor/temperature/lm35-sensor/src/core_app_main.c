
#include "lm35_sensor.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	lm35_sensor_init();

	while(1){
		lm35_sensor_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
