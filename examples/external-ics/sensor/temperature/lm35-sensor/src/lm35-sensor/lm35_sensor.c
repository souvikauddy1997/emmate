/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "lm35_sensor.h"
#include "lm35.h"
#define TAG	"LM35_sensor"

void lm35_sensor_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);
	lm35_init(); // Initialize LM35
	lm35_start_process() ;   // Start Process of LM35
	CORE_LOGI(TAG, "Returning from your_module_init");
}

void lm35_sensor_loop() {
	CORE_LOGI(TAG, "In your_module_loop");
	float temperature = 0;
	lm35_get_temperature(&temperature);  // Read temperature from LM35
	CORE_LOGI(TAG, "%0.1f C\n",temperature);
}
