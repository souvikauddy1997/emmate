/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef LM35_SENSOR_H_
#define LM35_SENSOR_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief  Initialize LM35
 * @return
 *
 **/
void lm35_sensor_init();
/**
 * @brief Read temperature from LM35
 *   print value in degree Celsius
 *
 * @return
 *
 **/
void lm35_sensor_loop();

#endif	/* LM35_SENSOR_H_ */
