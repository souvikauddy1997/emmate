/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef YOUR_MODULE_H_
#define YOUR_MODULE_H_

#include "emmate.h"
#include "thing.h"

/**
 *
 * */
void mq5_sensor_app_init();

/**
 * */
void mq5_sensor_app_loop();

#endif	/* YOUR_MODULE_H_ */
