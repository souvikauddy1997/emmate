/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   tm_to_sec.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time convention from tm structure to seconds.
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief	Init function for tm_to_sec module
 *
 */
void tm_to_sec_init();

/**
 * @brief	Execution function for tm_to_sec module
 *
 */
void tm_to_sec_loop();


