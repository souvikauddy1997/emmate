/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   tm_to_sec.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time convention from tm structure to seconds.
 *
 *
 *
 */

#include "tm_to_sec.h"
#include "core_common.h"
#include "core_constant.h"
#include "system.h"
#include "systime.h"

#include <string.h>

#define TAG "tm_to_sec"

/********************************************** Module's Static Functions **********************************************************************/

void get_curr_tm(struct tm *now) {
	struct tm curr_tm;

	// Get the current time.
	if (get_systime(&curr_tm) != 0) {
		memset(now, 0x00, sizeof(struct tm));
		CORE_LOGE(TAG, "Failed to fetch System's Current Time ...");
	} else {
		char strftime_buf[64];

		// make time string from current-time structure data, in human readable format.
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &curr_tm);

		// print the Current Date/Time
		CORE_LOGI(TAG, "The current UTC date/time is: %s", strftime_buf);

		memcpy(now, &curr_tm, sizeof(struct tm));
	}
}

/*******************************************************************************************************************/

void tm_to_sec_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
}

void tm_to_sec_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	struct tm now;

	// get the Current system time
	get_curr_tm(&now);

	CORE_LOGI(TAG, "Current system time in Seconds: %ld\r\n", convert_tm_to_seconds(&now));

	// wait
	CORE_LOGI(TAG, "Waiting for %d seconds ...", (DELAY_30_SEC)/1000);
	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);

	// get the Current system time
	get_curr_tm(&now);

	CORE_LOGI(TAG, "Current system time in Seconds: %ld\r\n", convert_tm_to_seconds(&now));
}
