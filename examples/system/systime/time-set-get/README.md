# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![image](res/fritzing/logging.png)


### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to set and get system using the systime module APIs.

#### Example specific configurations
This example is configured ..

<img src="res/systime-example-config.png" width="500">

See the functions `time_set_get_init() and time_set_get_loop()` for better understanding.

To set a desired current time as the system time open the file `time-set-get/time_set_get.h` and change the macro `CURRENT_TIME_MILLIS`