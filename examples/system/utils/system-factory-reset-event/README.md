# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="res/fritzing/system-factory-reset-event.png" width="700">

[//]: ![ESP32-DevKit-C](fritzing/system-factory-reset-event.png)

### For Other Hardware

Comming soon ...

## About this example

The EmMate Framework has a provision to attach a Factory Reset Button with the system. This button must be configured in `thing/thing.h`. Whenever this button is pressed and held for 10 seconds a factory reset event is generated. Now, factory reset means that all saved data/configurations will be deleted. So before clearing the memory the system provides a scope to the application to do any operation before completing the factory reset operation.

This example demonstrates system's factory-reset event functionality. It uses EmMate's `system/system_utils.h` APIs.

#### Example specific configurations
This example is configured ..

<img src="res/system-factory-reset-event-example-config.png" width="500">

This example does the following things:

- The application registers all callback function, that will be called whenever a factory reset event is generated, by calling the function `register_factory_reset_event_function()`

- When the callback is called, the application just prints some information for demonstration purpose. In a real application addtional application specific task can be done.

- Only after the control returns from this function, the factory reset actually takes place.
