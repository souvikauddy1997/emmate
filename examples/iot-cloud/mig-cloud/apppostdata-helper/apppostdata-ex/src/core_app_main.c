
#include "apppostdata_ex.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling apppostdata_ex_init() in your_module.c in your-module directory ...");
	apppostdata_ex_init();
	CORE_LOGI(TAG, "Returned from apppostdata_ex_init()");

	while(1){
		CORE_LOGD(TAG, "Calling apppostdata_ex_loop() in your_module.c in your-module directory ...");
		apppostdata_ex_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
