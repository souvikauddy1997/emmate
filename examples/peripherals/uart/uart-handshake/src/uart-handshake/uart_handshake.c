/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "uart_handshake.h"
#include "uart_helper_api.h"
#include <string.h>
#include "core_utils.h"

#define TAG	"uart_handshake"

// redine Maximum count for handshake between 2 EPS
#define MAX_COUNT_OF_COMMUNICATE 20

/*
 * Define 2 macro for MASTER_BOARD & SLAVE_BOARD
 * @note:
 * 	when you need to build & flash as master device then just set the MASTER_BOARD as 1 & SLAVE_BOARD as 0
 * 	when you need to build & flash as slave  device then just set the MASTER_BOARD as 0 & SLAVE_BOARD as 1
 */
#define MASTER_BOARD	1
#define SLAVE_BOARD		0

// MASTER_BOARD's messages
#define MASTER_BOARD_GREETING_MSG		"Hello SLAVE_BOARD\r\n"
#define MASTER_BOARD_GOOD_BYE_MSG		"Good Bye SLAVE_BOARD\r\n"

// SLAVE_BOARD's messages
#define SLAVE_BOARD_GREETING_MSG		"Hi! MASTER_BOARD\r\n"
#define SLAVE_BOARD_GOOD_BYE_MSG		"It’s been a pleasure working with you. MASTER_BOARD\r\n"


// Use UART Port 1
#define UART_PORT_NUM	UART_CORE_NUM_1

// Define UART's Tx, Rx (We use UART 1 without Hardware flowcontrol so CTS, RTS leave it blank)
#define UART_TX_PIN		UART_TX_GPIO
#define UART_RX_PIN		UART_RX_GPIO
#define UART_CTS_PIN
#define UART_RTS_PIN

// Define UART's Communication BaudRate
#define UART_BAUD_RATE	115200

static uint8_t count_of_communicate = 0;

// declear uart's receive data buffer
char uart_recv_data[1024] = { 0 };
size_t uart_recv_data_len = 0;

void uart_transactor_cb(uart_core_port_t uart_port, void* data, size_t size) {
	if (UART_PORT_NUM == uart_port) {
		// copy receive data
		strncpy(uart_recv_data, (char*) data, size);
#if MASTER_BOARD
		// Display Received data in the LOG
		CORE_LOGI(TAG, "Received Data(%d) From '%s' -> %s ", count_of_communicate, GET_VAR_NAME(SLAVE_BOARD, NULL),
				(char* )data);
#elif SLAVE_BOARD
		// Display Received data in the LOG
		CORE_LOGI(TAG, "Received Data(%d) From '%s' -> %s ",count_of_communicate, GET_VAR_NAME(MASTER_BOARD, NULL), (char* )data);
#endif

	}
}

void uart_handshake_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	/*
	 * initialize UART with
	 * specific Uart PORT
	 * baud rate
	 * Tx & Rx GPIO
	 * Uart Data Packet type
	 * Uart data receive callback
	 *
	 */
	core_err res = init_uart_driver(UART_PORT_NUM, UART_BAUD_RATE, UART_TX_PIN, UART_RX_PIN, UART_8N1,
			uart_transactor_cb);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize Uart");
	} else {
#if MASTER_BOARD
		CORE_LOGI(TAG, "Hello I'm %s and I'm Starting Communication with %s", GET_VAR_NAME(MASTER_BOARD, NULL),
				GET_VAR_NAME(SLAVE_BOARD, NULL));
		// wait for 2sec to Start Communication
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
		uart_driver_write_bytes(UART_PORT_NUM, MASTER_BOARD_GREETING_MSG, strlen(MASTER_BOARD_GREETING_MSG));
#elif SLAVE_BOARD
		CORE_LOGI(TAG, "Hello I'm %s and I'm Starting Communication with %s", GET_VAR_NAME(SLAVE_BOARD, NULL),
				GET_VAR_NAME(MASTER_BOARD, NULL));
#endif
	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void uart_handshake_loop() {
	CORE_LOGD(TAG, "In %s", __func__);
#if MASTER_BOARD

	// wait for getting messages from slave
	while (1) {
		if (strcmp(uart_recv_data, SLAVE_BOARD_GREETING_MSG) == 0) {
			// clear the last received data buffer
			memset(uart_recv_data, 0, sizeof(uart_recv_data));
			count_of_communicate++;

			if (count_of_communicate == MAX_COUNT_OF_COMMUNICATE) {
				// send the Last Greeting message to the slave from master
				uart_driver_write_bytes(UART_PORT_NUM, MASTER_BOARD_GOOD_BYE_MSG, strlen(MASTER_BOARD_GOOD_BYE_MSG));
			} else {
				// send the Greeting message to the slave from master
				uart_driver_write_bytes(UART_PORT_NUM, MASTER_BOARD_GREETING_MSG, strlen(MASTER_BOARD_GREETING_MSG));
			}
			break;
		} else if (strcmp(uart_recv_data, SLAVE_BOARD_GOOD_BYE_MSG) == 0) {
			// clear the last received data buffer
			memset(uart_recv_data, 0, sizeof(uart_recv_data));

			// After received the last message from the Slave and Master De-Initialize the UART
			core_err res = deinit_uart_driver(UART_PORT_NUM);
			if (res != CORE_OK) {
				CORE_LOGE(TAG, "failed to deinitialize Uart");
			}
		}
		TaskDelay(DELAY_100_MSEC / TICK_RATE_TO_MS);
	}

#elif SLAVE_BOARD
	// wait for getting messages from master
	while (1) {
		if (strcmp(uart_recv_data, MASTER_BOARD_GREETING_MSG) == 0) {
			count_of_communicate++;
			// clear the last received data buffer
			memset(uart_recv_data, 0, sizeof(uart_recv_data));
			// send the Greeting message to the Master from slave
			uart_driver_write_bytes(UART_PORT_NUM, SLAVE_BOARD_GREETING_MSG, strlen(SLAVE_BOARD_GREETING_MSG));
			break;
		} else if (strcmp(uart_recv_data, MASTER_BOARD_GOOD_BYE_MSG) == 0) {
			// clear the last received data buffer
			memset(uart_recv_data, 0, sizeof(uart_recv_data));

			// send the last greeting message to the Master from slave
			uart_driver_write_bytes(UART_PORT_NUM, SLAVE_BOARD_GOOD_BYE_MSG, strlen(SLAVE_BOARD_GOOD_BYE_MSG));

			//wait for 1sec
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

			// After received the last message from the Master and slave De-Initialize the UART
			core_err res = deinit_uart_driver(UART_PORT_NUM);
			if (res != CORE_OK) {
				CORE_LOGE(TAG, "failed to deinitialize Uart");
			}
		}
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}

#endif
}
