
#include "uart_handshake.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling uart_handshake_init() in your_module.c in your-module directory ...");
	uart_handshake_init();
	CORE_LOGI(TAG, "Returned from uart_handshake_init()");

	while(1){
		CORE_LOGD(TAG, "Calling uart_handshake_loop() in your_module.c in your-module directory ...");
		uart_handshake_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
