
#include "led_pwm_with_rgb_led.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling your_module_init() in your_module.c in your-module directory ...");
	led_pwm_with_rgb_led_init();
	CORE_LOGI(TAG, "Returned from your_module_init()");

	while(1){
		led_pwm_with_rgb_led_loop();
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}
}
