/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "led_pwm_with_rgb_led.h"
#include "core_utils.h"
#include "led_pwm_driver.h"

#define TAG	"led_pwm_with_rgb_led"

static RGB_COLOURS colour = WHITE;

/*
 * There are two kinds of RGB LEDs:
 * Common anode(+) LED and Common cathode(-) LED.
 * In this Example We used common cathod RGB Led. So, "COMMON_ANODE_RGB_LED" set as 0
 *
 * @note:
 * 	If in case you using a Common anode RGB Led then, set the "#define COMMON_ANODE_RGB_LED" to 1 to get the output.
 */
#define COMMON_ANODE_RGB_LED	0

core_err show_rgb_combine_colours(uint8_t red_decimal_code, uint8_t green_decimal_code, uint8_t blue_decimal_code) {

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Red GPIO
	 */
	core_err res = led_pwm_driver_set_decimal_code(LED_PWM_CHANNEL_0, red_decimal_code);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_0);
		return res;
	}

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Green GPIO
	 */
	res = led_pwm_driver_set_decimal_code(LED_PWM_CHANNEL_1, green_decimal_code);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_1);
		return res;
	}

	/*
	 * Set & Start LED PWM with 8bit Decimal Colour Code for RGB Led Blue GPIO
	 */
	res = led_pwm_driver_set_decimal_code(LED_PWM_CHANNEL_2, blue_decimal_code);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to set duty cycle to configured LED PWM Channel %d", LED_PWM_CHANNEL_2);
		return res;
	}

	return res;
}

void led_pwm_with_rgb_led_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	/* initializations LED PWM*/
	core_err res = led_pwm_driver_configure_timer(LED_PWM_TIMER_13_BIT, 5000, LED_PWM_TIMER_0);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Timer driver");
	}

	/*
	 * Install & activate LED PWM functionality
	 */
	res = led_pwm_driver_fade_func_install();
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM fade functionality");
	}

	/*
	 * Configure LED PWM Channels for RGB Led's Red, Green, Blue GPIOs
	 * @note:
	 * 	Set every Leds in off state.
	 * 	for Common Anode, Set 255 so LED GPIO will always on but physical LED work as off condition
	 * 	for Common Cathod, Set 0 so LED GPIO will always in off condition
	 */

	/*
	 * Configure LED PWM Channels for RGB Led Red GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_0, 255, RGB_LED_RED_GPIO,
			LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_0, 0, RGB_LED_RED_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 0");
	}

	/*
	 * Configure LED PWM Channels for RGB Led Green GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_1, 255, RGB_LED_GREEN_GPIO,
			LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_1, 0, RGB_LED_GREEN_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 1");
	}

	/*
	 * Configure LED PWM Channels for RGB Led Blue GPIO
	 */
#if COMMON_ANODE_RGB_LED
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_2, 255, RGB_LED_BLUE_GPIO,
			LED_PWM_TIMER_0);
#else
	res = led_pwm_driver_configure_channel_with_decimal_code(LED_PWM_CHANNEL_2, 0, RGB_LED_BLUE_GPIO, LED_PWM_TIMER_0);
#endif
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "failed to initialize LED PWM Channel 2");
	}

	CORE_LOGI(TAG, "Returning from your_module_init");
}

void led_pwm_with_rgb_led_loop() {
//	CORE_LOGD(TAG, "In %s", __func__);

	switch (colour) {
	case WHITE: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(WHITE, NULL));
		/**
		 * Check this link for mode information about RGB Colour Combination
		 * https://www.rapidtables.com/web/color/RGB_Color.html#color-table
		 */
#if COMMON_ANODE_RGB_LED

		show_rgb_combine_colours(0, 0, 0);
#else
		show_rgb_combine_colours(255, 255, 255);
#endif
		colour++;
		break;
	}
	case RED: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(RED, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(0, 255, 255);
#else
		show_rgb_combine_colours(255, 0, 0);
#endif
		colour++;
		break;
	}
	case LIME: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(LIME, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 0, 255);
#else
		show_rgb_combine_colours(0, 255, 0);
#endif
		colour++;
		break;
	}
	case BLUE: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(BLUE, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 255, 0);
#else
		show_rgb_combine_colours(0, 0, 255);
#endif
		colour++;
		break;
	}
	case YELLOW: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(YELLOW, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(0, 0, 255);
#else
		show_rgb_combine_colours(255, 255, 0);
#endif
		colour++;
		break;
	}
	case CYAN: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(CYAN, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 0, 0);
#else
		show_rgb_combine_colours(0, 255, 255);
#endif
		colour++;
		break;
	}
	case MAGENTA: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(MAGENTA, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(0, 255, 0);
#else
		show_rgb_combine_colours(255, 0, 255);
#endif
		colour++;
		break;
	}
	case SILVER: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(SILVER, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(63, 63, 63);
#else
		show_rgb_combine_colours(192, 192, 192);
#endif
		colour++;
		break;
	}
	case GRAY: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(GRAY, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(127, 127, 127);
#else
		show_rgb_combine_colours(128, 128, 128);
#endif
		colour++;
		break;
	}
	case MAROON: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(MAROON, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(127, 255, 255);
#else
		show_rgb_combine_colours(128, 0, 0);
#endif
		colour++;
		break;
	}
	case OLIVE: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(OLIVE, NULL));

#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(127, 127, 255);
#else
		show_rgb_combine_colours(128, 128, 0);
#endif

		colour++;
		break;
	}
	case GREEN: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(GREEN, NULL));

#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 127, 255);
#else
		show_rgb_combine_colours(0, 128, 0);
#endif
		colour++;
		break;
	}
	case PURPLE: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(PURPLE, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(127, 255, 127);
#else
		show_rgb_combine_colours(128, 0, 128);
#endif
		colour++;
		break;
	}
	case TEAL: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(TEAL, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 127, 127);
#else
		show_rgb_combine_colours(0, 128, 128);
#endif
		colour++;
		break;
	}
	case NAVY: {
		CORE_LOGI(TAG, "RGB colour = %s", GET_VAR_NAME(NAVY, NULL));
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 255, 127);
#else
		show_rgb_combine_colours(0, 0, 128);
#endif
		colour++;
		break;
	}

	default: {
#if COMMON_ANODE_RGB_LED
		show_rgb_combine_colours(255, 255, 255);
#else
		show_rgb_combine_colours(0, 0, 0);
#endif
		colour = 0;
		break;
	}

	}

}

