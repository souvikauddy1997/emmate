#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export CONFIG_PATH=$CURR_DIR/setup.conf

source $CONFIG_PATH
export $(grep --regexp ^[A-Z] $CONFIG_PATH | cut -d= -f1)

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

echo .....................................................................
echo Setting up your project
echo .....................................................................
echo Core distribution Location : $RELEASE_PKG_DIR
echo .....................................................................

if [ -z "$RELEASE_PKG_DIR" ]
then
	echo "Environment variable 'EMMATE_RELEASE_PATH' is not set"
	echo "You need to update your 'EMMATE_RELEASE_PATH' environment variable in the ~/.profile file."
	exit 1
fi


emmate_core_src=$RELEASE_PKG_DIR/src
emmate_platform_src=$RELEASE_PKG_DIR/platform

cd ..
export PROJ_DIR=`pwd`

echo Project Location : $PROJ_DIR
echo .....................................................................


#copying a temporary sample src
cd $RELEASE_PKG_DIR/setup/files/emmate-sample-proj

# Copy the root CMAkeLists.txt file to the project root
cp -v -u -r CMakeLists.txt $PROJ_DIR

# Always copy the build folder from RELEASE_PKG_DIR/setup/files
cp -v -u -r ./build $PROJ_DIR

if [ ! -d "$PROJ_DIR/src" ]
then

	echo "Setting up new project"
	
	echo "Copying the src to $PROJ_DIR"
	mkdir -p $PROJ_DIR/src
	
	#copy projects src templates files
	cp -v -u -r ./src/ $PROJ_DIR
		
#	echo "Copying the Eclipse files to $PROJ_DIR"
#	cp -v -u -r ./.externalToolBuilders $PROJ_DIR
#	cp -v -u -r ./.launches $PROJ_DIR
#	cp -v -u -r ./.settings $PROJ_DIR
#	cp -v -u -r ./.cproject $PROJ_DIR
#	cp -v -u -r ./.project $PROJ_DIR

else
: #skip
#		echo "Copying the Eclipse files to $PROJ_DIR"
#		cp -v -u -r ./.externalToolBuilders $PROJ_DIR
#		cp -v -u -r ./.launches $PROJ_DIR
#		cp -v -u -r ./.settings $PROJ_DIR
#		cp -v -u -r ./.cproject $PROJ_DIR
#		cp -v -u -r ./.project $PROJ_DIR

fi

# creating emmate directories
mkdir -p $PROJ_DIR/emmate

# creating build directories
mkdir -p $PROJ_DIR/build


cd $emmate_core_src
cp -v -u -r . $PROJ_DIR/emmate
mv -u -v -f $PROJ_DIR/emmate/*.c	$PROJ_DIR/src
rm -fv $PROJ_DIR/emmate/*.c


cd $emmate_platform_src
cp -v -u -r . $PROJ_DIR/emmate



