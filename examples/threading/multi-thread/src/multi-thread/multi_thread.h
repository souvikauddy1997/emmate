/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task
 *
 *
 *
 */

#ifndef MULTI_THREAD_H_
#define MULTI_THREAD_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for multi_thread module
 *
 */
void multi_thread_init();

/**
 * @brief	Execution function for multi_thread module
 *
 */
void multi_thread_loop();

#endif	/* MULTI_THREAD_H_ */
